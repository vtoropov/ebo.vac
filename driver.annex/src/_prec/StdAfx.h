#ifndef _STDAFX_H_4BE97360_F93F_4e5e_BBA9_1131455A19E1_INCLUDED
#define _STDAFX_H_4BE97360_F93F_4e5e_BBA9_1131455A19E1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-May-2018 at 12:17:27pm, GMT+7, Phuket, Rawai, Tuesday;
	This is File Guardian driver common library precompiled headers definition file.
	-----------------------------------------------------------------------------
	Adopted to Ebo virtual audio device/connector project on 20-Jun-2020 at 9:02:21p, UTC+7, Novosibirsk, Saturday;
*/

#ifndef WINVER                 // Specifies that the minimum required platform is Windows Vista.
#define WINVER         0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT           // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINNT   0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINDOWS         // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINDOWS 0x0600  // Change this to the appropriate value to target other platforms.
#endif

extern "C"
{
#pragma warning(push, 0)
	#include <fltKernel.h>
	#include <dontuse.h>
	#include <suppress.h>
	#include <Wdmsec.h>
	#include <windef.h>
	#include <ntimage.h>
	#include <stdarg.h>
//	#define NTSTRSAFE_NO_CB_FUNCTIONS
	#include <ntstrsafe.h>
	#include <ntddstor.h>
	#include <mountdev.h>
	#include <ntddvol.h>
	#include <Aux_klib.h>
	#include <bcrypt.h>
	#include <FltUserStructures.h>
#pragma warning(pop)
}

#pragma prefast(disable:__WARNING_ENCODE_MEMBER_FUNCTION_POINTER, "Not valid for kernel mode drivers")
/////////////////////////////////////////////////////////////////////////////
//
// macro utilities
//

#define FlagOnAll( F, T )  \
    (FlagOn( F, T ) == T)

#define SetFlagInterlocked(_ptrFlags,_flagToSet) \
    ((VOID)InterlockedOr(((volatile LONG*)(_ptrFlags)),_flagToSet))

#ifndef _T
#define _T(x) L ## x
#endif

#define PAGE_CODE_EX(_return)                            \
		if (PASSIVE_LEVEL != KeGetCurrentIrql()) {       \
			return _return;                              \
		}
/////////////////////////////////////////////////////////////////////////////
//
// constants and structures
//
#define S_OK STATUS_SUCCESS
#define SUCCEEDED(_nt_status) (S_OK == _nt_status)

typedef LIST_ENTRY             LIST_HEAD;
typedef NPAGED_LOOKASIDE_LIST  LIST_ALLOC;

#define LIST_FOR_EACH_SAFE(pCurr  , pNext, pHead) \
		for (pCurr = pHead->Flink , pNext = pCurr->Flink ; pCurr != pHead; \
		     pCurr = pNext        , pNext = pCurr->Flink)

#endif/*_STDAFX_H_4BE97360_F93F_4e5e_BBA9_1131455A19E1_INCLUDED*/