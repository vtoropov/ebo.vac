#ifndef _FGDRIVERCOMMONSTR_H_F1B20534_38D0_4F3D_92AD_2A1FFA198B75_INCLUDED
#define _FGDRIVERCOMMONSTR_H_F1B20534_38D0_4F3D_92AD_2A1FFA198B75_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-May-2018 at 7:42:06p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian shared library common driver string management interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 30-May-2018 at 9:19:35a, UTC+7, Phuket, Rawai, Wednesday;
	Adopted to Ebo virtual audio device/connector project on 20-Jun-2020 at 9:30:40p, UTC+7, Novosibirsk, Saturday;
*/

#ifndef _T
#define _T(x) L ## x
#endif

//
// a size is provided in characters, not in bytes
//
#define DECLARE_UNICODE_STRING(_var, _size )             \
                   WCHAR _var ## _buffer[ _size ] = {0}; \
          UNICODE_STRING _var = { 0, _size * sizeof(WCHAR) , _var ## _buffer }

//
// a size is provided in characters, not in bytes
//
#define DECLARE_UNICODE_STRING_STATIC(_var, _size )      \
          static  WCHAR _var ## _buffer[ _size ] = {0};  \
          static UNICODE_STRING _var = { 0, _size * sizeof(WCHAR) , _var ## _buffer }

//
// a size is calculated in bytes as sizeof(_buffer)
//
#define BIND_UNICODE_STRING(_var, _buffer)              \
                   _var.Buffer =  _buffer;               \
                   _var.Length =   0x0;                  \
                   _var.MaximumLength = sizeof(_buffer);

#define ABSOLUTE(wait) (wait)

#define RELATIVE(wait) (-(wait))

#ifndef NANOSECONDS
#define NANOSECONDS(nanos) (((signed __int64)(nanos)) / 100L)
#endif

#define MICROSECONDS(micros) \
(((signed __int64)(micros)) * NANOSECONDS(1000L))

#define MILLISECONDS(milli) \
(((signed __int64)(milli)) * MICROSECONDS(1000L))

#define SECONDS(seconds) \
(((signed __int64)(seconds)) * MILLISECONDS(1000L))
//
// http://www.osronline.com/article.cfm?article=261
// sample: interval.QuadPart = RELATIVE(SECONDS(5));
//
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace drv { namespace km { namespace annex {

	// https://docs.microsoft.com/en-us/windows-hardware/drivers/kernel/summary-of-kernel-mode-safe-string-functions

	class CStringW {
	private:
		UNICODE_STRING  m_str;
	public:
		 CStringW(void);
		 CStringW(CONST WCHAR* lpszSource);
		 CStringW(CONST CStringW&);
		~CStringW(void);
	public:
		NTSTATUS Allocate(const USHORT _szBytes);
		bool     IsValid (void) const;
		/*
			This function produces a memory allocation error;
			a reason with page allocation in non-page memory area;
		*/
		NTSTATUS CopyFrom(CONST WCHAR* lpszSource, CONST UINT uLength);
		NTSTATUS CopyFrom(PUNICODE_STRING pSource);
		NTSTATUS CopyTo  (UNICODE_STRING& _string);
	public:
		/*
			Routine Description:
				This helper routine simply allocates a buffer for a UNICODE_STRING and
				initializes its Length to zero.

				It uses whatever value is present in the MaximumLength field as the size
				for the allocation.

			Arguments:
				String - Pointer to UNICODE_STRING.

			Return Value:
				STATUS_INSUFFICIENT_RESOURCES if it was not possible to allocate the
				buffer from pool, STATUS_SUCCESS otherwise.
		*/
		static
		NTSTATUS
		Allocate (
			__inout PUNICODE_STRING _pString
			);
		/*
			Routine Description:
				This helper routine finds one string in another, returns start index of the first occurence,
				otherwise, -1.

			Arguments:
				_pWhere - Pointer to UNICODE_STRING, where the second string is searched.
				_pWhat  - Pointer to UNICODE_STRING, which is being searched.
		*/
		static
		INT
		Find (
			__inout PUNICODE_STRING _pWhere,
			__inout PUNICODE_STRING _pWhat
			);
		/*
			Routine Description:
				This helper routine frees the buffer of a UNICODE_STRING and resets its
				length to zero.

			Arguments:
				String - Pointer to UNICODE_STRING.
		*/
		static
		VOID
		Free (
			__inout PUNICODE_STRING _pString
			);

		static
		VOID DigitToString(UNICODE_STRING&, const LONG _digit);

	public:
		CStringW& operator = (PCWCHAR);
		CStringW& operator+= (PCWCHAR);

	public:
		operator PUNICODE_STRING (void);
		operator const PUNICODE_STRING (void) const;

	private:
		CStringW& operator=(const CStringW&);
	};

}}}}

#endif/*_FGDRIVERCOMMONSTR_H_F1B20534_38D0_4F3D_92AD_2A1FFA198B75_INCLUDED*/