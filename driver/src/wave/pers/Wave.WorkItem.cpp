/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Jun-2020 at 12:57:26p, UTC+7, Novosibirsk, Saturday;
	This is Ebo virtual audio device/conntector persistent work item interface implementation file.
*/
#include "StdAfx.h"
#include "Wave.WorkItem.h"

using namespace ebo::drv::km::pers;

#define __max_work_items_count   15
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace drv { namespace km { namespace pers { namespace _impl {

	PSAVEWORKER_PARAM WorkItem_GetParams(void) {
		static PSAVEWORKER_PARAM params = NULL;
		return params;
	}

}}}}}
using namespace ebo::drv::km::pers::_impl;
/////////////////////////////////////////////////////////////////////////////

CWaveWorkItems:: CWaveWorkItems(void) {}
CWaveWorkItems::~CWaveWorkItems(void) {}

/////////////////////////////////////////////////////////////////////////////

TSaveWorkItemPtr  CWaveWorkItems::Get (void) { TSaveWorkItemPtr pItem = NULL;

	LARGE_INTEGER timeOut   = { 0 };
	NTSTATUS      nt_status = STATUS_SUCCESS;

	TSaveWorkItemPtr pItems = WorkItem_GetParams();
	if (NULL == pItems)
		return  pItem ;

	for (int i_ = 0; i_ < __max_work_items_count; i_++) {

		TSaveWorkItem item_ref = pItems[i_];
		
		nt_status = ::KeWaitForSingleObject (
			&item_ref.EventDone, Executive, KernelMode, FALSE, &timeOut
		);

		if (S_OK == nt_status) {
			pItem = &item_ref; break;
		}
	}

	return pItem;
}

#pragma code_seg("PAGE")
VOID  CWaveWorkItems::Wait ( void )
{
	PAGED_CODE();
	TSaveWorkItemPtr pItems = WorkItem_GetParams();
	if (NULL == pItems)
		return;
	NTSTATUS nt_status = S_OK;

	for (int i_ = 0; i_ < __max_work_items_count; i_++) {
		// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-kewaitforsingleobject
		nt_status = ::KeWaitForSingleObject (
			&(pItems[i_].EventDone), Executive , KernelMode, FALSE, NULL
		);
	}
}
#pragma code_seg()

/////////////////////////////////////////////////////////////////////////////

#pragma code_seg("PAGE")
NTSTATUS   CWaveWorkItems::Create  (_In_  PDEVICE_OBJECT pDevice) {
	
	PAGED_CODE();

	NTSTATUS nt_status = S_OK;

	if (NULL == pDevice) {
		return (nt_status = STATUS_INVALID_PARAMETER);
	}

	PSAVEWORKER_PARAM pParams = WorkItem_GetParams();
	if (NULL != pParams)
		return (nt_status = STATUS_ALREADY_INITIALIZED);
	// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-exallocatepoolwithtag
	pParams = reinterpret_cast<PSAVEWORKER_PARAM>(
		::ExAllocatePoolWithTag (
			NonPagedPool, sizeof(SAVEWORKER_PARAM) * __max_work_items_count, AUX_STRING_POOL_TAG
		));
	if (NULL == pParams)
		return (nt_status = STATUS_INSUFFICIENT_RESOURCES);

	for (int i_ = 0; i_ < __max_work_items_count; i_++) {
		// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-ioallocateworkitem
		pParams[i_].pWorkItem = ::IoAllocateWorkItem(pDevice);

		if (NULL == pParams[i_].pWorkItem) {
			nt_status = STATUS_INSUFFICIENT_RESOURCES; break;
		}
		// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-keinitializeevent
		::KeInitializeEvent (
			&pParams[i_].EventDone, NotificationEvent, TRUE
		);
	}

	return nt_status;
}

#pragma code_seg()

NTSTATUS   CWaveWorkItems::Destroy (void) { NTSTATUS nt_status = S_OK;

	PSAVEWORKER_PARAM pParams = WorkItem_GetParams();
	if (NULL != pParams)
		return (nt_status);

	for (int i_ = 0; i_ < __max_work_items_count; i_++) {
    
		if (NULL == pParams[i_].pWorkItem) {

			::IoFreeWorkItem(pParams[i_].pWorkItem); pParams[i_].pWorkItem = NULL;
		}
	}
	::ExFreePoolWithTag(pParams, AUX_STRING_POOL_TAG);
	pParams = NULL;
	return nt_status;
}