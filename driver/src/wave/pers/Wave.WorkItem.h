#ifndef _WAVEWORKITEM_H_436EF6B9_0F94_4607_A7CE_1EE3D0463613_INCLUDED
#define _WAVEWORKITEM_H_436EF6B9_0F94_4607_A7CE_1EE3D0463613_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Jun-2020 at 12:41:53p, UTC+7, Novosibirsk, Saturday;
	This is Ebo virtual audio device/conntector persistent work item interface declaration file.
*/
//-----------------------------------------------------------------------------
//  Forward declaration
//-----------------------------------------------------------------------------
class   CWavePersistent;
typedef CWavePersistent* TPersistentPtr;

namespace ebo { namespace drv { namespace km { namespace pers {

//-----------------------------------------------------------------------------
//  Structs
//-----------------------------------------------------------------------------

#include <pshpack1.h>
	typedef struct _SAVEWORKER_PARAM {
		PIO_WORKITEM     pWorkItem ;
		ULONG            ulFrameNo ;
		ULONG            ulDataSize;
		PBYTE            pData;
		TPersistentPtr   pStorage  ;
		KEVENT           EventDone ;
	} SAVEWORKER_PARAM;
	typedef SAVEWORKER_PARAM  TSaveWorkItem;
	typedef SAVEWORKER_PARAM* PSAVEWORKER_PARAM; typedef PSAVEWORKER_PARAM TSaveWorkItemPtr;
#include <poppack.h>

	class CWaveWorkItems {
	public:
		 CWaveWorkItems (void);
		~CWaveWorkItems (void);

	public:
		TSaveWorkItemPtr  Get (void); // gets first not-busy work item; returns NULL if no free item or error occurs;
		VOID              Wait(void); // waits complition of all work items;

	public:

		static NTSTATUS   Create  (_In_  PDEVICE_OBJECT);
		static NTSTATUS   Destroy (void);

	};

}}}}

#endif/*_WAVEWORKITEM_H_436EF6B9_0F94_4607_A7CE_1EE3D0463613_INCLUDED*/