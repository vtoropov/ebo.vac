/*++
	Copyright (c) 1997-2000  Microsoft Corporation All Rights Reserved;
    To save the playback data to disk, this class maintains a circular data
    buffer, associated frame structures and worker items to save frames to
    disk.
    Each frame structure represents a portion of buffer. When that portion
    of frame is full, a workitem is scheduled to save it to disk.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver project on 12-Jun-2020 at 8:09:45a, UTC+7, Novosibirsk, Friday;
--*/
#include "StdAfx.h"
//#include "common_guid.h"
#include "Wave.Pers.h"
#pragma warning (disable : 4127)
#pragma warning (disable : 26165)

//=============================================================================
// Defines
//=============================================================================
#define RIFF_TAG                0x46464952;
#define WAVE_TAG                0x45564157;
#define FMT__TAG                0x20746D66;
#define DATA_TAG                0x61746164;

#define DEFAULT_FRAME_COUNT     2
#define DEFAULT_FRAME_SIZE      4 * PAGE_SIZE
#define DEFAULT_BUFFER_SIZE     DEFAULT_FRAME_SIZE * DEFAULT_FRAME_COUNT

#define DEFAULT_FILE_NAME       L"\\DosDevices\\C:\\stream"

#define MAX_WORKER_ITEM_COUNT   15

//=============================================================================
// Statics
//=============================================================================
ULONG              CWavePersistent::m_ulStreamId    = 0;
PDEVICE_OBJECT     CWavePersistent::m_pDeviceObject = NULL;

#pragma code_seg("PAGE")
//=============================================================================
// CWavePersistent
//=============================================================================

CWavePersistent:: CWavePersistent(VOID) :  
	m_pDataBuffer (NULL),
	m_ulFrameCount(DEFAULT_FRAME_COUNT),
	m_ulBufferSize(DEFAULT_BUFFER_SIZE),
	m_ulFrameSize (DEFAULT_FRAME_SIZE ),
	m_ulBufferPtr (0),
	m_ulFramePtr  (0),
	m_fFrameUsed    (NULL ),
	m_pFilePtr      (NULL ),
	m_fWriteDisabled(FALSE),
	m_bInitialized  (FALSE)
{

	PAGED_CODE();

	m_waveFormat                  = NULL;
	m_FileHeader.dwRiff           = RIFF_TAG;
	m_FileHeader.dwFileSize       = 0;
	m_FileHeader.dwWave           = WAVE_TAG;
	m_FileHeader.dwFormat         = FMT__TAG;
	m_FileHeader.dwFormatLength   = sizeof(WAVEFORMATEX);

	m_DataHeader.dwData           = DATA_TAG;
	m_DataHeader.dwDataLength     = 0;

	m_ulStreamId++;

	NTSTATUS nt_status = CWaveWorkItems::Create(GetDeviceObject()); nt_status;
}

//=============================================================================
CWavePersistent::~CWavePersistent(VOID)
{
	PAGED_CODE();
	//
	// Updates the wave header in data file with real file size.
	//
	if(m_pFilePtr) {
		m_FileHeader.dwFileSize    = (DWORD) m_pFilePtr->QuadPart - 2 * sizeof(DWORD);
		m_DataHeader.dwDataLength  = (DWORD) m_pFilePtr->QuadPart;
		m_DataHeader.dwDataLength -=  sizeof(m_FileHeader);
		m_DataHeader.dwDataLength -=  sizeof(m_DataHeader);
		m_DataHeader.dwDataLength -=  m_FileHeader.dwFormatLength;
							
		if (STATUS_SUCCESS == ::KeWaitForSingleObject ( &m_FileSync, Executive, KernelMode, FALSE, NULL )) {

			COptions_Write opt_write;
			NTSTATUS nt_status = m_wav_file.Open(opt_write);

			if (NT_SUCCESS(nt_status)) {

				FileWriteHeader();
				m_wav_file.Close();
			}

			::KeReleaseMutex(&m_FileSync, FALSE);
		}
	}

	if (m_waveFormat)      { ::ExFreePoolWithTag(m_waveFormat     , AUX_STRING_POOL_TAG); }
	if (m_fFrameUsed)      { ::ExFreePoolWithTag(m_fFrameUsed     , AUX_STRING_POOL_TAG); } // NOTE : Do not release m_pFilePtr;
	if (m_pDataBuffer)     { ::ExFreePoolWithTag(m_pDataBuffer    , AUX_STRING_POOL_TAG); }

	CWaveWorkItems::Destroy();
}

//=============================================================================
VOID
CWavePersistent::Disable ( BOOL fDisable )
{
	PAGED_CODE();

	m_fWriteDisabled = fDisable;
}

//=============================================================================

//=============================================================================
NTSTATUS
CWavePersistent::FileWriteHeader(VOID) {
	PAGED_CODE();

	NTSTATUS nt_status = STATUS_SUCCESS;

	if (m_wav_file.Is() == false)
		return (nt_status = STATUS_INVALID_HANDLE);

	if (NULL == m_waveFormat)
		return (nt_status = STATUS_INSUFFICIENT_RESOURCES);

	m_pFilePtr->QuadPart = 0;

	m_FileHeader.dwFormatLength = (m_waveFormat->wFormatTag == WAVE_FORMAT_PCM) ?
	sizeof( PCMWAVEFORMAT ) :
	sizeof( WAVEFORMATEX  ) + m_waveFormat->cbSize;

	nt_status = m_wav_file.Write(
		&m_FileHeader, sizeof(m_FileHeader), m_pFilePtr
	);
	if (FAILED(nt_status))
		return nt_status;

	nt_status = m_wav_file.Write(
		m_waveFormat, m_FileHeader.dwFormatLength, m_pFilePtr
	);
	if (FAILED(nt_status))
		return nt_status;

	nt_status = m_wav_file.Write(
		&m_DataHeader, sizeof(m_DataHeader), m_pFilePtr
	);

	return nt_status;
}
#pragma code_seg()

//=============================================================================

#pragma code_seg("PAGE")
NTSTATUS
CWavePersistent::Initialize ( VOID ) {
	PAGED_CODE();

	NTSTATUS    nt_status = STATUS_SUCCESS;
	WCHAR       szTemp[MAX_PATH] = {0};
	size_t      cLen = 0;

	//
	// Allocate data file name.
	//
	::RtlStringCchPrintfW(szTemp, MAX_PATH, L"%s_%d.wav", DEFAULT_FILE_NAME, m_ulStreamId);
	nt_status = ::RtlStringCchLengthW (szTemp, sizeof(szTemp)/sizeof(szTemp[0]), &cLen);

	if (NT_SUCCESS(nt_status)) {
		m_wav_file.Path().FileName() = szTemp;
	}

	//
	// Allocate memory for data buffer.
	//
	if (NT_SUCCESS(nt_status)) {

		m_pDataBuffer = (PBYTE)
		::ExAllocatePoolWithTag (
			NonPagedPool, m_ulBufferSize, AUX_STRING_POOL_TAG
		);

		if (NULL == m_pDataBuffer) {
			nt_status = STATUS_INSUFFICIENT_RESOURCES;
		}
	}
	//
	// Allocate memory for frame usage flags and m_pFilePtr.
	//
	if (NT_SUCCESS(nt_status)) {

		m_fFrameUsed = (PBOOL)
		::ExAllocatePoolWithTag (
			NonPagedPool, m_ulFrameCount * sizeof(BOOL) + sizeof(LARGE_INTEGER), AUX_STRING_POOL_TAG
		);

		if (NULL == m_fFrameUsed) {
			nt_status = STATUS_INSUFFICIENT_RESOURCES;
		}
	}
	
	::KeInitializeSpinLock ( &m_FrameInUseSpinLock ) ; // Initialize the spinlock to synchronize access to the frames
	::KeInitializeMutex    ( &m_FileSync, 1 ) ;        // Initialize the file mutex
	// Open the data file.
	if (NT_SUCCESS(nt_status)) {
		// m_fFrameUsed has additional memory to hold m_pFilePtr
		m_pFilePtr = (PLARGE_INTEGER)
		(((PBYTE) m_fFrameUsed) + m_ulFrameCount * sizeof(BOOL));
		::RtlZeroMemory(m_fFrameUsed, m_ulFrameCount * sizeof(BOOL) + sizeof(LARGE_INTEGER));

		m_bInitialized = TRUE;

		// Write wave header information to data file.
		nt_status = ::KeWaitForSingleObject (
			&m_FileSync, Executive, KernelMode, FALSE, NULL
		);

		if (STATUS_SUCCESS == nt_status) {

			COptions_Write opt_write;
			nt_status = this->m_wav_file.Open(opt_write);

			if (NT_SUCCESS(nt_status)) {
				nt_status = this->FileWriteHeader();
				nt_status = this->m_wav_file.Close();
			}

			::KeReleaseMutex( &m_FileSync, FALSE );
		}
	}

	return nt_status;
}

//=============================================================================

IO_WORKITEM_ROUTINE SaveFrameWorkerCallback;

VOID
SaveFrameWorkerCallback (
	PDEVICE_OBJECT, IN  PVOID  pContext
)
{
	PAGED_CODE();

	ASSERT(pContext);

	TSaveWorkItemPtr  pItem = reinterpret_cast<TSaveWorkItemPtr>(pContext);
	TPersistentPtr    pPers = NULL;

	if (NULL == pItem) {
		//
		// This is completely unexpected, assert here.
		//
		ASSERT(pItem); return;
	}
	NTSTATUS nt_status = S_OK;

	ASSERT(pItem->pStorage);
	ASSERT(pItem->pStorage->m_fFrameUsed);

	if (pItem->pWorkItem) {

		pPers = pItem->pStorage;

		if (STATUS_SUCCESS == ::KeWaitForSingleObject (&pPers->m_FileSync, Executive, KernelMode, FALSE, NULL)) {

			COptions_Write opt_write;
			nt_status = pPers->m_wav_file.Open(opt_write);

			if (NT_SUCCESS(nt_status)) {

				LARGE_INTEGER l_shift = {0};

				nt_status = pPers->m_wav_file.Write(pItem->pData, pItem->ulDataSize, &l_shift);
				nt_status = pPers->m_wav_file.Close();
			}
			InterlockedExchange( (LONG *)&(pPers->m_fFrameUsed[pItem->ulFrameNo]), FALSE );

			::KeReleaseMutex( &pPers->m_FileSync, FALSE );
		}
	}

	::KeSetEvent(&pItem->EventDone, 0, FALSE);
}

//=============================================================================
NTSTATUS
CWavePersistent::SetDataFormat (
	IN PKSDATAFORMAT   pDataFormat
)
{
	PAGED_CODE();
	NTSTATUS nt_status = STATUS_SUCCESS;
 
	ASSERT(pDataFormat);

	PWAVEFORMATEX pwfx = NULL;

	if (IsEqualGUIDAligned(pDataFormat->Specifier, KSDATAFORMAT_SPECIFIER_DSOUND)) {
		pwfx =
		&(((PKSDATAFORMAT_DSOUND) pDataFormat)->BufferDesc.WaveFormatEx);
	}
	else if (IsEqualGUIDAligned(pDataFormat->Specifier, KSDATAFORMAT_SPECIFIER_WAVEFORMATEX)) {
		pwfx = &((PKSDATAFORMAT_WAVEFORMATEX) pDataFormat)->WaveFormatEx;
	}

	if (pwfx) {
		// Free the previously allocated waveformat
		if (m_waveFormat) {
			ExFreePoolWithTag(m_waveFormat, AUX_STRING_POOL_TAG);
		}

		m_waveFormat = (PWAVEFORMATEX)
		::ExAllocatePoolWithTag (
			NonPagedPool,
			(pwfx->wFormatTag == WAVE_FORMAT_PCM) ? sizeof( PCMWAVEFORMAT ) : sizeof( WAVEFORMATEX  ) + pwfx->cbSize,
			AUX_STRING_POOL_TAG
		);

		if(m_waveFormat) {
			RtlCopyMemory(
				m_waveFormat, pwfx,
				(pwfx->wFormatTag == WAVE_FORMAT_PCM) ? sizeof( PCMWAVEFORMAT ) : sizeof( WAVEFORMATEX ) + pwfx->cbSize
			);
		}
		else {
			nt_status = STATUS_INSUFFICIENT_RESOURCES;
		}
	}
	return nt_status;
}

//=============================================================================
VOID
CWavePersistent::ReadData (
	_Inout_updates_bytes_all_(ulByteCount)  PBYTE   pBuffer,
	_In_                                    ULONG   ulByteCount
)
{
	UNREFERENCED_PARAMETER(pBuffer);
	UNREFERENCED_PARAMETER(ulByteCount);

	PAGED_CODE();

	// Not implemented yet.
}

//=============================================================================
#pragma code_seg()
VOID
CWavePersistent::SaveFrame (
	IN ULONG   ulFrameNo,
	IN ULONG   ulDataSize
)
{
	ebo::drv::km::pers::TSaveWorkItemPtr pItem = m_wrk_items.Get();
	if (NULL == pItem)
		return;

	pItem->pStorage   = this;
	pItem->ulFrameNo  = ulFrameNo ;
	pItem->ulDataSize = ulDataSize;
	pItem->pData      = m_pDataBuffer + ulFrameNo * m_ulFrameSize;

	::KeResetEvent(&pItem->EventDone);

	::IoQueueWorkItem(
		pItem->pWorkItem, ::SaveFrameWorkerCallback, CriticalWorkQueue, pItem
	);

	m_wrk_items.Wait();
}

//=============================================================================
VOID
CWavePersistent::WriteData (
	_In_reads_bytes_(ulByteCount)   PBYTE   pBuffer,
	_In_                            ULONG   ulByteCount
)
{
	ASSERT(pBuffer);

	BOOL    fSaveFrame     = FALSE;
	ULONG   ulSaveFramePtr = 0;
	//
	// If stream writing is disabled, then exit.
	//
	if (m_fWriteDisabled) {
		return;
	}

	if ( 0 == ulByteCount ) {
		return;
	}

	// Check to see if this frame is available.
	::KeAcquireSpinLockAtDpcLevel( &m_FrameInUseSpinLock );

	if (!m_fFrameUsed[m_ulFramePtr]) {
		::KeReleaseSpinLockFromDpcLevel( &m_FrameInUseSpinLock );

		ULONG ulWriteBytes = ulByteCount;

		if( (m_ulBufferSize - m_ulBufferPtr) < ulWriteBytes ) {
			ulWriteBytes = m_ulBufferSize - m_ulBufferPtr;
		}

		RtlCopyMemory(m_pDataBuffer + m_ulBufferPtr, pBuffer, ulWriteBytes);
		m_ulBufferPtr += ulWriteBytes;

		// Check to see if we need to save this frame
		if (m_ulBufferPtr >= ((m_ulFramePtr + 1) * m_ulFrameSize)) {
			fSaveFrame = TRUE;
		}

		// Loop the buffer, if we reached the end.
		if (m_ulBufferPtr == m_ulBufferSize) {
			fSaveFrame = TRUE;
			m_ulBufferPtr = 0;
		}

		if (fSaveFrame) {
			InterlockedExchange( (LONG *)&(m_fFrameUsed[m_ulFramePtr]), TRUE );
			ulSaveFramePtr = m_ulFramePtr;
			m_ulFramePtr  = (m_ulFramePtr + 1) % m_ulFrameCount;
		}

		// Write the left over if the next frame is available.
		if (ulWriteBytes != ulByteCount) {
			::KeAcquireSpinLockAtDpcLevel( &m_FrameInUseSpinLock );

			if (!m_fFrameUsed[m_ulFramePtr]) {
				::KeReleaseSpinLockFromDpcLevel( &m_FrameInUseSpinLock );
				RtlCopyMemory (
					m_pDataBuffer + m_ulBufferPtr, pBuffer + ulWriteBytes, ulByteCount - ulWriteBytes
				);
				m_ulBufferPtr += ulByteCount - ulWriteBytes;
			}
			else {
				::KeReleaseSpinLockFromDpcLevel( &m_FrameInUseSpinLock );
			}
		}

		if (fSaveFrame) {
			this->SaveFrame(ulSaveFramePtr, m_ulFrameSize);
		}
	}
	else {
		::KeReleaseSpinLockFromDpcLevel( &m_FrameInUseSpinLock );
	}
}

/////////////////////////////////////////////////////////////////////////////
#pragma code_seg("PAGE")
NTSTATUS
CWavePersistent::SetDeviceObject ( IN  PDEVICE_OBJECT DeviceObject ) {
	PAGED_CODE();

	ASSERT(DeviceObject);

	NTSTATUS  nt_status = STATUS_SUCCESS;

	m_pDeviceObject = DeviceObject;
	return nt_status;
}

PDEVICE_OBJECT
CWavePersistent::GetDeviceObject ( VOID ) {
	PAGED_CODE();

	return m_pDeviceObject;
}
#pragma code_seg()