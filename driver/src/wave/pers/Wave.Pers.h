/*++
	Copyright (c) 1997-2000  Microsoft Corporation All Rights Reserved
	This class supplies services to save data to disk.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver project on 12-Jun-2020 at 7:50:24a, UTC+7, Novosibirsk, Friday;
--*/
#ifndef _PERSISTENT_H_4C596890_BEAF_45EB_A5C3_FA4027D72B70_INCLUDED
#define _PERSISTENT_H_4C596890_BEAF_45EB_A5C3_FA4027D72B70_INCLUDED

#include "Wave.WorkItem.h"
#include "fs.generic.file.h"

//-----------------------------------------------------------------------------
//  Structs
//-----------------------------------------------------------------------------
// wave file header.
#include <pshpack1.h>
typedef struct _OUTPUT_FILE_HEADER
{
    DWORD           dwRiff;
    DWORD           dwFileSize;
    DWORD           dwWave;
    DWORD           dwFormat;
    DWORD           dwFormatLength;
} OUTPUT_FILE_HEADER;
typedef OUTPUT_FILE_HEADER* POUTPUT_FILE_HEADER;

typedef struct _OUTPUT_DATA_HEADER
{
    DWORD           dwData;
    DWORD           dwDataLength;
} OUTPUT_DATA_HEADER;
typedef OUTPUT_DATA_HEADER* POUTPUT_DATA_HEADER;

#include <poppack.h>

//-----------------------------------------------------------------------------
//  Classes
//-----------------------------------------------------------------------------
using ebo::drv::km::pers::CWaveWorkItems;
using ebo::drv::km::pers::TSaveWorkItemPtr;
using ebo::drv::km::file_sys::CGenericFile;
using ebo::drv::km::file_sys::COptions_Write;
///////////////////////////////////////////////////////////////////////////////

IO_WORKITEM_ROUTINE SaveFrameWorkerCallback;

class CWavePersistent
{
	friend VOID            SaveFrameWorkerCallback ( PDEVICE_OBJECT, IN  PVOID  pContext );
protected:
	CGenericFile           m_wav_file;
    PBYTE                  m_pDataBuffer;        // Data buffer.
    ULONG                  m_ulBufferSize;       // Total buffer size.

    ULONG                  m_ulFramePtr;         // Current Frame.
    ULONG                  m_ulFrameCount;       // Frame count.
    ULONG                  m_ulFrameSize;
    ULONG                  m_ulBufferPtr;        // Pointer in buffer.
    PBOOL                  m_fFrameUsed;         // Frame usage table.
    KSPIN_LOCK             m_FrameInUseSpinLock; // Spinlock for synch.
    KMUTEX                 m_FileSync;           // Synchronizes file access

    OUTPUT_FILE_HEADER     m_FileHeader;
    PWAVEFORMATEX          m_waveFormat;
    OUTPUT_DATA_HEADER     m_DataHeader;
    PLARGE_INTEGER         m_pFilePtr;

    static PDEVICE_OBJECT  m_pDeviceObject;
    static ULONG           m_ulStreamId;
    BOOL                   m_fWriteDisabled;

    BOOL                   m_bInitialized;
	CWaveWorkItems         m_wrk_items;

public:
     CWavePersistent(VOID);
    ~CWavePersistent(VOID);

	VOID                   Disable          ( BOOL fDisable );
    NTSTATUS               Initialize       ( VOID );
	VOID                   ReadData         (
        _Inout_updates_bytes_all_(ulByteCount)  PBYTE   pBuffer,
        _In_                                    ULONG   ulByteCount
    );
    NTSTATUS               SetDataFormat    ( IN  PKSDATAFORMAT pDataFormat );
	VOID                   WaitAllWorkItems ( VOID );
	VOID                   WriteData        (
        _In_reads_bytes_(ulByteCount)   PBYTE   pBuffer,
        _In_                            ULONG   ulByteCount
    );

public:
	static NTSTATUS        SetDeviceObject  ( IN  PDEVICE_OBJECT);
	static PDEVICE_OBJECT  GetDeviceObject  ( VOID );

private:
    NTSTATUS               FileWriteHeader ( VOID );
	VOID                   SaveFrame ( IN  ULONG ulFrameNo, IN  ULONG ulDataSize);
};

typedef CWavePersistent* TPersistentPtr;
#endif/*_PERSISTENT_H_4C596890_BEAF_45EB_A5C3_FA4027D72B70_INCLUDED*/
