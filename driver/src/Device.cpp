/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Jun-2020 at 7:32:59a, UTC+7, Novosibirsk, Friday;
	This is Ebo virtual audio cable (VAC) device interface implementation file;
*/
#include "StdAfx.h"
#include "Device.h"
#include "Adapter.Iface.h"

using namespace ebo::drv::km;
//=============================================================================
CDevice:: CDevice(void) {}
CDevice::~CDevice(void) {}
//=============================================================================

//=============================================================================
/*++
Routine Description:
  This function is called by the operating system when the device is 
  started.
  It is responsible for starting the miniports.  This code is specific to    
  the adapter because it calls out miniports for functions that are specific 
  to the adapter.                                                            

Arguments:
  pDeviceObject - pointer to the driver object
  pIrp          - pointer to the irp 
  pResourceList - pointer to the resource list assigned by PnP manager

Return Value:
  NT status code.
--*/
NTSTATUS   CDevice::Start(_In_ PDEVICE_OBJECT pDeviceObject, _In_ PIRP pIrp, _In_ PRESOURCELIST pResourceList) {
	__unused(pResourceList);
	PAGED_CODE();

	ASSERT(pDeviceObject);
	ASSERT(pIrp);
	ASSERT(pResourceList);

	/*PUNKNOWN        pUnknownTopology = NULL;
	PUNKNOWN        pUnknownWave     = NULL;
	PADAPTERCOMMON  pAdapterCommon   = NULL;
	PUNKNOWN        pUnknownCommon   = NULL;*/

	NTSTATUS nt_status = STATUS_SUCCESS;
	return   nt_status;
}
//=============================================================================

