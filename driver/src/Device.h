/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Jun-2020 at 7:22:57a, UTC+7, Novosibirsk, Friday;
	This is Ebo virtual audio cable (VAC) device interface declaration file;
*/
#ifndef _DEVICE_H_3596CD57_992F_4EF1_AF71_4E445C046EFC_INCLUDED
#define _DEVICE_H_3596CD57_992F_4EF1_AF71_4E445C046EFC_INCLUDED

namespace ebo { namespace drv { namespace km {

	class CDevice {
	public:
		 CDevice (void);
		~CDevice (void);

	public:
		static NTSTATUS Start(_In_ PDEVICE_OBJECT, _In_ PIRP Irp, _In_ PRESOURCELIST);
	};

}}}

#endif/*_DEVICE_H_3596CD57_992F_4EF1_AF71_4E445C046EFC_INCLUDED*/