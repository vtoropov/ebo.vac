/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Jun-2020 at 6:08:51a, UTC+7, Novosibirsk, Saturday;
	This is Ebo virtual audio device (VAC) connector interface declaration file;
*/
#ifndef _CONNECT_H_51F266E9_82CE_45FB_833F_17FE48C87D23_INCLUDED
#define _CONNECT_H_51F266E9_82CE_45FB_833F_17FE48C87D23_INCLUDED
//=============================================================================
// Typedefs
//=============================================================================

// Connection table for registering topology/wave bridge connection
typedef struct _PHYSICALCONNECTIONTABLE
{
	ULONG       ulTopologyIn;
	ULONG       ulTopologyOut;
	ULONG       ulWaveIn;
	ULONG       ulWaveOut;
} PHYSICALCONNECTIONTABLE, *PPHYSICALCONNECTIONTABLE;

// This is the structure of the portclass FDO device extension Nt has created
// for us.  We keep the adapter common object here.
struct IAdapterCommon;
typedef struct _PortClassDeviceContext              // 32       64      Byte offsets for 32 and 64 bit architectures
{
	ULONG_PTR m_pulReserved1[2];                    // 0-7      0-15    First two pointers are reserved.
	PDEVICE_OBJECT m_DoNotUsePhysicalDeviceObject;  // 8-11     16-23   Reserved pointer to our Physical Device Object (PDO).
	PVOID m_pvReserved2;                            // 12-15    24-31   Reserved pointer to our Start Device function.
	PVOID m_pvReserved3;                            // 16-19    32-39   "Out Memory" according to DDK.  
	IAdapterCommon* m_pCommon;                      // 20-23    40-47   Pointer to our adapter common object.
	PVOID m_pvUnused1;                              // 24-27    48-55   Unused space.
	PVOID m_pvUnused2;                              // 28-31    56-63   Unused space.

													// Anything after above line should not be used.
													// This actually goes on for (64*sizeof(ULONG_PTR)) but it is all opaque.
} PortClassDeviceContext;

//=============================================================================
// Externs
//=============================================================================

extern PHYSICALCONNECTIONTABLE TopologyPhysicalConnections; // Physical connection table. Defined in mintopo.cpp for each sample

extern NTSTATUS PropertyHandler_Topology ( IN  PPCPROPERTY_REQUES  PropertyRequest ); // Generic topology handler
extern NTSTATUS PropertyHandler_Wave     ( IN  PPCPROPERTY_REQUEST PropertyRequest );

// Default WaveFilter automation table.
// Handles the GeneralComponentId request.
extern NTSTATUS PropertyHandler_WaveFilter ( IN PPCPROPERTY_REQUEST PropertyRequest );


#endif/*_CONNECT_H_51F266E9_82CE_45FB_833F_17FE48C87D23_INCLUDED*/