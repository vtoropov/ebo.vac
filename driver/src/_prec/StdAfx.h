#ifndef _STDAFX_H_4BE97360_F93F_ABCD_BBA9_1131455A19E1_INCLUDED
#define _STDAFX_H_4BE97360_F93F_ABCD_BBA9_1131455A19E1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Jun-2016 at 11:44:04, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian file system event log minifilter driver precompiled headers definition file.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC project on 12-Jun-2020 at 3:41:23a, UTC+7, Novosibirsk, Friday;
*/

#ifndef WINVER                 // Specifies that the minimum required platform is Windows Vista.
#define WINVER         0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT           // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINNT   0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINDOWS         // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINDOWS 0x0600  // Change this to the appropriate value to target other platforms.
#endif

#include <portcls.h>
#include <stdunk.h>
#include <Ntstrsafe.h>
#include <ksdebug.h>

#pragma prefast(disable:__WARNING_ENCODE_MEMBER_FUNCTION_POINTER, "Not valid for kernel mode drivers")
/////////////////////////////////////////////////////////////////////////////
//
// macro utilities
//

#define FlagOnAll( F, T )  \
    (FlagOn( F, T ) == T)

#define SetFlagInterlocked(_ptrFlags,_flagToSet) \
    ((VOID)InterlockedOr(((volatile LONG*)(_ptrFlags)),_flagToSet))

#define PAGE_CODE_EX(_return)                            \
		if (PASSIVE_LEVEL != KeGetCurrentIrql()) {       \
			return _return;                              \
		}

/////////////////////////////////////////////////////////////////////////////
//
// constants and structures
//

#define __unused(x) (void)(x)
#define __log_dbg_parse_names  0x00000001
#define __extern_c extern "C"

#define AUX_STRING_POOL_TAG  'auxs'
typedef UNICODE_STRING w_string;

#ifndef S_OK
#define S_OK STATUS_SUCCESS
#endif
#ifndef SUCCEEDED
#define SUCCEEDED(_nt_status) (S_OK == _nt_status)
#endif
#ifndef FAILED
#define FAILED(_nt_status) (S_OK != _nt_status)
#endif

#endif/*_STDAFX_H_4BE97360_F93F_ABCD_BBA9_1131455A19E1_INCLUDED*/