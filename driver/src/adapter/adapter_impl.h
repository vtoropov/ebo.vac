#ifndef _ADAPTER_IMPL_H_AD9A29E7_9D67_4E8C_BB7B_AA3510448584_INCLUDED
#define _ADAPTER_IMPL_H_AD9A29E7_9D67_4E8C_BB7B_AA3510448584_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Jun-2020 at 9:35:24p, UTC+7, Novosibirsk, Friday;
	This is Ebo VAC driver common adapter COM-interface inheritance declaration file.
*/
#include "adapter_iface.h"

namespace vac { namespace _impl {

	class CAdapter_Base : public CUnknown {
	protected:
		 CAdapter_Base (void);
		~CAdapter_Base (void);
	};

}}

#endif/*_ADAPTER_IMPL_H_AD9A29E7_9D67_4E8C_BB7B_AA3510448584_INCLUDED*/