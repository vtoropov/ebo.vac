/*++
Copyright (c) 1997-2000  Microsoft Corporation All Rights Reserved
Module Name:
    adapter_iface.h
Abstract:
    CAdapterCommon class declaration.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver project on 12-Jun-2020 at 5:59:05a, UTC+7, Novosibirsk, Friday;
--*/

#ifndef _ADAPTER_IFACE_H_06110639_6CF6_4234_B7C9_F255875593BC_INCLUDED
#define _ADAPTER_IFACE_H_06110639_6CF6_4234_B7C9_F255875593BC_INCLUDED

//=============================================================================
// Defines
//=============================================================================

DEFINE_GUID(IID_IAdapterCommon,
0x7eda2950, 0xbf9f, 0x11d0, 0x87, 0x1f, 0x0, 0xa0, 0xc9, 0x11, 0xb5, 0x44);

//=============================================================================
// Interfaces
//=============================================================================

///////////////////////////////////////////////////////////////////////////////
// IAdapterCommon
//
interface  __declspec(novtable) IAdapterCommon : public IUnknown
{
	virtual __declspec(nothrow) NTSTATUS        __stdcall Init(PDEVICE_OBJECT ) PURE;
	virtual __declspec(nothrow) PDEVICE_OBJECT  __stdcall GetDeviceObject(void) PURE;
	virtual __declspec(nothrow) VOID            __stdcall SetWaveServiceGroup(PSERVICEGROUP) PURE;
	virtual __declspec(nothrow) NTSTATUS        __stdcall InstantiateDevices (void) PURE;
	virtual __declspec(nothrow) NTSTATUS        __stdcall UninstantiateDevices (void) PURE;
	virtual __declspec(nothrow) NTSTATUS        __stdcall Plugin (void) PURE;
	virtual __declspec(nothrow) NTSTATUS        __stdcall Unplug (void) PURE;
	virtual __declspec(nothrow) PUNKNOWN*       __stdcall WavePortDriverDest(void) PURE;
	virtual __declspec(nothrow) BOOL            __stdcall bDevSpecificRead  (void) PURE;
	virtual __declspec(nothrow) VOID            __stdcall bDevSpecificWrite (BOOL) PURE;
	virtual __declspec(nothrow) INT             __stdcall iDevSpecificRead  (void) PURE;
	virtual __declspec(nothrow) VOID            __stdcall iDevSpecificWrite (INT ) PURE;
	virtual __declspec(nothrow) UINT            __stdcall uiDevSpecificRead (void) PURE;
	virtual __declspec(nothrow) VOID            __stdcall uiDevSpecificWrite(UINT) PURE;
	virtual __declspec(nothrow) BOOL            __stdcall MixerMuteRead     (ULONG _ndx) PURE;
	virtual __declspec(nothrow) VOID            __stdcall MixerMuteWrite    (ULONG _ndx, BOOL _b_value) PURE;
	virtual __declspec(nothrow) ULONG           __stdcall MixerMuxRead      (void) PURE;
	virtual __declspec(nothrow) VOID            __stdcall MixerMuxWrite     (ULONG _ndx) PURE;
	virtual __declspec(nothrow) LONG            __stdcall MixerVolumeRead   (ULONG _ndx, LONG _channel) PURE;
	virtual __declspec(nothrow) VOID            __stdcall MixerVolumeWrite  (ULONG _ndx, LONG _channel, LONG _value) PURE;
	virtual __declspec(nothrow) VOID            __stdcall MixerReset (void) PURE;
	virtual __declspec(nothrow) BOOL            __stdcall IsInstantiated    (void) PURE;
	virtual __declspec(nothrow) BOOL            __stdcall IsPluggedIn       (void) PURE;
	virtual __declspec(nothrow) NTSTATUS        __stdcall SetInstantiateWorkItem (__drv_aliasesMem PIO_WORKITEM) PURE;
	virtual __declspec(nothrow) NTSTATUS        __stdcall FreeInstantiateWorkItem(void) PURE;
 
};
typedef IAdapterCommon* IAdapterCommonPtr;

//=============================================================================
// Function Prototypes
//=============================================================================
NTSTATUS
NewAdapterCommon
( 
	OUT PUNKNOWN*  Unknown,
	IN  REFCLSID,
	IN  PUNKNOWN   UnknownOuter OPTIONAL,
	_When_((PoolType & NonPagedPoolMustSucceed) != 0,
		__drv_reportError("Must succeed pool allocations are forbidden. "
		                  "Allocation failures cause a system crash."))
	IN  POOL_TYPE  PoolType 
);

#endif  //_ADAPTER_IFACE_H_06110639_6CF6_4234_B7C9_F255875593BC_INCLUDED