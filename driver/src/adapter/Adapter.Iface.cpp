/*++
Copyright (c) 1997-2000  Microsoft Corporation All Rights Reserved
Module Name:
    common.cpp
Abstract:
    Implementation of the AdapterCommon class.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver project on 12-Jun-2020 at 6:50:06a, UTC+7, Novosibirsk, Friday;
--*/
#include "StdAfx.h"
#include "common_guid.h"
#include "Adapter.Iface.h"
#include "device.h"
#include "persistent.h"
#pragma warning(disable : 4127)
#pragma warning(disable : 28204) // disables the warning due to annotation mismatches between DDK headers for QueryInterface();

#define HNS_PER_MS              10000
#define INSTANTIATE_INTERVAL_MS 10000   // 10 seconds between instantiate and uninstantiate

//-----------------------------------------------------------------------------
// Externals
//-----------------------------------------------------------------------------

PSAVEWORKER_PARAM  CPersistent::m_pWorkItems    = NULL;
PDEVICE_OBJECT     CPersistent::m_pDeviceObject = NULL;

typedef
NTSTATUS
(*PMSVADMINIPORTCREATE)
(
	_Out_       PUNKNOWN *,
	_In_        REFCLSID,
	_In_opt_    PUNKNOWN,
	_In_        POOL_TYPE
);

NTSTATUS
CreateMiniportWaveCyclicMSVAD
( 
	OUT PUNKNOWN*,
	IN  REFCLSID ,
	IN  PUNKNOWN ,
	_When_((PoolType & NonPagedPoolMustSucceed) != 0,
		__drv_reportError("Must succeed pool allocations are forbidden. "
		                  "Allocation failures cause a system crash"))
	IN  POOL_TYPE PoolType
);

NTSTATUS
CreateMiniportTopologyMSVAD
( 
	OUT PUNKNOWN *,
	IN  REFCLSID,
	IN  PUNKNOWN,
	_When_((PoolType & NonPagedPoolMustSucceed) != 0,
		__drv_reportError("Must succeed pool allocations are forbidden. "
		                  "Allocation failures cause a system crash"))
	IN  POOL_TYPE PoolType
);

//=============================================================================
// Helper Routines
//=============================================================================
//=============================================================================
/*++
Routine Description:
    This function creates and registers a subdevice consisting of a port       
    driver, a minport driver and a set of resources bound together.  It will   
    also optionally place a pointer to an interface on the port driver in a    
    specified location before initializing the port driver.  This is done so   
    that a common ISR can have access to the port driver during 
    initialization, when the ISR might fire.                                   

Arguments:
    DeviceObject     - pointer to the driver object
    Irp              - pointer to the irp object.
    Name             - name of the miniport. Passes to PcRegisterSubDevice
    PortClassId      - port class id. Passed to PcNewPort.
    MiniportClassId  - miniport class id. Passed to PcNewMiniport.
    MiniportCreate   - pointer to a miniport creation function. If NULL, 
                       PcNewMiniport is used.
    UnknownAdapter   - pointer to the adapter object. 
                       Used for initializing the port.
    ResourceList     - pointer to the resource list.
    PortInterfaceId  - GUID that represents the port interface.
    OutPortInterface - pointer to store the port interface
    OutPortUnknown   - pointer to store the unknown port interface.

Return Value:
    NT status code.
--*/
#pragma code_seg("PAGE")
NTSTATUS
InstallSubdevice
( 
	_In_        PDEVICE_OBJECT          DeviceObject,
	_In_opt_    PIRP                    Irp ,
	_In_        PWSTR                   Name,
	_In_        REFGUID                 PortClassId    ,
	_In_        REFGUID                 MiniportClassId,
	_In_opt_    PMSVADMINIPORTCREATE    MiniportCreate ,
	_In_opt_    PUNKNOWN                UnknownAdapter ,
	_In_opt_    PRESOURCELIST           ResourceList   ,
	_Out_opt_   PUNKNOWN *              OutPortUnknown ,
	_Out_opt_   PUNKNOWN *              OutMiniportUnknown
)
{
	PAGED_CODE();

	ASSERT(DeviceObject);
	ASSERT(Name);

	NTSTATUS    ntStatus = STATUS_SUCCESS;
	PPORT       port     = NULL;
	PUNKNOWN    miniport = NULL;
	DPF_ENTER(("[InstallSubDevice %S]", Name));
	//
	// Create the port driver object
	//
	ntStatus = PcNewPort(&port, PortClassId);
	//
	// Create the miniport object
	//
	if (NT_SUCCESS(ntStatus)) {
		if (MiniportCreate) {
			ntStatus =  MiniportCreate ( 
					&miniport,
					MiniportClassId,
					NULL,
					NonPagedPool 
				);
		}
		else {
			ntStatus = PcNewMiniport (
					(PMINIPORT *) &miniport, 
					MiniportClassId
				);
		}
	}
	//
	// Init the port driver and miniport in one go.
	//
	if (NT_SUCCESS(ntStatus)) {
#pragma warning(push)
		// IPort::Init's annotation on ResourceList requires it to be non-NULL.  However,
		// for dynamic devices, we may no longer have the resource list and this should
		// still succeed.
		//
#pragma warning(disable:6387)
		ntStatus =  port->Init ( 
				DeviceObject, Irp, miniport, UnknownAdapter, ResourceList 
			);
#pragma warning(pop)

		if (NT_SUCCESS(ntStatus)) {
			// Register the subdevice (port/miniport combination).
			//
			ntStatus =  PcRegisterSubdevice ( 
				DeviceObject, Name, port 
			);
		}
    }
	//
	// Deposit the port interfaces if it's needed.
	//
	if (NT_SUCCESS(ntStatus)) {
		if (OutPortUnknown) {

			ntStatus =  port->QueryInterface ( 
					IID_IUnknown, (PVOID *)OutPortUnknown
				);
		}

		if (OutMiniportUnknown) {
			ntStatus =  miniport->QueryInterface ( 
					IID_IUnknown, (PVOID *) OutMiniportUnknown 
				);
		}
	}

	if (port) {
		port->Release();
	}

	if (miniport) {
		miniport->Release();
	}

	return ntStatus;
}

//=============================================================================
#pragma code_seg("PAGE")
IO_WORKITEM_ROUTINE InstantiateTimerWorkRoutine;

VOID InstantiateTimerWorkRoutine (
	_In_ DEVICE_OBJECT *_DeviceObject, 
	_In_opt_ PVOID _Context
)
{
	PAGED_CODE();

	UNREFERENCED_PARAMETER(_DeviceObject);

	PADAPTERCOMMON  pCommon = (PADAPTERCOMMON) _Context;

	if (NULL == pCommon) {
		//
		// This is completely unexpected, assert here.
		//
		ASSERT(pCommon);
		return;
	}
	//
	// Loop through various states of instantiated and
	// plugged in.
	//
	if (pCommon->IsInstantiated() && pCommon->IsPluggedIn()) {
		pCommon->UninstantiateDevices();
	}
	else if (pCommon->IsInstantiated() && !pCommon->IsPluggedIn()) {
		pCommon->Plugin();
	}
	else if (!pCommon->IsInstantiated()) {
		pCommon->InstantiateDevices();
		pCommon->Unplug();
	}
	//
	// Free the work item that was allocated in the DPC.
	//
	pCommon->FreeInstantiateWorkItem();
}

//=============================================================================
/*++
Routine Description:
  Dpc routine. This simulates an interrupt service routine to simulate
  a jack plug/unplug.

Arguments:
  Dpc             - the Dpc object
  DeferredContext - Pointer to a caller-supplied context to be passed to
                    the DeferredRoutine when it is called
  SA1             - System argument 1
  SA2             - System argument 2

Return Value:
  NT status code.
--*/
#pragma code_seg()
KDEFERRED_ROUTINE InstantiateTimerNotify;

VOID InstantiateTimerNotify (
	IN  PKDPC   Dpc,
	IN  PVOID   DeferredContext,
	IN  PVOID   SA1,
	IN  PVOID   SA2
)
{
	UNREFERENCED_PARAMETER(Dpc);
	UNREFERENCED_PARAMETER(SA1);
	UNREFERENCED_PARAMETER(SA2);

	PIO_WORKITEM    pWorkItem = NULL;
	PADAPTERCOMMON  pCommon   = (PADAPTERCOMMON) DeferredContext;

	if (NULL == pCommon) {
		//
		// This is completely unexpected, assert here.
		//
		ASSERT(pCommon);
		return;
	}
	//
	// Queue a work item to run at PASSIVE_LEVEL so we can call
	// PortCls in order to register or unregister subdevices.
	//
	pWorkItem = IoAllocateWorkItem(pCommon->GetDeviceObject());
	if (NULL != pWorkItem) {
		//
		// Store the work item in the adapter common object and queue it.
		//
		if (NT_SUCCESS(pCommon->SetInstantiateWorkItem(pWorkItem))) {

			IoQueueWorkItem(pWorkItem, InstantiateTimerWorkRoutine, DelayedWorkQueue, DeferredContext);
		}
		else {
			//
			// If we failed to stash the work item in the common adapter object,
			// free it now or else we'll leak it.
			//
			IoFreeWorkItem(pWorkItem);
		}
	}
}

//=============================================================================
// Classes
//=============================================================================

///////////////////////////////////////////////////////////////////////////////
// CAdapterCommon
//   

class CAdapterCommon : 
    public IAdapterCommon,
    public IAdapterPowerManagement,
    public CUnknown    
{
private:
	PUNKNOWN             m_pPortWave    ;        // Port Wave Interface
	PUNKNOWN             m_pMiniportWave;        // Miniport Wave Interface
	PUNKNOWN             m_pPortTopology;        // Port Mixer Topology Interface
	PUNKNOWN             m_pMiniportTopology;    // Miniport Mixer Topology Interface
	PSERVICEGROUP        m_pServiceGroupWave;
	PDEVICE_OBJECT       m_pDeviceObject;      
	DEVICE_POWER_STATE   m_PowerState;        
	PCDevice             m_pVrtDevice;           // Virtual MSVAD HW object
	PKTIMER              m_pInstantiateTimer;    // Timer object
	PRKDPC               m_pInstantiateDpc;      // Deferred procedure call object
	BOOL                 m_bInstantiated;        // Flag indicating whether or not subdevices are exposed
	BOOL                 m_bPluggedIn;           // Flag indicating whether or not a jack is plugged in
	PIO_WORKITEM         m_pInstantiateWorkItem; // Work Item for instantiate timer callback

	//=====================================================================
	// Helper routines for managing the states of topologies being exposed
	STDMETHODIMP_(NTSTATUS) ExposeMixerTopology();
	STDMETHODIMP_(NTSTATUS) ExposeWaveTopology ();
	STDMETHODIMP_(NTSTATUS) UnexposeMixerTopology();
	STDMETHODIMP_(NTSTATUS) UnexposeWaveTopology ();
	STDMETHODIMP_(NTSTATUS) ConnectTopologies();
	STDMETHODIMP_(NTSTATUS) DisconnectTopologies();

public:
	//=====================================================================
	// Default CUnknown
	DECLARE_STD_UNKNOWN();
	DEFINE_STD_CONSTRUCTOR(CAdapterCommon);
	~CAdapterCommon(VOID);

	//=====================================================================
	// Default IAdapterPowerManagement
	IMP_IAdapterPowerManagement;

	//=====================================================================
	// IAdapterCommon methods                                               
	STDMETHODIMP_(NTSTATUS) Init
	(   
		IN  PDEVICE_OBJECT  DeviceObject
	);

	STDMETHODIMP_(PDEVICE_OBJECT) GetDeviceObject(VOID);
	STDMETHODIMP_(NTSTATUS)       InstantiateDevices(VOID);
	STDMETHODIMP_(NTSTATUS)       UninstantiateDevices(VOID);
	STDMETHODIMP_(NTSTATUS)       Plugin(VOID);
	STDMETHODIMP_(NTSTATUS)       Unplug(VOID);
	STDMETHODIMP_(PUNKNOWN *)     WavePortDriverDest(VOID);

	STDMETHODIMP_(VOID)           SetWaveServiceGroup(IN  PSERVICEGROUP   ServiceGroup );
	STDMETHODIMP_(BOOL)           bDevSpecificRead   (VOID);
	STDMETHODIMP_(VOID)           bDevSpecificWrite  (IN  BOOL  bDevSpecific);
	STDMETHODIMP_(INT )           iDevSpecificRead   (VOID);
	STDMETHODIMP_(VOID)           iDevSpecificWrite  (IN  INT   iDevSpecific);
	STDMETHODIMP_(UINT)           uiDevSpecificRead  (VOID);
	STDMETHODIMP_(VOID)           uiDevSpecificWrite (IN  UINT  uiDevSpecific);
	STDMETHODIMP_(BOOL)           MixerMuteRead      (IN  ULONG Index);
	STDMETHODIMP_(VOID)           MixerMuteWrite     (IN  ULONG Index , IN  BOOL Value);
	STDMETHODIMP_(ULONG)          MixerMuxRead       (VOID);
	STDMETHODIMP_(VOID)           MixerMuxWrite      (IN  ULONG Index);
	STDMETHODIMP_(VOID)           MixerReset         (VOID);
	STDMETHODIMP_(LONG)           MixerVolumeRead    (IN  ULONG Index , IN  LONG Channel);
	STDMETHODIMP_(VOID)           MixerVolumeWrite   (IN  ULONG Index , IN  LONG Channel , IN  LONG Value );
	STDMETHODIMP_(BOOL)           IsInstantiated     (VOID) { return m_bInstantiated; };
	STDMETHODIMP_(BOOL)           IsPluggedIn        (VOID) { return m_bPluggedIn; }
	STDMETHODIMP_(NTSTATUS)       SetInstantiateWorkItem(
		_In_ __drv_aliasesMem   PIO_WORKITEM    WorkItem
	);
	STDMETHODIMP_(NTSTATUS) FreeInstantiateWorkItem(VOID);

	//=====================================================================
	// friends

	friend NTSTATUS  NewAdapterCommon ( 
		OUT PADAPTERCOMMON* OutAdapterCommon, 
		IN  PRESOURCELIST   ResourceList 
	);
};

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

//=============================================================================
#pragma code_seg("PAGE")
NTSTATUS
NewAdapterCommon
( 
	OUT PUNKNOWN * Unknown,
	IN  REFCLSID ,
	IN  PUNKNOWN   UnknownOuter OPTIONAL,
	_When_((PoolType & NonPagedPoolMustSucceed) != 0,
		__drv_reportError("Must succeed pool allocations are forbidden. "
		                  "Allocation failures cause a system crash"))
	IN  POOL_TYPE  PoolType 
)
{
	PAGED_CODE();

	ASSERT(Unknown);

	STD_CREATE_BODY_
	( 
		CAdapterCommon, 
		Unknown       , 
		UnknownOuter  , 
		PoolType      ,      
		PADAPTERCOMMON 
	);
}

//=============================================================================
CAdapterCommon::~CAdapterCommon ( VOID  )
{
	PAGED_CODE();
	DPF_ENTER(("[CAdapterCommon::~CAdapterCommon]"));
	if (m_pInstantiateTimer) {
		KeCancelTimer(m_pInstantiateTimer);
		ExFreePoolWithTag(m_pInstantiateTimer, MSVAD_POOLTAG);
	}
	//
	// Since we just cancelled the  the instantiate timer, wait for all 
	// queued DPCs to complete before we free the instantiate DPC.
	//
	KeFlushQueuedDpcs();

	if (m_pInstantiateDpc) {
		ExFreePoolWithTag( m_pInstantiateDpc, MSVAD_POOLTAG );
		//
		// Should also ensure that this destructor is synchronized
		// with the instantiate timer DPC and work item.
		//
	}

	if (m_pVrtDevice) {
		delete m_pVrtDevice; m_pVrtDevice = NULL;
	}

	CPersistent::DestroyWorkItems();

	if (m_pMiniportWave) {
		m_pMiniportWave->Release();
		m_pMiniportWave = NULL;
	}

	if (m_pPortWave) {
		m_pPortWave->Release();
		m_pPortWave = NULL;
	}

	if (m_pMiniportTopology) {
		m_pMiniportTopology->Release();
		m_pMiniportTopology = NULL;
	}

	if (m_pPortTopology) {
		m_pPortTopology->Release();
		m_pPortTopology = NULL;
	}

	if (m_pServiceGroupWave) {
		m_pServiceGroupWave->Release();
	}
}
#pragma code_seg()
//=============================================================================

STDMETHODIMP_(PDEVICE_OBJECT)   
CAdapterCommon::GetDeviceObject (VOID ) { return m_pDeviceObject; }

//=============================================================================
#pragma code_seg("PAGE")
NTSTATUS
CAdapterCommon::Init ( 
    IN  PDEVICE_OBJECT DeviceObject 
)
{
	PAGED_CODE();

	ASSERT(DeviceObject);

	NTSTATUS ntStatus = STATUS_SUCCESS;
	DPF_ENTER(("[CAdapterCommon::Init]"));
	m_pDeviceObject        = DeviceObject;
	m_PowerState           = PowerDeviceD0;
	m_pPortWave            = NULL;
	m_pMiniportWave        = NULL;
	m_pPortTopology        = NULL;
	m_pMiniportTopology    = NULL;
	m_pInstantiateTimer    = NULL;
	m_pInstantiateDpc      = NULL;
	m_bInstantiated        = FALSE;
	m_bPluggedIn           = FALSE;
	m_pInstantiateWorkItem = NULL;
	//
	// Initialize persistent.
	// 
	m_pVrtDevice = new (NonPagedPool, MSVAD_POOLTAG) CDevice;
	if (!m_pVrtDevice) {
		DPF(D_TERSE, ("Insufficient memory for virtual device"));
		ntStatus = STATUS_INSUFFICIENT_RESOURCES;
	}
	else {
		m_pVrtDevice->MixerReset();
	}

	CPersistent::SetDeviceObject(DeviceObject);   //device object is needed by CPersistent
	//
	// Allocate DPC for instantiation timer.
	//
	if (NT_SUCCESS(ntStatus)) {
		m_pInstantiateDpc = (PRKDPC)
			ExAllocatePoolWithTag (
				NonPagedPool,
				sizeof(KDPC),
				MSVAD_POOLTAG
			);

		if (!m_pInstantiateDpc) {
			DPF(D_TERSE, ("[Could not allocate memory for DPC]"));
			ntStatus = STATUS_INSUFFICIENT_RESOURCES;
		}
	}
	//
	// Allocate timer for instantiation.
	//
	if (NT_SUCCESS(ntStatus)) {
		m_pInstantiateTimer = (PKTIMER)
			ExAllocatePoolWithTag (
				NonPagedPool,
				sizeof(KTIMER),
				MSVAD_POOLTAG
			);

		if (!m_pInstantiateTimer) {
			DPF(D_TERSE, ("[Could not allocate memory for Timer]"));
			ntStatus = STATUS_INSUFFICIENT_RESOURCES;
		}
	}
	//
	// Initialize instantiation timer and DPC.
	//
	if (NT_SUCCESS(ntStatus)) {
		KeInitializeDpc(m_pInstantiateDpc, InstantiateTimerNotify, this);
		KeInitializeTimerEx(m_pInstantiateTimer, NotificationTimer);

#ifdef _ENABLE_INSTANTIATION_INTERVAL_
		//
		// Set the timer to expire every INSTANTIATE_INTERVAL_MS milliseconds.
		//
		LARGE_INTEGER liInstantiateInterval = {0};
		liInstantiateInterval.QuadPart = -1 * INSTANTIATE_INTERVAL_MS * HNS_PER_MS;
		KeSetTimerEx (
			m_pInstantiateTimer    ,
			liInstantiateInterval  ,
			INSTANTIATE_INTERVAL_MS,
			m_pInstantiateDpc
		);
#endif
	}

	return ntStatus;
}
//=============================================================================
STDMETHODIMP_(VOID)
CAdapterCommon::MixerReset (VOID) {
	PAGED_CODE();
    
	if (m_pVrtDevice) {
		m_pVrtDevice->MixerReset();
	}
}

//=============================================================================
STDMETHODIMP
CAdapterCommon::NonDelegatingQueryInterface ( 
	_In_         REFIID  Interface,
	_COM_Outptr_ PVOID*  Object 
)
{
	PAGED_CODE();

	ASSERT(Object);

	if (IsEqualGUIDAligned(Interface, IID_IUnknown)) {
		*Object = PVOID(PUNKNOWN(PADAPTERCOMMON(this)));
	}
	else if (IsEqualGUIDAligned(Interface, IID_IAdapterCommon)) {
		*Object = PVOID(PADAPTERCOMMON(this));
	}
	else if (IsEqualGUIDAligned(Interface, IID_IAdapterPowerManagement)) {
		*Object = PVOID(PADAPTERPOWERMANAGEMENT(this));
	}
	else {
		*Object = NULL;
	}

	if (*Object) {
		PUNKNOWN(*Object)->AddRef();
		return STATUS_SUCCESS;
	}

	return STATUS_INVALID_PARAMETER;
}

//=============================================================================
STDMETHODIMP_(VOID)
CAdapterCommon::SetWaveServiceGroup ( 
	IN PSERVICEGROUP ServiceGroup 
)
{
	PAGED_CODE();
    
	DPF_ENTER(("[CAdapterCommon::SetWaveServiceGroup]"));
    
	if (m_pServiceGroupWave) {
		m_pServiceGroupWave->Release();
	}

	m_pServiceGroupWave = ServiceGroup;

	if (m_pServiceGroupWave) {
		m_pServiceGroupWave->AddRef();
	}
}

//=============================================================================
STDMETHODIMP_(NTSTATUS)
CAdapterCommon::InstantiateDevices (VOID)
{
	PAGED_CODE();

	NTSTATUS ntStatus = STATUS_SUCCESS;

	if (m_bInstantiated) {
		return STATUS_SUCCESS;
	}
	//
	// If the mixer topology port is not exposed, create and expose it.
	//
	ntStatus = ExposeMixerTopology();
	//
	// Create and expose the wave topology.
	//
	if (NT_SUCCESS(ntStatus)) {
		ntStatus = ExposeWaveTopology();
	}
	//
	// Register the physical connection between wave and mixer topologies.
	//
	if (NT_SUCCESS(ntStatus)) {
		ntStatus = ConnectTopologies();
	}

	if (NT_SUCCESS(ntStatus)) {
		m_bInstantiated = TRUE;
		m_bPluggedIn = TRUE;
	}

	return ntStatus;
}

//=============================================================================
STDMETHODIMP_(NTSTATUS)
CAdapterCommon::UninstantiateDevices (VOID)
{
	PAGED_CODE();

	NTSTATUS ntStatus = STATUS_SUCCESS;
	//
	// Check if we're already uninstantiated
	//
	if (!m_bInstantiated) {
		return ntStatus;
	}
	//
	// Unregister the physical connection between wave and mixer topologies
	// and unregister/unexpose the wave topology. This is the same as being 
	// unplugged.
	//
	if (NT_SUCCESS(ntStatus)) {
		ntStatus = Unplug();
	}
	//
	// Unregister the topo port
	//
	if (NT_SUCCESS(ntStatus)) {
		ntStatus = UnexposeMixerTopology();
	}

	if (NT_SUCCESS(ntStatus)) {
		m_bInstantiated = FALSE;
		m_bPluggedIn = FALSE;
	}

	return ntStatus;
}

//=============================================================================
// Called in response to jacks being plugged in.
STDMETHODIMP_(NTSTATUS)
CAdapterCommon::Plugin (VOID)
{
	PAGED_CODE();

	NTSTATUS ntStatus = STATUS_SUCCESS;

	if (!m_bInstantiated) {
		return STATUS_INVALID_DEVICE_STATE;
	}

	if (m_bPluggedIn) {
		return ntStatus;
	}
	//
	// Create and expose the wave topology.
	//
	if (NT_SUCCESS(ntStatus)) {
		ntStatus = ExposeWaveTopology();
	}
	//
	// Register the physical connection between wave and mixer topologies.
	//
	if (NT_SUCCESS(ntStatus)) {
		ntStatus = ConnectTopologies();
	}

	if (NT_SUCCESS(ntStatus)) {
		m_bPluggedIn = TRUE;
	}

	return ntStatus;
}

//=============================================================================
// Called in response to jacks being unplugged.
STDMETHODIMP_(NTSTATUS)
CAdapterCommon::Unplug (VOID)
{
	PAGED_CODE();

	NTSTATUS ntStatus = STATUS_SUCCESS;

	if (!m_bInstantiated) {
		return STATUS_INVALID_DEVICE_STATE;
	}

	if (!m_bPluggedIn) {
		return ntStatus;
	}
	//
	// Unregister the physical connection between wave and mixer topologies.
	//
	if (NT_SUCCESS(ntStatus)) {
		ntStatus = DisconnectTopologies();
	}
	//
	// Unregister and destroy the wave port
	//
	if (NT_SUCCESS(ntStatus)) {
		ntStatus = UnexposeWaveTopology();
	}

	if (NT_SUCCESS(ntStatus)) {
		m_bPluggedIn = FALSE;
	}

	return ntStatus;
}
// Creates and registers the mixer topology.
STDMETHODIMP_(NTSTATUS) 
CAdapterCommon::ExposeMixerTopology (VOID)
{
	NTSTATUS ntStatus = STATUS_SUCCESS;

	PAGED_CODE();

	if (m_pPortTopology) {
		return ntStatus;
	}

	ntStatus = InstallSubdevice( 
		m_pDeviceObject,
		NULL,
		L"Topology",
		CLSID_PortTopology,
		CLSID_PortTopology, 
		CreateMiniportTopologyMSVAD,
		PUNKNOWN(PADAPTERCOMMON(this)),
		NULL,
		&m_pPortTopology,
		&m_pMiniportTopology
	);

	return ntStatus;
}
//  Creates and registers wave topology.
STDMETHODIMP_(NTSTATUS) 
CAdapterCommon::ExposeWaveTopology (VOID) {
	NTSTATUS ntStatus = STATUS_SUCCESS;

	PAGED_CODE();

	if (m_pPortWave) {
		return ntStatus;
	}

	ntStatus = InstallSubdevice( 
		m_pDeviceObject,
		NULL   ,
		L"Wave",
		CLSID_PortWaveCyclic,
		CLSID_PortWaveCyclic,   
		CreateMiniportWaveCyclicMSVAD,
		PUNKNOWN(PADAPTERCOMMON(this)),
		NULL,
		&m_pPortWave,
		&m_pMiniportWave
	);

	return ntStatus;
}
// Unregisters and releases the mixer topology.
STDMETHODIMP_(NTSTATUS) 
CAdapterCommon::UnexposeMixerTopology (VOID)
{
    NTSTATUS               ntStatus = STATUS_SUCCESS;
    PUNREGISTERSUBDEVICE   pUnregisterSubdevice = NULL;

	PAGED_CODE();

	if (NULL == m_pPortTopology) {
		return ntStatus;
	}
	//
	// Get the IUnregisterSubdevice interface.
	//
	ntStatus = m_pPortTopology->QueryInterface(
			IID_IUnregisterSubdevice, (PVOID *)&pUnregisterSubdevice
	);
	//
	// Unregister the topo port.
	//
	if (NT_SUCCESS(ntStatus)) {
		ntStatus = pUnregisterSubdevice->UnregisterSubdevice(
			m_pDeviceObject,
			m_pPortTopology
		);
		//
		// Release the IUnregisterSubdevice interface.
		//
		pUnregisterSubdevice->Release();
		//
		// At this point, we're done with the mixer topology and 
		// the miniport.
		//
		if (NT_SUCCESS(ntStatus)) {

			m_pPortTopology->Release();
			m_pPortTopology = NULL;

			m_pMiniportTopology->Release();
			m_pMiniportTopology = NULL;
		}
    }

    return ntStatus;
}
// Unregisters and releases the wave topology.
STDMETHODIMP_(NTSTATUS)
CAdapterCommon::UnexposeWaveTopology (VOID)
{
	NTSTATUS               ntStatus = STATUS_SUCCESS;
	PUNREGISTERSUBDEVICE   pUnregisterSubdevice = NULL;

	PAGED_CODE();

	if (NULL == m_pPortWave) {
		return ntStatus;
	}
	//
	// Get the IUnregisterSubdevice interface.
	//
	ntStatus = m_pPortWave->QueryInterface(
			IID_IUnregisterSubdevice, (PVOID *)&pUnregisterSubdevice
	);
	//
	// Unregister the wave port.
	//
	if (NT_SUCCESS(ntStatus)) {
		ntStatus = pUnregisterSubdevice->UnregisterSubdevice(
			m_pDeviceObject, m_pPortWave
		);
		//
		// Release the IUnregisterSubdevice interface.
		//
		pUnregisterSubdevice->Release();
		//
		// At this point, we're done with the mixer topology and 
		// the miniport.
		//
		if (NT_SUCCESS(ntStatus)) {
			m_pPortWave->Release();
			m_pPortWave = NULL;

			m_pMiniportWave->Release();
			m_pMiniportWave = NULL;
		}
	}
	return ntStatus;
}
// Connects the bridge pins between the wave and mixer topologies.
STDMETHODIMP_(NTSTATUS)
CAdapterCommon::ConnectTopologies (VOID)
{
	NTSTATUS ntStatus = STATUS_SUCCESS;

	PAGED_CODE();
	//
	// Connect the capture path.
	//
	if ((TopologyPhysicalConnections.ulTopologyOut != (ULONG)-1) && (TopologyPhysicalConnections.ulWaveIn != (ULONG)-1)) {

		ntStatus = PcRegisterPhysicalConnection( 
			m_pDeviceObject,
			m_pPortTopology,
			TopologyPhysicalConnections.ulTopologyOut,
			m_pPortWave,
			TopologyPhysicalConnections.ulWaveIn
		);
	}
	//
	// Connect the render path.
	//
	if (NT_SUCCESS(ntStatus)) {
		if ((TopologyPhysicalConnections.ulWaveOut != (ULONG)-1) && (TopologyPhysicalConnections.ulTopologyIn != (ULONG)-1)) {

			ntStatus = PcRegisterPhysicalConnection ( 
					m_pDeviceObject, m_pPortWave, TopologyPhysicalConnections.ulWaveOut, m_pPortTopology, TopologyPhysicalConnections.ulTopologyIn
				);
		}
	}

	return ntStatus;
}
//  Disconnects the bridge pins between the wave and mixer topologies.
STDMETHODIMP_(NTSTATUS)
CAdapterCommon::DisconnectTopologies (VOID)
{
	NTSTATUS                        ntStatus = STATUS_SUCCESS;
	PUNREGISTERPHYSICALCONNECTION   pUnregisterPhysicalConnection = NULL;

	PAGED_CODE();
	//
	// Get the IUnregisterPhysicalConnection interface
	//
	ntStatus = m_pPortTopology->QueryInterface(
				IID_IUnregisterPhysicalConnection, (PVOID *)&pUnregisterPhysicalConnection
	);
	//
	// Remove the render physical connection
	//
	if (NT_SUCCESS(ntStatus) &&
	   (TopologyPhysicalConnections.ulWaveOut != (ULONG)-1) &&
	   (TopologyPhysicalConnections.ulTopologyIn != (ULONG)-1)) {

		ntStatus = pUnregisterPhysicalConnection->UnregisterPhysicalConnection(
		m_pDeviceObject,
		m_pPortWave,
		TopologyPhysicalConnections.ulWaveOut,
		m_pPortTopology,
		TopologyPhysicalConnections.ulTopologyIn);
		//
		// Remove the capture physical connection
		//
		if (NT_SUCCESS(ntStatus) &&
		   (TopologyPhysicalConnections.ulTopologyOut != (ULONG)-1) &&
		   (TopologyPhysicalConnections.ulWaveIn != (ULONG)-1)) {

			ntStatus = pUnregisterPhysicalConnection->UnregisterPhysicalConnection(
				m_pDeviceObject, m_pPortTopology, TopologyPhysicalConnections.ulTopologyOut, m_pPortWave, TopologyPhysicalConnections.ulWaveIn
			);
		}
		//
		// Release the IUnregisterPhysicalConnection interface.
		//
		pUnregisterPhysicalConnection->Release();
	}
	return ntStatus;
}

#pragma code_seg()
//=============================================================================
// Sets the work item that will be called to instantiate or  uninstantiate topologies.
STDMETHODIMP_(NTSTATUS)		
CAdapterCommon::SetInstantiateWorkItem (
	_In_ __drv_aliasesMem PIO_WORKITEM WorkItem
)
{
	//
	// Make sure there isn't already a work item allocated.
	//
	if ( m_pInstantiateWorkItem != NULL ) {
		return STATUS_INVALID_DEVICE_STATE;
	}
	//
	// Stash the work item to be free'd after the work routine is called.
	//
	m_pInstantiateWorkItem = WorkItem;

	return STATUS_SUCCESS;
}

#pragma code_seg("PAGE")
//=============================================================================
// Frees a work item that was called to instantiate or uninstantiate topologies.
STDMETHODIMP_(NTSTATUS)
CAdapterCommon::FreeInstantiateWorkItem(VOID)
{
	PAGED_CODE();
	//
	// Make sure we actually have a work item set.
	//
	if ( m_pInstantiateWorkItem == NULL ) {
		return STATUS_INVALID_DEVICE_STATE;
	}
	//
	// Go ahead and free the work item.
	//
	IoFreeWorkItem( m_pInstantiateWorkItem );
	m_pInstantiateWorkItem = NULL;

	return STATUS_SUCCESS;
}

//=============================================================================
// Returns the wave port.
STDMETHODIMP_(PUNKNOWN *)
CAdapterCommon::WavePortDriverDest (VOID) 
{
	PAGED_CODE();

	return &m_pPortWave;
}

#pragma code_seg()

//=============================================================================
// Fetch Device Specific information.
STDMETHODIMP_(BOOL)
CAdapterCommon::bDevSpecificRead(VOID) {

	if (m_pVrtDevice) {
		return m_pVrtDevice->bGetDevSpecific();
	}

	return FALSE;
}

//=============================================================================
// Store the new value in the Device Specific location.
STDMETHODIMP_(VOID)
CAdapterCommon::bDevSpecificWrite (
	IN  BOOL bDevSpecific
)
{
	if (m_pVrtDevice) {
		m_pVrtDevice->bSetDevSpecific(bDevSpecific);
	}
}

//=============================================================================
// Fetch Device Specific information.
STDMETHODIMP_(INT)
CAdapterCommon::iDevSpecificRead(VOID)
{
	if (m_pVrtDevice) {
		return m_pVrtDevice->iGetDevSpecific();
	}

	return 0;
}

//=============================================================================
// Store the new value in the Device Specific location.
STDMETHODIMP_(VOID)
CAdapterCommon::iDevSpecificWrite (
	IN  INT iDevSpecific
)
{
	if (m_pVrtDevice) {
		m_pVrtDevice->iSetDevSpecific(iDevSpecific);
	}
}

//=============================================================================
// Fetch Device Specific information.
STDMETHODIMP_(UINT)
CAdapterCommon::uiDevSpecificRead(VOID)
{
	if (m_pVrtDevice) {
		return m_pVrtDevice->uiGetDevSpecific();
	}

	return 0;
}

//=============================================================================
// Store the new value in the Device Specific location.
STDMETHODIMP_(VOID)
CAdapterCommon::uiDevSpecificWrite (
	IN  UINT uiDevSpecific
)
{
	if (m_pVrtDevice) {
		m_pVrtDevice->uiSetDevSpecific(uiDevSpecific);
	}
}

//=============================================================================
// Store the new value in mixer register array.
STDMETHODIMP_(BOOL)
CAdapterCommon::MixerMuteRead (
	IN  ULONG Index
)
{
	if (m_pVrtDevice) {
		return m_pVrtDevice->GetMixerMute(Index);
	}

	return 0;
}

//=============================================================================
// Store the new value in mixer register array.
STDMETHODIMP_(VOID)
CAdapterCommon::MixerMuteWrite (
	IN  ULONG  Index,
	IN  BOOL   Value
)
{
    if (m_pVrtDevice) {
		m_pVrtDevice->SetMixerMute(Index, Value);
    }
}

//=============================================================================
// Return the mux selection
STDMETHODIMP_(ULONG)
CAdapterCommon::MixerMuxRead(VOID)
{
	if (m_pVrtDevice) {
		return m_pVrtDevice->GetMixerMux();
	}

	return 0;
}

//=============================================================================
// Store the new mux selection
STDMETHODIMP_(VOID)
CAdapterCommon::MixerMuxWrite (
	IN  ULONG   Index
)
{
	if (m_pVrtDevice) {
		m_pVrtDevice->SetMixerMux(Index);
	}
}

//=============================================================================
// Return the value in mixer register array.
STDMETHODIMP_(LONG)
CAdapterCommon::MixerVolumeRead ( 
	IN  ULONG   Index,
	IN  LONG    Channel
)
{
	if (m_pVrtDevice) {
		return m_pVrtDevice->GetMixerVolume(Index, Channel);
	}

	return 0;
}

//=============================================================================
// Store the new value in mixer register array.
STDMETHODIMP_(VOID)
CAdapterCommon::MixerVolumeWrite ( 
	IN  ULONG   Index,
	IN  LONG    Channel,
	IN  LONG    Value
)
{
	if (m_pVrtDevice) {
		m_pVrtDevice->SetMixerVolume(Index, Channel, Value);
	}
}

//=============================================================================
// NewState - The requested, new power state for the device.
STDMETHODIMP_(VOID)
CAdapterCommon::PowerChangeState ( 
	_In_  POWER_STATE  NewState 
)
{
	DPF_ENTER(("[CAdapterCommon::PowerChangeState]"));
	//
	// is this actually a state change??
	//
	if (NewState.DeviceState != m_PowerState) {
		//
		// switch on new state
		//
		switch (NewState.DeviceState)
		{
		case PowerDeviceD0:
		case PowerDeviceD1:
		case PowerDeviceD2:
		case PowerDeviceD3:
			m_PowerState = NewState.DeviceState;

			DPF ( 
				D_VERBOSE, 
				("Entering D%d", ULONG(m_PowerState) - ULONG(PowerDeviceD0)) 
			);

			break;
    
		default:
			DPF(D_VERBOSE, ("Unknown Device Power State"));
			break;
		}
	}
}

//=============================================================================
/*++
Routine Description:
    Called at startup to get the caps for the device.  This structure provides 
    the system with the mappings between system power state and device power 
    state.  This typically will not need modification by the driver.         

Arguments:
    PowerDeviceCaps - The device's capabilities. 

Return Value:
    NT status code.
--*/
_Use_decl_annotations_
STDMETHODIMP_(NTSTATUS)
CAdapterCommon::QueryDeviceCapabilities ( 
    PDEVICE_CAPABILITIES    PowerDeviceCaps 
)
{
	UNREFERENCED_PARAMETER(PowerDeviceCaps);

	DPF_ENTER(("[CAdapterCommon::QueryDeviceCapabilities]"));

	return (STATUS_SUCCESS);
}

//=============================================================================
// Query to see if the device can change to this power state
// NewStateQuery - The requested, new power state for the device
STDMETHODIMP_(NTSTATUS)
CAdapterCommon::QueryPowerChangeState ( 
	_In_  POWER_STATE   NewStateQuery 
)
{
	UNREFERENCED_PARAMETER(NewStateQuery);

	DPF_ENTER(("[CAdapterCommon::QueryPowerChangeState]"));

	return STATUS_SUCCESS;
}