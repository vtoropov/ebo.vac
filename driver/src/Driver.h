#ifndef _DRIVER_H_F3A4BFF5_0846_4C7A_8DD5_99E4BD516878_INCLUDED
#define _DRIVER_H_F3A4BFF5_0846_4C7A_8DD5_99E4BD516878_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Jun-2020 at 4:33:15a, UTC+7, Novosibirsk, Friday;
	This is Ebo VAC driver entry point interface declaration file.
*/

namespace ebo { namespace drv { namespace km {
	// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nc-wdm-driver_initialize
	// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/wdfdriver/nc-wdfdriver-evt_wdf_driver_device_add
	// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/wdfdriver/nc-wdfdriver-evt_wdf_driver_unload
	class CDriver {
	public:
		 CDriver (void);
		~CDriver (void);
	public:
		static NTSTATUS AddDevice   (_In_ PDRIVER_OBJECT, _In_ PDEVICE_OBJECT);
		static NTSTATUS DriverEntry (_In_ PDRIVER_OBJECT, _In_ PUNICODE_STRING pszRegistryPath);
		static VOID     DriverUnload(_In_ PDRIVER_OBJECT);
	};

}}}

__extern_c
NTSTATUS DriverEntry(_In_ struct _DRIVER_OBJECT*, _In_ PUNICODE_STRING pszRegistryPath);

#endif/*_DRIVER_H_F3A4BFF5_0846_4C7A_8DD5_99E4BD516878_INCLUDED*/