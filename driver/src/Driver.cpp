/*++
	Copyright (c) 1997-2000 Microsoft Corporation All Rights Reserved
    Setup and miniport installation. No resources are used by msvad.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver project on 12-Jun-2020 at 4:35:35a, UTC+7, Novosibirsk, Friday;
--*/
#include "StdAfx.h"
#include "Driver.h"
#include "Device.h"

using namespace ebo::drv::km;

#define __max_min_ports (2)
//-----------------------------------------------------------------------------
CDriver:: CDriver(void) {}
CDriver::~CDriver(void) {}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/*
Routine Description:
  The Plug & Play subsystem is handing us a brand new PDO, for which we
  (by means of INF registration) have been asked to provide a driver.

  We need to determine if we need to be in the driver stack for the device.
  Create a function device object to attach to the stack
  Initialize that device object
  Return status success.

  All audio adapter drivers can use this code without change.
  Set MAX_MINIPORTS depending on the number of miniports that the driver
  uses.

Arguments:
  DriverObject         - pointer to a driver object
  PhysicalDeviceObject - pointer to a device object created by the
                          underlying bus driver.
Return Value:
  NT status code.
*/
#pragma code_seg("PAGE")
NTSTATUS   CDriver::AddDevice   (_In_ PDRIVER_OBJECT pDriverObject, _In_ PDEVICE_OBJECT pDevice) {

	PAGED_CODE();
	// tells the class driver to add the device;
	NTSTATUS nt_status = ::PcAddAdapterDevice(
			pDriverObject, pDevice, PCPFNSTARTDEVICE(CDevice::Start), __max_min_ports, 0
		);
	return nt_status;
}
#pragma code_seg()

NTSTATUS   CDriver::DriverEntry (_In_ PDRIVER_OBJECT, _In_ PUNICODE_STRING pszRegistryPath) {
	pszRegistryPath;
	NTSTATUS nt_status = STATUS_SUCCESS;
	return nt_status;
}

VOID       CDriver::DriverUnload(_In_ PDRIVER_OBJECT) {}
//-----------------------------------------------------------------------------
// https://docs.microsoft.com/en-us/cpp/preprocessor/code-seg?view=vs-2019
// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/portcls/nf-portcls-pcinitializeadapterdriver
//-----------------------------------------------------------------------------

#pragma code_seg("INIT")
NTSTATUS DriverEntry(_In_ struct _DRIVER_OBJECT* pDriverObject, _In_ PUNICODE_STRING pszRegistryPath) {

	PAGED_CODE();

	NTSTATUS nt_status = ::PcInitializeAdapterDriver(
			pDriverObject, pszRegistryPath, CDriver::AddDevice
		);
	return nt_status;
}
#pragma code_seg()