#ifndef _FSGENERICDEFS_H_3960A0A4_4A33_45A0_B7F1_63B24448270C_INCLUDED
#define _FSGENERICDEFS_H_3960A0A4_4A33_45A0_B7F1_63B24448270C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Jun-2020 at 6:35:06p, UTC+7, Novosibirsk, Sunday;
	This is Ebo driver shared library generic file definition interface declaration file.
*/
#include "w_string.h"
#ifndef PURE
#define PURE = 0
#endif
namespace ebo { namespace drv { namespace km { namespace file_sys {

	using ebo::drv::km::annex::CStringW;
	// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/ntifs/nf-ntifs-ntcreatefile
	class COptions {
	public:
		enum _atts : DWORD {
			e_is_dir     = 0,    // is folder or file;
			e_aleratable = 1,    // to generate alerts in cases when synchronization is not coompeted;
			e_synced     = 2,    // synchronous input/output attribute;
			e_exclusive  = 3,    // no sharing an access;
			e_existing   = 4,    // if true, a file must exist, otherwise, new file is created;
			e_replace    = 5,    // a file is superseded if necessary;
			e__att__count
		};
	protected:
		bool   m_att_mask[_atts::e__att__count];

	public:
		 COptions (void);
		~COptions (void);

	public:
		virtual	DWORD   Attributes   (void) const; // file/directory attributes;
		virtual DWORD   CreateMode   (void) const; // options of creating  file system object;
		virtual DWORD   DesiredAccess(void) const; // desired access to file system object;
		virtual DWORD   Disposition  (void) const; // create disposition;
		virtual DWORD   SharedAccess (void) const; // shared access to file system object;

	public:
		bool  Get(const _atts) const;
		void  Set(const _atts, const bool _value);

	public:
		COptions& operator += (const _atts);  // sets attribute specified to true;
		COptions& operator -= (const _atts);  // sets attribute specified to false;
	};

	class COptions_Read : public COptions {
	                     typedef COptions TBase;
	public:
		 COptions_Read (void);
		~COptions_Read (void);

	public:
		DWORD   Attributes   (void) const override;
		DWORD   DesiredAccess(void) const override;
		DWORD   Disposition  (void) const override;
		DWORD   SharedAccess (void) const override;
	};

	class COptions_Write : public COptions {
	                      typedef COptions TBase;
	private:
		bool  m_b_overwrite;

	public:
		 COptions_Write (void);
		~COptions_Write (void);

	public:
		DWORD   Attributes   (void) const override;
		DWORD   DesiredAccess(void) const override;
		DWORD   Disposition  (void) const override;
		DWORD   SharedAccess (void) const override;

	public:
		bool    Overwrite(void) const;
		bool&   Overwrite(void)      ;
	};

	class CPath {
	protected:
		CStringW   m_dir ;  // folder/directory path; by default, it is expected user application temporary folder;
		CStringW   m_name;  // file name; can be generated dynamically, if not set explicitely;

	public:
		 CPath (void);
		~CPath (void);

	public:
		const
		CStringW&  Dir (void) const; // absolute folder/directory path;
		CStringW&  Dir (void)      ;
		const bool Is  (void) const;
		const
		CStringW&  FileName(void) const;
		CStringW&  FileName(void)      ;
	};

}}}}
#endif/*_FSGENERICDEFS_H_3960A0A4_4A33_45A0_B7F1_63B24448270C_INCLUDED*/