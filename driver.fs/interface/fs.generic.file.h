#ifndef _STORAGEFILE_H_1DEADAC7_DC82_48CA_8587_11523DA4E59F_INCLUDED
#define _STORAGEFILE_H_1DEADAC7_DC82_48CA_8587_11523DA4E59F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Jun-2020 at 7:45:00p, UTC+7, Novosibirsk, Saturday;
	This is Ebo driver shared library generic file interface declaration file.
*/
#include "fs.generic.defs.h"
namespace ebo { namespace drv { namespace km { namespace file_sys {

	using ebo::drv::km::annex::CStringW;
	// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-zwcreatefile
	// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-zwclose
	// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-zwwritefile
	class CGenericFile {
	protected:
		HANDLE     m_handle;
		CPath      m_path  ;

	public:
		 CGenericFile (void);
		~CGenericFile (void);

	public:
		NTSTATUS   Close(void) ;
		const
		bool       Is   (void) const;      // returns true if file is open or is created;
		NTSTATUS   Open (const COptions&);
		const
		CPath&     Path (void) const;
		CPath&     Path (void)      ;
		NTSTATUS   Write(PVOID pBuffer, const ULONG uLength, PLARGE_INTEGER pShift); // writes data to file and assigned pShift to new position;

	private: // not copyable;
		CGenericFile (const CGenericFile&);
		CGenericFile& operator = (const CGenericFile&);
	};

}}}}

#endif/*_STORAGEFILE_H_1DEADAC7_DC82_48CA_8587_11523DA4E59F_INCLUDED*/