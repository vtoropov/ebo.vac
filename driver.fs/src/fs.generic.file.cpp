/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Jun-2020 at 7:59:20p, UTC+7, Novosibirsk, Saturday;
	This is Ebo driver shared library generic file interface implementation file.
*/
#include "StdAfx.h"
#include "fs.generic.file.h"

using namespace ebo::drv::km::file_sys;

/////////////////////////////////////////////////////////////////////////////

CGenericFile:: CGenericFile(void) : m_handle(NULL) {}
CGenericFile::~CGenericFile(void) {}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS     CGenericFile::Close(void) { NTSTATUS nt_status = S_OK;
	if (this->Is() == false)
		return nt_status;

	nt_status = ::ZwClose(m_handle); m_handle = NULL;

	return nt_status;
}

const bool   CGenericFile::Is   (void) const { return (NULL != m_handle); }
NTSTATUS     CGenericFile::Open (const COptions& _ops) {
	PAGED_CODE();

	NTSTATUS nt_status = STATUS_SUCCESS;

	if (this->Is() == true) {
		return nt_status ;
	}
	//
	// a log file path must be absolute, at least it must contain a destination folder,
	// otherwise, we'll get STATUS_OBJECT_PATH_SYNTAX_BAD;
	//
	UNICODE_STRING path_= RTL_CONSTANT_STRING(_T("\\??\\Global\\c:\\fg_driver_log.log"));

	OBJECT_ATTRIBUTES atts_ = {0};
	InitializeObjectAttributes(
		&atts_, &path_, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, NULL
	);

	IO_STATUS_BLOCK io_status = {0};

	nt_status = ::ZwCreateFile (
		&m_handle, _ops.DesiredAccess(), &atts_ , &io_status, NULL, _ops.Attributes(), _ops.SharedAccess(), _ops.Disposition(), _ops.CreateMode(), NULL, 0
	);

	return nt_status;
}
const
CPath&       CGenericFile::Path(void) const { return m_path; }
CPath&       CGenericFile::Path(void)       { return m_path; }

NTSTATUS     CGenericFile::Write(PVOID pBuffer, const ULONG uLength, PLARGE_INTEGER pShift) {
	
	NTSTATUS nt_status = S_OK;

	if (this->Is() == false)
		return (nt_status = STATUS_INVALID_HANDLE);

	if (NULL == pBuffer) return (nt_status = STATUS_INVALID_PARAMETER);
	if (NULL == pShift ) return (nt_status = STATUS_INVALID_PARAMETER);
	if (NULL == uLength) return (nt_status = STATUS_INVALID_PARAMETER);
	
	IO_STATUS_BLOCK io_status = {0};

	nt_status = ::ZwWriteFile(
		m_handle, NULL, NULL, NULL, &io_status, pBuffer, uLength, pShift, NULL
	);
	if (SUCCEEDED(nt_status))
		pShift->QuadPart += uLength;

	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////