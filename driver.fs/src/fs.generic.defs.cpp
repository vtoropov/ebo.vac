/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Jun-2020 at 6:40:46p, UTC+7, Novosibirsk, Sunday;
	This is Ebo driver shared library generic file definition interface implementation file.
*/
#include "StdAfx.h"
#include "fs.generic.defs.h"

using namespace ebo::drv::km::file_sys;

/////////////////////////////////////////////////////////////////////////////

COptions:: COptions(void) { ::memset(m_att_mask, 0, sizeof(DWORD) * _atts::e__att__count); }
COptions::~COptions(void) {}

/////////////////////////////////////////////////////////////////////////////

DWORD   COptions::Attributes   (void) const { return (FILE_ATTRIBUTE_NORMAL); }
DWORD   COptions::CreateMode   (void) const {
	DWORD dw_opts = 0;
	if (this->Get(_atts::e_aleratable) == true ) dw_opts |= FILE_SYNCHRONOUS_IO_ALERT;
	if (this->Get(_atts::e_aleratable) == false) dw_opts |= FILE_SYNCHRONOUS_IO_NONALERT;
	if (this->Get(_atts::e_is_dir)     == true ) dw_opts |= FILE_DIRECTORY_FILE;
	if (this->Get(_atts::e_is_dir)     == false) dw_opts |= FILE_NON_DIRECTORY_FILE;

	return dw_opts;
}
DWORD   COptions::DesiredAccess(void) const {
	DWORD  dw_opts = 0;
	if (this->Get(_atts::e_synced) == true) dw_opts |= SYNCHRONIZE;
	return dw_opts;
}
DWORD   COptions::Disposition  (void) const { DWORD  dw_opts = 0; return dw_opts; }
DWORD   COptions::SharedAccess (void) const { DWORD  dw_opts = 0; return dw_opts; }

/////////////////////////////////////////////////////////////////////////////

bool    COptions::Get(const _atts _e_att) const { if (_e_att < _atts::e__att__count) return m_att_mask[_e_att]; else return false; }
void    COptions::Set(const _atts _e_att, const bool _value) { if (_e_att < _atts::e__att__count) m_att_mask[_e_att] = _value; }

/////////////////////////////////////////////////////////////////////////////

COptions&   COptions::operator += (const _atts _e_att) { this->Set(_e_att, true ); return *this; }
COptions&   COptions::operator -= (const _atts _e_att) { this->Set(_e_att, false); return *this; }

/////////////////////////////////////////////////////////////////////////////

COptions_Read:: COptions_Read(void) {}
COptions_Read::~COptions_Read(void) {}

/////////////////////////////////////////////////////////////////////////////

DWORD   COptions_Read::Attributes   (void) const { return 0; }
DWORD   COptions_Read::DesiredAccess(void) const {
	DWORD  dw_opts = TBase::DesiredAccess(); dw_opts |= FILE_READ_DATA;
	return dw_opts;
}
DWORD   COptions_Read::Disposition  (void) const {
	DWORD  dw_opts = TBase::Disposition();
	if (TBase::Get(_atts::e_existing) == true ) dw_opts |= FILE_OPEN;
	if (TBase::Get(_atts::e_existing) == false) dw_opts |= FILE_OPEN_IF;

	return dw_opts;
}
DWORD   COptions_Read::SharedAccess (void) const {
	DWORD  dw_opts = TBase::SharedAccess();
	if (TBase::Get(_atts::e_exclusive) == false) dw_opts |= FILE_READ_DATA;
	if (TBase::Get(_atts::e_exclusive) == false) dw_opts |= FILE_WRITE_DATA;
	return dw_opts;
}

/////////////////////////////////////////////////////////////////////////////

COptions_Write:: COptions_Write(void) : m_b_overwrite(false) {}
COptions_Write::~COptions_Write(void) {}

/////////////////////////////////////////////////////////////////////////////

DWORD   COptions_Write::Attributes   (void) const { return 0; }
DWORD   COptions_Write::DesiredAccess(void) const {
	DWORD  dw_opts = TBase::DesiredAccess();
	if (this->Overwrite() == true )  dw_opts |= FILE_WRITE_DATA ;
	if (this->Overwrite() == false)  dw_opts |= FILE_APPEND_DATA;
	return dw_opts;
}
DWORD   COptions_Write::Disposition  (void) const {
	DWORD  dw_opts = TBase::Disposition();
	if (TBase::Get(_atts::e_replace ) == true ) dw_opts |= FILE_SUPERSEDE;
	if (TBase::Get(_atts::e_existing) == false) dw_opts |= FILE_OPEN_IF;

	if (this->Overwrite() == true)
		dw_opts |= FILE_OVERWRITE_IF;
	return dw_opts;
}
DWORD   COptions_Write::SharedAccess (void) const {
	DWORD  dw_opts = TBase::SharedAccess();
	if (TBase::Get(_atts::e_exclusive) == false) dw_opts |= FILE_SHARE_READ;
	return dw_opts;
}

/////////////////////////////////////////////////////////////////////////////

bool    COptions_Write::Overwrite(void) const { return m_b_overwrite; }
bool&   COptions_Write::Overwrite(void)       { return m_b_overwrite; }

/////////////////////////////////////////////////////////////////////////////

CPath:: CPath (void) {}
CPath::~CPath (void) {}

/////////////////////////////////////////////////////////////////////////////
const
CStringW&   CPath::Dir (void) const { return m_dir ; }
CStringW&   CPath::Dir (void)       { return m_dir ; }
const bool  CPath::Is  (void) const { return(m_dir.IsValid() && m_name.IsValid()); }
const
CStringW&   CPath::FileName(void) const { return m_name; }
CStringW&   CPath::FileName(void)       { return m_name; }