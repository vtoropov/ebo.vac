/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Jun-2020 at 4:50:10p, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack virtual audio cable driver installation handler interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.vac.ins.hand.h"

using namespace ebo::vac::handler;

/////////////////////////////////////////////////////////////////////////////

CSetupHandler:: CSetupHandler(void) { m_error <<__MODULE__ << S_OK >> __MODULE__; }
CSetupHandler:: CSetupHandler(const CSetupHandler& _ref) : CSetupHandler() { *this = _ref; }
CSetupHandler::~CSetupHandler(void) {}

/////////////////////////////////////////////////////////////////////////////
const
TSetupCmd&   CSetupHandler::Command(void) const { return m_command; }
TSetupCmd&   CSetupHandler::Command(void)       { return m_command; }
TErrorRef    CSetupHandler::Error  (void) const { return m_error; }
const
CSetupPath&  CSetupHandler::INF    (void) const { return m_inf_file; }
CSetupPath&  CSetupHandler::INF    (void)       { return m_inf_file; }
const
CSetupPath&  CSetupHandler::Install(void) const { return m_setup; }
CSetupPath&  CSetupHandler::Install(void)       { return m_setup; }

/////////////////////////////////////////////////////////////////////////////

CSetupHandler& CSetupHandler::operator = (const CSetupHandler& _ref) {

	this->Command() = _ref.Command();
	this->m_error   = _ref.Error();
	this->INF()     = _ref.INF();
	this->Install() = _ref.Install();
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CSetupHandler_Default:: CSetupHandler_Default(void) : TBase() {
	this->Command() = CSetupCmd_Enum::Default();
	this->INF() = CSetupPath(_T("[#err: INF file is not available]"), false);
	this->Install() = CSetupPath(_T("[#err: not applicable]"), false);
}
CSetupHandler_Default::~CSetupHandler_Default(void) {}