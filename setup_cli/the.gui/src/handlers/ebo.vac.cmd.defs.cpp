/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Jun-2020 at 9:42:35a, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack virtual audio cable driver coomand interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.vac.cmd.defs.h"

using namespace ebo::vac::handler;

/////////////////////////////////////////////////////////////////////////////

CDrvCommand:: CDrvCommand(void) : m_id(0) {}
CDrvCommand:: CDrvCommand(const CDrvCommand& _ref) : CDrvCommand() { *this = _ref; }
CDrvCommand:: CDrvCommand(const DWORD _id, LPCTSTR _lp_sz_cap) : m_id(_id), m_caption(_lp_sz_cap) {}
CDrvCommand::~CDrvCommand(void) {}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR     CDrvCommand::Caption (void) const { return m_caption.GetString(); }
CAtlString& CDrvCommand::Caption (void)       { return m_caption; }
DWORD       CDrvCommand::ID (void) const      { return m_id; }
DWORD&      CDrvCommand::ID (void)            { return m_id; }
const bool  CDrvCommand::Is (void) const      { return m_id > 0 && m_caption.IsEmpty() == false; }
const bool  CDrvCommand::Is (const CDrvCommand& _cmd) const {
	  bool b_same = true;
	  if  (b_same) b_same = this->ID() == _cmd.ID();
	  if  (b_same) b_same = 0 == this->m_caption.CompareNoCase(_cmd.Caption());

	  return b_same;
}

/////////////////////////////////////////////////////////////////////////////

CDrvCommand&   CDrvCommand::operator = (const CDrvCommand& _ref) {
	this->ID() = _ref.ID();
	this->Caption() = _ref.Caption();
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CDrvCommand_Enum:: CDrvCommand_Enum(void) {
	try {
		m_cmds.push_back(CDrvCommand(1, _T("Enable Driver")));
		m_cmds.push_back(CDrvCommand(2, _T("Disable Driver")));
	}
	catch (const ::std::bad_alloc&) {}
}
CDrvCommand_Enum::~CDrvCommand_Enum(void) {}

/////////////////////////////////////////////////////////////////////////////

const INT     CDrvCommand_Enum::Has (const CDrvCommand& _cmd) const {
	INT n_found = CSelection::not_selected;

	for (size_t i_ = 0; i_ < m_cmds.size(); i_++) {
		if (m_cmds[i_].Is(_cmd) == true) {
			n_found = static_cast<INT>(i_); break;
		}
	}
	if (n_found == CSelection::not_selected && m_cmds.empty() == false)
		n_found  = CSelection().Default();
	return n_found;
}
const
TDrvCommands&    CDrvCommand_Enum::Raw (void) const { return m_cmds; }

/////////////////////////////////////////////////////////////////////////////

CDrvCommand      CDrvCommand_Enum::Default(void) { return CDrvCommand(1, _T("Enable Driver")); }

/////////////////////////////////////////////////////////////////////////////

CExeMethod:: CExeMethod(void) : m_id(0) {}
CExeMethod:: CExeMethod(const CExeMethod& _ref) : CExeMethod() { *this = _ref; }
CExeMethod:: CExeMethod(const DWORD _id, LPCTSTR _lp_sz_cap) : m_id(_id), m_caption(_lp_sz_cap) {}
CExeMethod::~CExeMethod(void) {}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR      CExeMethod::Caption(void) const { return m_caption.GetString(); }
CAtlString&  CExeMethod::Caption(void)       { return m_caption; }
DWORD        CExeMethod::ID (void) const     { return m_id; }
DWORD&       CExeMethod::ID (void)           { return m_id; }
const bool   CExeMethod::Is (void) const     { return this->ID() != 0 && this->m_caption.IsEmpty() == false; }
const bool   CExeMethod::Is (const CExeMethod& _ref) const {
	  bool b_same = true;
	  if ( b_same ) b_same = this->ID() == _ref.ID();
	  if ( b_same ) b_same = 0 == m_caption.CompareNoCase(_ref.Caption());

	  return b_same;
}

/////////////////////////////////////////////////////////////////////////////

CExeMethod&  CExeMethod::operator = (const CExeMethod& _ref) {
	this->ID() = _ref.ID();
	this->Caption() = _ref.Caption();
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CExeMethod_Enum:: CExeMethod_Enum(void) {
	try {
		m_methods.push_back(CExeMethod(1, _T("Direct Diver Call")));
		m_methods.push_back(CExeMethod(2, _T("Hidden File")));
		m_methods.push_back(CExeMethod(3, _T("Registry Entry")));
	}
	catch (const ::std::bad_alloc&) {}
}
CExeMethod_Enum::~CExeMethod_Enum(void) {}

/////////////////////////////////////////////////////////////////////////////

const INT   CExeMethod_Enum::Has (const CExeMethod& _method) const {

	INT n_found = CSelection::not_selected;

	for (size_t i_ = 0; i_ < m_methods.size(); i_++) {

		if (m_methods[i_].Is(_method) == true) {
			n_found = static_cast<INT>(i_); break;
		}
	}
	if (n_found == CSelection::not_selected && m_methods.empty() == false)
		n_found  = CSelection().Default();

	return n_found;
}
const
TMethods&   CExeMethod_Enum::Raw (void) const { return m_methods; }

/////////////////////////////////////////////////////////////////////////////

CExeMethod  CExeMethod_Enum::Default(void) { return CExeMethod(3, _T("Registry Entry")); }