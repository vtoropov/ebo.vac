#ifndef _EBOVACCMDHAND_H_9C990797_E4B5_4E47_969F_C2BCBC03EDE2_INCLUDED
#define _EBOVACCMDHAND_H_9C990797_E4B5_4E47_969F_C2BCBC03EDE2_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Jun-2020 at 11:05:59a, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack virtual audio cable driver command handler interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "ebo.vac.cmd.defs.h"

namespace ebo { namespace vac { namespace handler {

	using shared::sys_core::CError;

	class CCmdHandler {
	private:
		CError        m_error  ;
		CDrvCommand   m_command;
		CExeMethod    m_method ;

	public:
		 CCmdHandler (void);
		~CCmdHandler (void);

	public:
		const
		CDrvCommand&  Command (void) const;
		CDrvCommand&  Command (void)      ;
		TErrorRef     Error   (void) const;
		const
		CExeMethod&   Method  (void) const;
		CExeMethod&   Method  (void)      ;
	};

	class CCmdHandler_Default : public CCmdHandler {
	                           typedef CCmdHandler TBase;
	public:
		 CCmdHandler_Default (void);
		~CCmdHandler_Default (void);
	};

}}}

#endif/*_EBOVACCMDHAND_H_9C990797_E4B5_4E47_969F_C2BCBC03EDE2_INCLUDED*/