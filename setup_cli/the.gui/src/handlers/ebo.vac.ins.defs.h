#ifndef _EBOVACINSDEFS_H_AAD6081E_599B_4DEC_8CB6_0E28D103AC20_INCLUDED
#define _EBOVACINSDEFS_H_AAD6081E_599B_4DEC_8CB6_0E28D103AC20_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Jun-2020 at 3:59:52p, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack virtual audio cable driver installation status interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "ebo.vac.cnn.data.h"

namespace ebo { namespace vac { namespace handler {

	using ebo::vac::data::CSelection;

	using shared::sys_core::CError;

	class CSetupCommand {
	private:
		DWORD        m_id;
		CAtlString   m_caption;

	public:
		 CSetupCommand (void);
		 CSetupCommand (const CSetupCommand&);
		 CSetupCommand (const DWORD _cmd_id, LPCTSTR _lp_sz_cap);
		~CSetupCommand (void);

	public:
		LPCTSTR      Caption(void) const;
		CAtlString&  Caption(void)      ;
		DWORD        ID(void) const;
		DWORD&       ID(void)      ;
		const bool   Is(void) const;
		const bool   Is(const CSetupCommand&) const;

	public:
		CSetupCommand& operator = (const CSetupCommand&);
	};

	typedef ::std::vector<CSetupCommand>   TSetupCmds;

	class CSetupCmd_Enum {
	private:
		TSetupCmds   m_cmds;

	public:
		 CSetupCmd_Enum (void);
		~CSetupCmd_Enum (void);

	public:
		const INT   Has (const CSetupCommand&) const;
		const
		TSetupCmds& Raw (void) const;

	public:
		static
		CSetupCommand Default(void);
	};

	class CSetupPath {
	private:
		CAtlString   m_path;
		bool         m_enabled;

	public:
		 CSetupPath (void);
		 CSetupPath (const CSetupPath&);
		 CSetupPath (LPCTSTR _lp_sz_path, const bool _b_enabled);
		~CSetupPath (void);

	public:
		bool     Enabled(void) const;
		bool&    Enabled(void)      ;
		bool        Is  (void) const;
		LPCTSTR     Path(void) const;
		CAtlString& Path(void) ;

	public:
		CSetupPath& operator = (const CSetupPath&);

	public:
		static CSetupPath Default(void);
	};

}}}

typedef ebo::vac::handler::CSetupCommand  TSetupCmd;

#endif/*_EBOVACINSDEFS_H_AAD6081E_599B_4DEC_8CB6_0E28D103AC20_INCLUDED*/