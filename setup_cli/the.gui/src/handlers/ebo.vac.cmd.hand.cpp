/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Jun-2020 at 11:16:56a, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack virtual audio cable driver command handler interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.vac.cmd.hand.h"

using namespace ebo::vac::handler;

/////////////////////////////////////////////////////////////////////////////

CCmdHandler:: CCmdHandler(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CCmdHandler::~CCmdHandler(void) {}

/////////////////////////////////////////////////////////////////////////////
const
CDrvCommand&  CCmdHandler::Command (void) const { return m_command; }
CDrvCommand&  CCmdHandler::Command (void)       { return m_command; }
TErrorRef     CCmdHandler::Error   (void) const { return m_error ;  }
const
CExeMethod&   CCmdHandler::Method  (void) const { return m_method;  }
CExeMethod&   CCmdHandler::Method  (void)       { return m_method;  }

/////////////////////////////////////////////////////////////////////////////

CCmdHandler_Default:: CCmdHandler_Default(void) : TBase() {
	TBase::Command() = CDrvCommand_Enum::Default();
	TBase::Method () = CExeMethod_Enum::Default();
}
CCmdHandler_Default::~CCmdHandler_Default(void) {}

/////////////////////////////////////////////////////////////////////////////