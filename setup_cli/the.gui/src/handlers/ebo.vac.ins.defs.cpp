/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Jun-2020 at 4:16:32p, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack virtual audio cable driver installation status interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.vac.ins.defs.h"

using namespace ebo::vac::handler;

/////////////////////////////////////////////////////////////////////////////

CSetupCommand:: CSetupCommand(void) : m_id(0) {}
CSetupCommand:: CSetupCommand(const CSetupCommand& _ref) : CSetupCommand() { *this = _ref; }
CSetupCommand:: CSetupCommand(const DWORD _cmd_id, LPCTSTR _lp_sz_cap) : m_id(_cmd_id), m_caption(_lp_sz_cap) {}
CSetupCommand::~CSetupCommand(void) {}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR         CSetupCommand::Caption(void) const { return m_caption.GetString(); }
CAtlString&     CSetupCommand::Caption(void)       { return m_caption; }
DWORD           CSetupCommand::ID(void) const      { return m_id; }
DWORD&          CSetupCommand::ID(void)            { return m_id; }
const bool      CSetupCommand::Is(void) const      { return this->ID() > 0 && m_caption.IsEmpty() == false; }
const bool      CSetupCommand::Is(const CSetupCommand& _cmd) const {

	bool b_same = true;
	if  (b_same) b_same = this->ID() == _cmd.ID();
	if  (b_same) b_same = 0 == this->m_caption.CompareNoCase(_cmd.Caption());

	return b_same;
}

/////////////////////////////////////////////////////////////////////////////

CSetupCommand& CSetupCommand::operator = (const CSetupCommand& _ref) {

	this->ID() = _ref.ID();
	this->Caption() = _ref.Caption();
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CSetupCmd_Enum:: CSetupCmd_Enum(void) {
	try {
		m_cmds.push_back(CSetupCommand(1, _T("Get Driver Status")));
		m_cmds.push_back(CSetupCommand(2, _T("Install Driver")));
		m_cmds.push_back(CSetupCommand(3, _T("Uninstall Driver")));
	}
	catch (const ::std::bad_alloc&) {}
}
CSetupCmd_Enum::~CSetupCmd_Enum(void) {}

/////////////////////////////////////////////////////////////////////////////

const INT   CSetupCmd_Enum::Has(const CSetupCommand& _cmd) const {

	INT n_found = CSelection::not_selected;

	for (size_t i_ = 0; i_ < m_cmds.size(); i_++) {

		if (m_cmds[i_].Is(_cmd) == true) {
			n_found = static_cast<INT>(i_); break;
		}
	}
	if (n_found == CSelection::not_selected && m_cmds.empty() == false)
		n_found  = CSelection().Default();

	return n_found;
}
const
TSetupCmds& CSetupCmd_Enum::Raw(void) const { return m_cmds; }

/////////////////////////////////////////////////////////////////////////////

CSetupCommand CSetupCmd_Enum::Default(void) { return CSetupCommand(1, _T("Get Driver Status")); }

/////////////////////////////////////////////////////////////////////////////

CSetupPath:: CSetupPath (void) : m_enabled(false) {}
CSetupPath:: CSetupPath (const CSetupPath& _ref) : CSetupPath() { *this = _ref; }
CSetupPath:: CSetupPath (LPCTSTR _lp_sz_path, const bool _enable) : m_path(_lp_sz_path), m_enabled(_enable) {}
CSetupPath::~CSetupPath (void) {}

/////////////////////////////////////////////////////////////////////////////

bool     CSetupPath::Enabled(void) const { return m_enabled; }
bool&    CSetupPath::Enabled(void)       { return m_enabled; }
bool        CSetupPath::Is  (void) const { return m_path.IsEmpty() == false; }
LPCTSTR     CSetupPath::Path(void) const { return m_path.GetString(); }
CAtlString& CSetupPath::Path(void)       { return m_path; }

/////////////////////////////////////////////////////////////////////////////

CSetupPath& CSetupPath::operator = (const CSetupPath& _ref) {
	this->Enabled() = _ref.Enabled();
	this->Path() = _ref.Path();
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CSetupPath CSetupPath::Default(void) { return CSetupPath(_T("[the path is not accessible]"), false); }

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////