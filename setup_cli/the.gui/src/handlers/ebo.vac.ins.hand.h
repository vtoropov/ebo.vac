#ifndef _EBOVACINSHAND_H_86662B2A_64D9_4402_8252_B96BD2F115FE_INCLUDED
#define _EBOVACINSHAND_H_86662B2A_64D9_4402_8252_B96BD2F115FE_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Jun-2020 at 4:38:57p, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack virtual audio cable driver installation handler interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "ebo.vac.ins.defs.h"

namespace ebo { namespace vac { namespace handler {

	using shared::sys_core::CError;

	class CSetupHandler {
	private:
		CError       m_error;
		TSetupCmd    m_command;
		CSetupPath   m_setup;    // setup utility path; can be either absolute or relative;
		CSetupPath   m_inf_file; // INF setup file; can be either absolute or relative;
	public:
		 CSetupHandler (void);
		 CSetupHandler (const CSetupHandler&);
		~CSetupHandler (void);

	public:
		const
		TSetupCmd&   Command(void) const;
		TSetupCmd&   Command(void)      ;
		TErrorRef    Error  (void) const;
		const
		CSetupPath&  INF    (void) const;
		CSetupPath&  INF    (void)      ;
		const
		CSetupPath&  Install(void) const;
		CSetupPath&  Install(void)      ;

	public:
		CSetupHandler& operator = (const CSetupHandler&);
	};

	class CSetupHandler_Default : public CSetupHandler {
	                             typedef CSetupHandler TBase;
	public:
		 CSetupHandler_Default (void);
		~CSetupHandler_Default (void);
	};

}}}

#endif/*_EBOVACINSHAND_H_86662B2A_64D9_4402_8252_B96BD2F115FE_INCLUDED*/