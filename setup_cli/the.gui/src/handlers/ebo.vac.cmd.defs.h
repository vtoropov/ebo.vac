#ifndef _EBOVACCMDDEFS_H_0E574911_32B9_417B_931D_198DD47B7ACB_INCLUDED
#define _EBOVACCMDDEFS_H_0E574911_32B9_417B_931D_198DD47B7ACB_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Jun-2020 at 9:33:47a, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack virtual audio cable driver command interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "ebo.vac.cnn.data.h"

namespace ebo { namespace vac { namespace handler {

	using ebo::vac::data::CSelection;

	using shared::sys_core::CError;

	class CDrvCommand {
	private:
		DWORD      m_id;
		CAtlString m_caption;
	public:
		 CDrvCommand (void);
		 CDrvCommand (const CDrvCommand&);
		 CDrvCommand (const DWORD _id, LPCTSTR _lp_sz_cap);
		~CDrvCommand (void);

	public:
		LPCTSTR     Caption (void) const;
		CAtlString& Caption (void);
		DWORD       ID (void) const ;
		DWORD&      ID (void)       ;
		const bool  Is (void) const ;
		const bool  Is (const CDrvCommand&) const;

	public:
		CDrvCommand&   operator = (const CDrvCommand&);
	};

	typedef ::std::vector<CDrvCommand>  TDrvCommands;

	class CDrvCommand_Enum {
	private:
		TDrvCommands     m_cmds;

	public:
		 CDrvCommand_Enum (void);
		~CDrvCommand_Enum (void);

	public:
		const INT     Has (const CDrvCommand&) const;
		const
		TDrvCommands& Raw (void) const;
	public:
		static
		CDrvCommand   Default(void);
	};

	class CExeMethod {
	private:
		DWORD      m_id;
		CAtlString m_caption;

	public:
		 CExeMethod (void);
		 CExeMethod (const CExeMethod&);
		 CExeMethod (const DWORD _id, LPCTSTR _lps_sz_cap);
		~CExeMethod (void);

	public:
		LPCTSTR     Caption(void) const;
		CAtlString& Caption(void)      ;
		DWORD       ID (void) const;
		DWORD&      ID (void)      ;
		const bool  Is (void) const;
		const bool  Is (const CExeMethod&) const;

	public:
		CExeMethod&  operator = (const CExeMethod&);
	};

	typedef ::std::vector<CExeMethod> TMethods;

	class CExeMethod_Enum {
	private:
		TMethods  m_methods;

	public:
		 CExeMethod_Enum(void);
		~CExeMethod_Enum(void);

	public:
		const INT   Has (const CExeMethod&) const;
		const
		TMethods&   Raw (void) const;

	public:
		static
		CExeMethod  Default(void);
	};

}}}

typedef ebo::vac::handler::CDrvCommand_Enum    TDrvCmdEnum;
typedef ebo::vac::handler::CExeMethod_Enum     TMethodEnum;

#endif/*_EBOVACCMDDEFS_H_0E574911_32B9_417B_931D_198DD47B7ACB_INCLUDED*/