//
// Created by Tech_dog (ebontrop@gmail.com) on 12-May-2020 at 6:24:55a, UTC+7, Novosibirsk, Tuesday;
// This is FakeGPS driver configuration dialog identifier declaration file.
// -----------------------------------------------------------------------------
// Adopted to Ebo VAC driver client project on 22-Jun-2020 at 8:02:11a, UTC+7, Monday;
//

#pragma region __cfg_dlg

#define IDD_EBO_VAC_DRV_DLG        3301
#define IDR_EBO_VAC_DRV_DLG_ICO    3303

#define IDC_EBO_VAC_DRV_SAVE       3305
#define IDC_EBO_VAC_DRV_BAN        3307
#define IDR_EBO_VAC_DRV_BAN        IDC_EBO_VAC_DRV_BAN
#define IDC_EBO_VAC_DRV_TAB        3309
#define IDC_EBO_VAC_DRV_SEP        3311

#pragma region __cfg_1st_page

#define IDD_EBO_VAC_DRV_1ST_PAG    3401

#define IDC_EBO_VAC_DRV_DEV_AVA    3403
#define IDR_EBO_VAC_DRV_DEV_AVA    IDC_EBO_VAC_DRV_DEV_AVA
#define IDC_EBO_VAC_DRV_DEV_CAP    3405
#define IDC_EBO_VAC_DRV_DEV_SEP    3407

#define IDC_EBO_VAC_DRV_INP_LST    3409
#define IDC_EBO_VAC_DRV_INP_LAB    3411

#define IDC_EBO_VAC_DRV_OUT_LST    3413
#define IDC_EBO_VAC_DRV_OUT_LAB    3415

#define IDC_EBO_VAC_DRV_SMP_AVA    3417
#define IDR_EBO_VAC_DRV_SMP_AVA    IDC_EBO_VAC_DRV_SMP_AVA
#define IDC_EBO_VAC_DRV_SMP_CAP    3419
#define IDC_EBO_VAC_DRV_SMP_SEP    3421
#define IDC_EBO_VAC_DRV_SMP_BUF    3423

#define IDC_EBO_VAC_DRV_SMP_LAB    3425
#define IDC_EBO_VAC_DRV_SMP_FRQ    3427

#define IDC_EBO_VAC_DRV_BIT_LAB    3429
#define IDC_EBO_VAC_DRV_BIT_LST    3431

#define IDC_EBO_VAC_DRV_CHL_LAB    3433
#define IDC_EBO_VAC_DRV_CHL_LST    3435


#pragma endregion

#pragma region __cfg_2nd_page

#define IDD_EBO_VAC_DRV_2ND_PAG    3501

#define IDC_EBO_VAC_DRV_CMD_AVA    3503
#define IDR_EBO_VAC_DRV_CMD_AVA    IDC_EBO_VAC_DRV_CMD_AVA
#define IDC_EBO_VAC_DRV_CMD_CAP    3505
#define IDC_EBO_VAC_DRV_CMD_SEP    3507

#define IDC_EBO_VAC_DRV_CMD_LAB    3509
#define IDC_EBO_VAC_DRV_CMD_LST    3511

#define IDC_EBO_VAC_DRV_PRF_AVA    3513
#define IDR_EBO_VAC_DRV_PRF_AVA    IDC_EBO_VAC_DRV_PRF_AVA
#define IDC_EBO_VAC_DRV_PRF_CAP    3515
#define IDC_EBO_VAC_DRV_PRF_SEP    3517

#define IDC_EBO_VAC_DRV_PRF_LAB    3519
#define IDC_EBO_VAC_DRV_PRF_LST    3521

#define IDC_EBO_VAC_DRV_LOG_AVA    3523
#define IDR_EBO_VAC_DRV_LOG_AVA    IDC_EBO_VAC_DRV_LOG_AVA
#define IDC_EBO_VAC_DRV_LOG_CAP    3525
#define IDC_EBO_VAC_DRV_LOG_SEP    3527

#define IDC_EBO_VAC_DRV_LOG_LAB    3529
#define IDC_EBO_VAC_DRV_LOG_PTH    3531
#define IDC_EBO_VAC_DRV_LOG_BTN    3533

#pragma endregion

#pragma region __cfg_3rd_page

#define IDD_EBO_VAC_DRV_3RD_PAG    3601

#define IDC_EBO_VAC_DRV_STA_AVA    3603
#define IDR_EBO_VAC_DRV_STA_AVA    IDC_EBO_VAC_DRV_STA_AVA
#define IDC_EBO_VAC_DRV_STA_CAP    3605
#define IDC_EBO_VAC_DRV_STA_SEP    3607

#define IDC_EBO_VAC_DRV_MOD_LAB    3609
#define IDC_EBO_VAC_DRV_MOD_CMD    3611

#define IDC_EBO_VAC_DRV_UTL_LAB    3613
#define IDC_EBO_VAC_DRV_UTL_PAT    3615
#define IDC_EBO_VAC_DRV_UTL_BRW    3617

#define IDC_EBO_VAC_DRV_INF_PAT    3619
#define IDC_EBO_VAC_DRV_INF_BRW    3621

#define IDC_EBO_VAC_DRV_INF_IMG    3623
#define IDR_EBO_VAC_DRV_INF_IMG    IDC_EBO_VAC_DRV_INF_IMG 
#define IDC_EBO_VAC_DRV_INF_TXT    3625
#define IDS_EBO_VAC_DRV_INF_TXT    3627

#pragma endregion

#pragma endregion


