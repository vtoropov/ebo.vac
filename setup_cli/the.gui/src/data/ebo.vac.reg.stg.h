#ifndef _EBOVACREGSTG_H_F6E42514_1F61_4859_80A9_427284D37898_INCLUDED
#define _EBOVACREGSTG_H_F6E42514_1F61_4859_80A9_427284D37898_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Jun-2020 at 6:30:31a, UTC+7, Novosibirsk, Saturday;
	This is Ebo Pack virtual audio cable driver connection data interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.reg.hive.h"

#include "ebo.vac.cnn.data.h"

namespace ebo { namespace vac { namespace data {

	using shared::sys_core::CError;
	using shared::registry::CRegistryStg;
	using shared::registry::CRegOptions ;

	class CRegistry_Base {
	protected:
		CRegistryStg   m_stg;
		CError         m_error;

	protected:
		 CRegistry_Base (void);
		~CRegistry_Base (void);

	public:
		TErrorRef     Error (void) const;
	};

	class CReg_Connects : public CRegistry_Base {
	                     typedef CRegistry_Base TBase;

	public:
		 CReg_Connects (void);
		~CReg_Connects (void);

	public:
		CReg_Connects& operator << (const CInputDevices& );  // saves selected input device;
		CReg_Connects& operator >> (      CInputDevices& );  // loads selected input device;

		CReg_Connects& operator << (const COutputDevices&);  // saves selected output device;
		CReg_Connects& operator >> (      COutputDevices&);  // loads selected output device;
	};
}}}
#endif/*_EBOVACREGSTG_H_F6E42514_1F61_4859_80A9_427284D37898_INCLUDED*/