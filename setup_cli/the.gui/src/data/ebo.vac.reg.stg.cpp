/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Jun-2020 at 6:53:30a, UTC+7, Novosibirsk, Saturday;
	This is Ebo Pack virtual audio cable driver connection data interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.vac.reg.stg.h"

using namespace ebo::vac::data;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace vac { namespace data { namespace _impl {

	class CDrvCfg_Reg {
	public:
		static LPCTSTR Root(void) { static LPCTSTR lp_sz_fld = _T("Software\\Vac\\"); return lp_sz_fld; }
	};
	class CDrvCfg_Connect : public CDrvCfg_Reg {
	public:
		LPCTSTR Folder_In (void) const {
			static CAtlString cs_in; if (cs_in.IsEmpty()) {
				cs_in += CDrvCfg_Reg::Root();
				cs_in += _T("Input");
			}
			return (LPCTSTR)cs_in;
		}
		LPCTSTR Folder_Out(void) const {
			static CAtlString cs_in; if (cs_in.IsEmpty()) {
				cs_in += CDrvCfg_Reg::Root();
				cs_in += _T("Output");
			}
			return (LPCTSTR)cs_in;
		}
		LPCTSTR Selected (void) const {
			static LPCTSTR lp_sz_value = _T("Selected"); return lp_sz_value;
		}
	};

}}}}
using namespace ebo::vac::data::_impl;
/////////////////////////////////////////////////////////////////////////////

CRegistry_Base:: CRegistry_Base(void) : m_stg(HKEY_CURRENT_USER, CRegOptions::eDoNotModifyPath) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CRegistry_Base::~CRegistry_Base(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef     CRegistry_Base::Error (void) const { return m_error; }

/////////////////////////////////////////////////////////////////////////////

CReg_Connects:: CReg_Connects(void) : TBase() {}
CReg_Connects::~CReg_Connects(void) {}

/////////////////////////////////////////////////////////////////////////////

CReg_Connects& CReg_Connects::operator << (const CInputDevices& _inputs) {
	m_error << __MODULE__ << S_OK;

	HRESULT hr_ = m_stg.Save(CDrvCfg_Connect().Folder_In(), CDrvCfg_Connect().Selected(), (LONG) _inputs.Selected().Index());
	if (FAILED(hr_))
		m_error = m_stg.Error();

	return *this;
}

CReg_Connects& CReg_Connects::operator >> (      CInputDevices& _inputs) {
	m_error << __MODULE__ << S_OK;

	LONG n_selected = -1;

	HRESULT hr_ = m_stg.Load(CDrvCfg_Connect().Folder_In(), CDrvCfg_Connect().Selected(), n_selected, -1);
	if (FAILED(hr_))
		m_error = m_stg.Error();
	else
		_inputs.Selected() = n_selected;

	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CReg_Connects& CReg_Connects::operator << (const COutputDevices& _outputs) {
	m_error << __MODULE__ << S_OK;

	HRESULT hr_ = m_stg.Save(CDrvCfg_Connect().Folder_Out(), CDrvCfg_Connect().Selected(), (LONG) _outputs.Selected().Index());
	if (FAILED(hr_))
		m_error = m_stg.Error();

	return *this;
}

CReg_Connects& CReg_Connects::operator >> (      COutputDevices& _outputs) {
	m_error << __MODULE__ << S_OK;

	LONG n_selected = -1;

	HRESULT hr_ = m_stg.Load(CDrvCfg_Connect().Folder_Out(), CDrvCfg_Connect().Selected(), n_selected, -1);
	if (FAILED(hr_))
		m_error = m_stg.Error();
	else
		_outputs.Selected() = n_selected;

	return *this;
}

/////////////////////////////////////////////////////////////////////////////