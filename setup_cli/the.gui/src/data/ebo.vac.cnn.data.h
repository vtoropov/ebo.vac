#ifndef _EBOVACCNNDAATA_H_3EF616AA_C366_4C01_836F_D666A77BA311_INCLUDED
#define _EBOVACCNNDAATA_H_3EF616AA_C366_4C01_836F_D666A77BA311_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Jun-2020 at 3:11:18a, UTC+7, Novosibirsk, Saturday;
	This is Ebo Pack virtual audio cable driver connection data interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "ebo.vac.smp.data.h"

namespace ebo { namespace vac { namespace data {

	using shared::sys_core::CError;

	class CDevice {
	protected:
		GUID        m_guid;
		CAtlString  m_name;

	public:
		 CDevice (void);
		 CDevice (const CDevice&);
		~CDevice (void);

	public:
		const
		GUID&       ID  (void) const;
		GUID&       ID  (void)      ;
		const bool  Is  (void) const; // returns true if guid is not NULL_GUID and name is not empty;
		const bool  Is  (const CDevice&) const; // checks an input objects for equality; TODO: a validity of objects is not taken into account;
		LPCTSTR     Name(void) const;
		CAtlString& Name(void)      ;

	public:
		CDevice& operator <<(const GUID&);
		CDevice& operator <<(LPCTSTR _lp_sz_name);

	public:
		CDevice& operator = (const CDevice&);

	public:
		operator   bool (void) const; // returns true if guid is not NULL_GUID and name is not empty;
	};

	bool operator == (const CDevice& _ls_ref, const CDevice& _rs_ref);
	bool operator != (const CDevice& _ls_ref, const CDevice& _rs_ref);

	typedef ::std::vector<CDevice> TDevices;

	class CDevice_Enum_Base {
	protected:
		TDevices   m_devices;
		CError     m_error  ;
		CSelection m_select ;
		
	protected:
		 CDevice_Enum_Base (void);
		~CDevice_Enum_Base (void);

	public:
		virtual HRESULT    Load (void) PURE ;
		virtual HRESULT    Save (void) PURE ;
	public:
		const
		TDevices&    Devices (void) const;
		TErrorRef    Error   (void) const;
		const INT    Has     (const CDevice&) const; // returns an index if an object provided is found, otherwise, -1;
		TSelectedRef Selected(void) const;  // deprecated;
		TSelected    Selected(void)      ;  // deprecated;
	};

	class CInputDevices : public CDevice_Enum_Base {
	                     typedef CDevice_Enum_Base TBase;
	public:
		 CInputDevices (void);
		~CInputDevices (void);

	public:
#pragma warning(disable : 4481)
		HRESULT    Load (void) override sealed;
		HRESULT    Save (void) override sealed;
#pragma warning(default : 4481)
	private:
		static
		BOOL CALLBACK DSEnumCallback(LPGUID lpGuid, LPCWSTR lpcstrDescription, LPCWSTR lpcstrModule, LPVOID lpContext);
	};
	// https://docs.microsoft.com/en-us/windows/win32/api/mmdeviceapi/nf-mmdeviceapi-immdeviceenumerator-getdefaultaudioendpoint
	class COutputDevices : public CDevice_Enum_Base {
	                      typedef CDevice_Enum_Base TBase;
	public:
		 COutputDevices (void);
		~COutputDevices (void);

	public:
#pragma warning(disable : 4481)
		HRESULT    Load (void) override sealed;
		HRESULT    Save (void) override sealed;
#pragma warning(default : 4481)
	};

	class CConnection {
	private:
		CError   m_error ;
		CDevice  m_input ;
		CDevice  m_output;

	public:
		 CConnection (void);
		 CConnection (const CConnection&);
		~CConnection (void);

	public:
		TErrorRef   Error (void) const;
		const
		CDevice&    Input (void) const;
		CDevice&    Input (void)      ;
		const
		CDevice&    Output(void) const;
		CDevice&    Output(void)      ;

	public:
		CConnection& operator << (const CDevice&);  // assignes input device;
		CConnection& operator >> (const CDevice&);  // assignes output device;

	public:
		CConnection& operator =  (const CConnection&);
	};

	class CConnection_Default : public CConnection {
	                           typedef CConnection TBase;
	public:
		 CConnection_Default (void);
		~CConnection_Default (void);
	};
}}}

#endif/*_EBOVACCNNDAATA_H_3EF616AA_C366_4C01_836F_D666A77BA311_INCLUDED*/