/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Jun-2020 at 3:37:03a, UTC+7, Novosibirsk, Saturday;
	This is Ebo Pack virtual audio cable driver connection data interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.vac.cnn.data.h"
#include "ebo.vac.reg.stg.h"

using namespace ebo::vac::data;

/////////////////////////////////////////////////////////////////////////////

CDevice:: CDevice(void) : m_guid(GUID_NULL) {}
CDevice:: CDevice(const CDevice& _ref) : CDevice() { *this = _ref; }
CDevice::~CDevice(void) {}

/////////////////////////////////////////////////////////////////////////////
const
GUID&       CDevice::ID  (void) const { return m_guid; }
GUID&       CDevice::ID  (void)       { return m_guid; }
const bool  CDevice::Is  (void) const {
	static const GUID na_ = GUID_NULL;
	const BOOL b_result = ::IsEqualGUID(na_, this->m_guid);
	return (b_result == FALSE && m_name.IsEmpty() == false);
}
const bool  CDevice::Is  (const CDevice& _ref) const {
	bool b_same = true;
	if  (b_same) b_same = !!::IsEqualGUID(_ref.ID(), this->ID());
	if  (b_same) b_same = (0 == this->m_name.CompareNoCase(_ref.Name()));
	
	return b_same;
}

LPCTSTR     CDevice::Name(void) const { return m_name.GetString(); }
CAtlString& CDevice::Name(void)       { return m_name; }

/////////////////////////////////////////////////////////////////////////////

CDevice&    CDevice::operator <<(const GUID& _guid) {
	const errno_t err_ = ::memcpy_s(&this->m_guid, sizeof(GUID), &_guid, sizeof(GUID));
	if ( 0 != err_) {
		// copying memory or access error;
	}
	return *this;
}
CDevice&    CDevice::operator <<(LPCTSTR _lp_sz_name) {
	this->Name() = _lp_sz_name;
	return *this;
}

CDevice&    CDevice::operator = (const CDevice& _ref) {
	*this  <<_ref.ID();
	*this  <<_ref.Name();
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CDevice::operator   bool (void) const { return this->Is(); }

/////////////////////////////////////////////////////////////////////////////

bool ebo::vac::data::operator == (const CDevice& _ls_ref, const CDevice& _rs_ref) { return  _ls_ref.Is(_rs_ref); }
bool ebo::vac::data::operator != (const CDevice& _ls_ref, const CDevice& _rs_ref) { return !_ls_ref.Is(_rs_ref); }

/////////////////////////////////////////////////////////////////////////////

CDevice_Enum_Base:: CDevice_Enum_Base(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CDevice_Enum_Base::~CDevice_Enum_Base(void) {}

/////////////////////////////////////////////////////////////////////////////
const
TDevices&    CDevice_Enum_Base::Devices (void) const { return m_devices ; }
TErrorRef    CDevice_Enum_Base::Error   (void) const { return m_error   ; }
const INT    CDevice_Enum_Base::Has     (const CDevice& _ref) const {

	INT n_ndx = CSelection::not_selected;
	for (size_t i_ = 0; i_ < m_devices.size(); i_++) {
		if (m_devices[i_].Is(_ref)) {
			n_ndx = static_cast<INT>(i_); break;
		}
	}
	return n_ndx;
}
TSelectedRef CDevice_Enum_Base::Selected(void) const { return m_select  ; }
TSelected    CDevice_Enum_Base::Selected(void)       { return m_select  ; }

/////////////////////////////////////////////////////////////////////////////

CInputDevices:: CInputDevices(void) : TBase() { m_error << __MODULE__ << S_OK >> __MODULE__; }
CInputDevices::~CInputDevices(VOID) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CInputDevices::Load (void) {
	m_error << __MODULE__ << S_OK;

	if (m_devices.empty() == false)
		m_devices.clear();
#if (0)
	const UINT nNumDevs = ::waveInGetNumDevs();
	if ( 0 == nNumDevs ) {
	}
	else {
		WAVEINCAPS caps_ = {0};
		for (UINT i_ = 0; i_ < nNumDevs; i_++) {
			MMRESULT n_result = ::waveInGetDevCaps(i_, &caps_, sizeof(WAVEINCAPS));
			if (MMSYSERR_NOERROR != n_result)
				continue;
			CDevice dev_;
			dev_ = STATIC_GUID_NULL;
			dev_ = caps_.szPname;
			if (dev_.Is())
				m_devices.push_back(dev_);
		}
	}
#else
	// https://docs.microsoft.com/en-us/previous-versions/windows/desktop/ee417545(v=vs.85)
	HRESULT hr_ = ::DirectSoundCaptureEnumerate(CInputDevices::DSEnumCallback, this);
	if (FAILED(hr_)) {
		return (m_error = hr_);
	}
	m_error = (CReg_Connects() >> *this).Error();
#endif

	return m_error;
}

HRESULT    CInputDevices::Save (void) {
	m_error << __MODULE__ << S_OK;

	m_error = (CReg_Connects() << *this).Error();

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

BOOL CALLBACK CInputDevices::DSEnumCallback(LPGUID lpGuid, LPCWSTR lpcstrDescription, LPCWSTR lpcstrModule, LPVOID lpContext) {
	lpGuid; lpcstrDescription; lpcstrModule; lpContext;
	CInputDevices* pInputs = reinterpret_cast<CInputDevices*>(lpContext);
	if (NULL == pInputs)
		return FALSE;

	pInputs->m_error << __MODULE__ << S_OK;

	if (NULL != lpGuid && NULL != pInputs) {

		CDevice dev_;
		dev_ << *lpGuid;
		dev_ << lpcstrDescription;

		if (dev_.Is() == true) {
			try {
				pInputs->m_devices.push_back(dev_);
			}
			catch (const ::std::bad_alloc&) {
				pInputs->m_error = E_OUTOFMEMORY;
				return FALSE;
			}
		}
	}
	return TRUE; // indicates that enumeration must be continued;
}

/////////////////////////////////////////////////////////////////////////////

COutputDevices:: COutputDevices(void) : TBase() { m_error << __MODULE__ << S_OK >> __MODULE__; }
COutputDevices::~COutputDevices(VOID) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    COutputDevices::Load (void) {
	m_error << __MODULE__ << S_OK;

	if (m_devices.empty() == false)
		m_devices.clear();
	const UINT nNumDevs = ::waveOutGetNumDevs();
	if ( 0 == nNumDevs ) 
		return m_error;

	// https://docs.microsoft.com/en-us/windows/win32/api/mmeapi/nf-mmeapi-waveoutgetdevcaps
	WAVEOUTCAPS caps_ = {0};
	for (UINT i_ = 0; i_ < nNumDevs; i_++) {
		MMRESULT n_result = ::waveOutGetDevCaps(i_, &caps_, sizeof(WAVEOUTCAPS));
		if (MMSYSERR_NOERROR != n_result) {
			m_error = E_FAIL; break;
		}
		CDevice dev_;
		dev_ << caps_.szPname;
		if (dev_.Name().IsEmpty()) // cannot use guid check because it is not applicable for Win function;
			continue;
		try {
			m_devices.push_back(dev_);
		}
		catch (const ::std::bad_alloc&) {
			m_error = E_OUTOFMEMORY; break;
		}
	}

	return m_error;
}

HRESULT    COutputDevices::Save (void) {
	m_error << __MODULE__ << S_OK;

	m_error = (CReg_Connects() << *this).Error();

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CConnection:: CConnection(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CConnection:: CConnection(const CConnection& _ref) : CConnection() { *this = _ref; }
CConnection::~CConnection(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CConnection::Error (void) const { return m_error ; }
const
CDevice&    CConnection::Input (void) const { return m_input ; }
CDevice&    CConnection::Input (void)       { return m_input ; }
const
CDevice&    CConnection::Output(void) const { return m_output; }
CDevice&    CConnection::Output(void)       { return m_output; }

/////////////////////////////////////////////////////////////////////////////

CConnection& CConnection::operator << (const CDevice& _dev) { this->Input() = _dev; return *this; }
CConnection& CConnection::operator >> (const CDevice& _dev) { this->Output() = _dev; return *this; }

/////////////////////////////////////////////////////////////////////////////

CConnection& CConnection::operator  = (const CConnection& _ref) {
	this->m_error = _ref.Error();
	this->Input()  = _ref.Input();
	this->Output()  = _ref.Output();
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CConnection_Default:: CConnection_Default(void) : TBase() { HRESULT hr_ = S_OK;
	CInputDevices inputs; hr_ = inputs.Load(); if (SUCCEEDED(hr_) && inputs.Devices().empty() == false) TBase::Input() = inputs.Devices()[0];
	COutputDevices outputs; hr_ = outputs.Load(); if (SUCCEEDED(hr_) && outputs.Devices().empty() == false) TBase::Output() = outputs.Devices()[0];
}
CConnection_Default::~CConnection_Default(void) {}