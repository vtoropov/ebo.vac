/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Jun-2020 at 11:28:00a, UTC+7, Novosibirsk, Saturday;
	This is Ebo Pack virtual audio cable driver sample data interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.vac.smp.data.h"

using namespace ebo::vac::data;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace vac { namespace data { namespace _impl {

	const
	TChannels&  Channel_GetObjects(void) {
		static TChannels channels;

		if (channels.empty() == true) {
			try {
				channels.push_back(CChannel(KSAUDIO_SPEAKER_MONO            , _T("Mono")));
				channels.push_back(CChannel(KSAUDIO_SPEAKER_STEREO          , _T("Stereo")));
				channels.push_back(CChannel(KSAUDIO_SPEAKER_QUAD            , _T("Quadraphonic")));
				channels.push_back(CChannel(KSAUDIO_SPEAKER_SURROUND        , _T("Surround")));
				channels.push_back(CChannel(KSAUDIO_SPEAKER_5POINT1_BACK    , _T("5.1 back")));
				channels.push_back(CChannel(KSAUDIO_SPEAKER_5POINT1_SURROUND, _T("5.1 surround")));
				channels.push_back(CChannel(KSAUDIO_SPEAKER_7POINT1_WIDE    , _T("7.1 wide")));
				channels.push_back(CChannel(KSAUDIO_SPEAKER_7POINT1_SURROUND, _T("7.1 surround")));
			}
			catch (const ::std::bad_alloc&) {}
		}

		return channels;
	}

}}}}
using namespace ebo::vac::data::_impl;
/////////////////////////////////////////////////////////////////////////////

CBits:: CBits(void) {
	static DWORD u_bits[] = {
		8, 16, 24, 32
	};
	try {
		for (INT i_ = 0; i_ < _countof(u_bits); i_++) {
			m_bits.push_back(u_bits[i_]);
		}
	}
	catch (const ::std::bad_alloc&) {}}
CBits::~CBits(void) {}

/////////////////////////////////////////////////////////////////////////////

const INT     CBits::Has(const DWORD _bits) const {
	INT n_found = CSelection::not_selected;

	for (size_t i_ = 0; i_ < m_bits.size(); i_++) {
		if (m_bits[i_] == _bits) {
			n_found = static_cast<INT>(i_); break;
		}
	}
	if (n_found == CSelection::not_selected && m_bits.empty() == false)
		n_found  = CSelection().Default();
	return n_found;
}
const TBits&  CBits::Raw(void) const { return m_bits; }

/////////////////////////////////////////////////////////////////////////////

CSelection:: CSelection(void) : m_index(_ndx::not_selected) {}
CSelection::~CSelection(void) {}

/////////////////////////////////////////////////////////////////////////////

INT         CSelection::Default(void) const { return _ndx::e_default; }
INT         CSelection::Index  (void) const { return m_index; }
INT&        CSelection::Index  (void)       { return m_index; }

/////////////////////////////////////////////////////////////////////////////

CSelection& CSelection::operator = (const INT _n_index) { this->Index() = _n_index; return *this; }

/////////////////////////////////////////////////////////////////////////////

CFreqRates:: CFreqRates(void) {
	static DWORD u_rate[] = {
		8000, 11025, 16000, 22050, 32000, 37800, 44056, 44100, 47250, 48000, 50000, 50400, 64000, 88200, 96000, 176400, 192000
	};
	try {
		for (INT i_ = 0; i_ < _countof(u_rate); i_++) {
			m_rates.push_back(u_rate[i_]);
		}
	}
	catch (const ::std::bad_alloc&) {}
}
CFreqRates::~CFreqRates(void) {}

/////////////////////////////////////////////////////////////////////////////

const INT     CFreqRates::Has (const DWORD _rate) const {
	INT n_found = CSelection::not_selected;

	for (size_t i_ = 0; i_ < m_rates.size(); i_++) {
		if (m_rates[i_] == _rate) {
			n_found = static_cast<INT>(i_); break;
		}
	}
	if (n_found == CSelection::not_selected && m_rates.empty() == false)
		n_found  = CSelection().Default();
	return n_found;
}
const TRates& CFreqRates::Raw (void) const { return m_rates ; }
const
CSelection&   CFreqRates::Selected(void) const { return m_select; }
CSelection&   CFreqRates::Selected(void)       { return m_select; }

/////////////////////////////////////////////////////////////////////////////

CChannel:: CChannel(void) : m_type(0) {}
CChannel:: CChannel(const CChannel& _ref) : CChannel() { *this = _ref; }
CChannel:: CChannel (const DWORD _type, LPCTSTR _lp_sz_cap) : m_type(_type), m_caption(_lp_sz_cap) {}
CChannel::~CChannel(void) {}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR    CChannel::Caption(void) const  { return m_caption.GetString(); }
HRESULT    CChannel::Caption(LPCTSTR _lp_sz_cap) {
	if (NULL == _lp_sz_cap || !::_tcslen(_lp_sz_cap))
		return E_INVALIDARG;
	m_caption = _lp_sz_cap;
	return S_OK;
}
bool       CChannel::Is     (void) const { return (m_type && m_caption.IsEmpty() == false); }
bool       CChannel::Is     (const CChannel& _ref) const {
	bool b_same = true;
	if (b_same) b_same = this->Type() == _ref.Type();
	if (b_same) b_same = 0 == m_caption.CollateNoCase(_ref.Caption());
	return b_same;
}

DWORD      CChannel::Type   (void) const { return  m_type; }
DWORD&     CChannel::Type   (void)       { return  m_type; }

/////////////////////////////////////////////////////////////////////////////

CChannel&  CChannel::operator = (const CChannel& _ref) {
	this->Caption( _ref.Caption() );
	this->Type() = _ref.Type();
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CChannels:: CChannels(void) : m_channels(Channel_GetObjects()) {}
CChannels::~CChannels(void) {}

/////////////////////////////////////////////////////////////////////////////

const INT    CChannels::Has (const CChannel& _ref) const {
	INT n_found = CSelection::not_selected;
	for (size_t i_ = 0; i_ < m_channels.size(); i_++) {
		if (m_channels[i_].Is(_ref) == true) {
			n_found = static_cast<INT>(i_); break;
		}
	}
	if (n_found == CSelection::not_selected && m_channels.empty() == false)
		n_found  = CSelection().Default();
	return n_found;
}
const
TChannels&   CChannels::Raw     (void) const { return m_channels; }
const
CSelection&  CChannels::Selected(void) const { return m_select  ; }
CSelection&  CChannels::Selected(void)       { return m_select  ; }

/////////////////////////////////////////////////////////////////////////////

CSample:: CSample(void) : m_bits(0), m_buffer(0), m_rate(0) {}
CSample::~CSample(void) {}

/////////////////////////////////////////////////////////////////////////////

DWORD     CSample::Bits   (void) const { return m_bits;    }
DWORD&    CSample::Bits   (void)       { return m_bits;    }
DWORD     CSample::Buffer (void) const { return m_buffer ; }
DWORD&    CSample::Buffer (void)       { return m_buffer ; }
const
CChannel& CSample::Channel(void) const { return m_channel; }
CChannel& CSample::Channel(void)       { return m_channel; }
DWORD     CSample::Rate   (void) const { return m_rate ;   }
DWORD&    CSample::Rate   (void)       { return m_rate ;   }

/////////////////////////////////////////////////////////////////////////////

CSample_Default:: CSample_Default(void) : TBase() {
	TBase::Bits() = 16;
	TBase::Buffer() = 64;
	TBase::Channel() = CChannel(KSAUDIO_SPEAKER_STEREO, _T("Stereo"));
	TBase::Rate() = 44100;
}
CSample_Default::~CSample_Default(void) {}