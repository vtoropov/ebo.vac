#ifndef _EBOVACSMPDATA_H_8098EFFB_4934_4333_B961_02CC18E65A56_INCLUDED
#define _EBOVACSMPDATA_H_8098EFFB_4934_4333_B961_02CC18E65A56_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Jun-2020 at 11:14:25a, UTC+7, Novosibirsk, Saturday;
	This is Ebo Pack virtual audio cable driver sample data interface declaration file.
*/
#include "shared.gen.sys.err.h"

namespace ebo { namespace vac { namespace data {

	using shared::sys_core::CError;

	typedef ::std::vector<DWORD>  TBits ;
	class CBits {
	private:
		TBits   m_bits;

	public:
		 CBits (void);
		~CBits (void);

	public:
		const INT    Has(const DWORD _bits) const; // returns index of bits' value provided, otherwise, default index (0);
		const TBits& Raw(void) const;
	};

	class CSelection {
	public:
		enum _ndx : INT {
			not_selected = -1,
			e_default    =  0,
		};
	protected:
		INT          m_index;  // index of selected value from the acceptable range;

	public:
		 CSelection (void);
		~CSelection (void);

	public:
		INT         Default(void) const;
		INT         Index  (void) const;
		INT&        Index  (void)      ;

	public:
		CSelection& operator = (const INT _n_index);
	};

	typedef ::std::vector<DWORD>  TRates;
	// https://en.wikipedia.org/wiki/Sampling_(signal_processing)
	class CFreqRates {
	public:
		TRates       m_rates ;
		CSelection   m_select;

	public:
		 CFreqRates (void);
		~CFreqRates (void);

	public:
		const INT     Has (const DWORD _rate) const; // returns an index of rate provided, if not found, returns default index(0);
		const TRates& Raw (void) const;
		const
		CSelection& Selected(void) const; // returns selected index of element ot ranege; deprecated;
		CSelection& Selected(void)      ; // deprecated;
	};
	// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/ksmedia/ns-ksmedia-ksaudio_channel_config
	class CChannel {
	private:
		DWORD        m_type;
		CAtlString   m_caption;

	public:
		 CChannel (void);
		 CChannel (const CChannel&);
		 CChannel (const DWORD, LPCTSTR);
		~CChannel (void);

	public:
		LPCTSTR  Caption(void) const;
		HRESULT  Caption(LPCTSTR)   ;
		bool     Is     (void) const;
		bool     Is     (const CChannel&) const; // returns true if channel object provided is the same with this object;
		DWORD    Type   (void) const;
		DWORD&   Type   (void)      ;

	public:
		CChannel& operator = (const CChannel&);
	};

	typedef ::std::vector<CChannel> TChannels;

	class CChannels {
	private:
		TChannels   m_channels;
		CSelection  m_select;

	public:
		 CChannels (void);
		~CChannels (void);

	public:
		const INT  Has (const CChannel&) const; // returns index of the channel, if found provided one;
		const
		TChannels&  Raw (void) const;
		const
		CSelection& Selected(void) const; // deprecated;
		CSelection& Selected(void)      ; // deprecated;
	};

	class CSample {
	private:
		DWORD     m_buffer ;
		DWORD     m_rate   ;
		CChannel  m_channel;
		DWORD     m_bits   ;

	public:
		 CSample (void);
		~CSample (void);

	public:
		DWORD     Bits   (void) const;
		DWORD&    Bits   (void)      ;
		DWORD     Buffer (void) const;
		DWORD&    Buffer (void)      ;
		const
		CChannel& Channel(void) const;
		CChannel& Channel(void)      ;
		DWORD     Rate   (void) const;
		DWORD&    Rate   (void)      ;
	};

	class CSample_Default : public CSample {
	                       typedef CSample TBase;
	public:
		 CSample_Default(void);
		~CSample_Default(void);
	};

}}}
typedef       ebo::vac::data::CSelection& TSelected   ;
typedef const ebo::vac::data::CSelection& TSelectedRef;

#endif/*_EBOVACSMPDATA_H_8098EFFB_4934_4333_B961_02CC18E65A56_INCLUDED*/