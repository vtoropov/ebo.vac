#ifndef _EBOFAKEGPSDRVDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED
#define _EBOFAKEGPSDRVDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-May-2020 at 11:43:26p, UTC+7, Novosibirsk, Tuesday;
	This is FakeGPS driver configuration dialog interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver client project on 22-Jun-2020 at 4:43:18p, UTC+7, Monday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.frms.img.ban.h"

#include "ebo.vac.drv.tab.set.h"

namespace ebo { namespace vac { namespace gui {

	using shared::sys_core::CError;

	using ex_ui::frames::CImageBanner;
	using ex_ui::draw::defs::IRenderer;

	using shared::gui::ITabSetCallback;

	class CDrvDlg {
	private:
		class CDrvDlgImpl : public ::ATL::CDialogImpl<CDrvDlgImpl>, ITabSetCallback, IRenderer {
		                   typedef ::ATL::CDialogImpl<CDrvDlgImpl>  TDialog;
			friend class CTraceDlg;
		private:
			CImageBanner      m_banner;
			CDrvTabSet        m_tabset;

		public :
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CDrvDlgImpl)
				MESSAGE_HANDLER     (WM_COMMAND   ,   OnBtnCmd )
				MESSAGE_HANDLER     (WM_DESTROY   ,   OnDestroy)
				MESSAGE_HANDLER     (WM_INITDIALOG,   OnInitDlg)
				MESSAGE_HANDLER     (WM_KEYDOWN   ,   OnKeyDown) // does not work in modeless dialogs;
				MESSAGE_HANDLER     (WM_SYSCOMMAND,   OnSysCmd )
				NOTIFY_CODE_HANDLER (TCN_SELCHANGE ,  OnNotify )
			END_MSG_MAP()

		public:
			 CDrvDlgImpl (void) ;
			~CDrvDlgImpl (void) ;

		private:
			LRESULT OnBtnCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnInitDlg(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnSysCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		private:
			LRESULT OnNotify (INT, LPNMHDR, BOOL&);

		private: // ITabSetCallback
#pragma warning(disable:4481)
			virtual HRESULT TabSet_OnDataChanged (const UINT pageId, const bool bChanged) override sealed;
			virtual HRESULT TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) override sealed;
		private: // IRenderer
			HRESULT DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea) const override sealed;
#pragma warning(default:4481)
		};
	private:
		CDrvDlgImpl   m_dlg;

	public:
		 CDrvDlg (void);
		~CDrvDlg (void);

	public:
		HRESULT    DoModal(void);

	private:
		CDrvDlg (const CDrvDlg&);
		CDrvDlg& operator= (const CDrvDlg&);
	};

}}}

#endif/*_EBOFAKEGPSDRVDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED*/