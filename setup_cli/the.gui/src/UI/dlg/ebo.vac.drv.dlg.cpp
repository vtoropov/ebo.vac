/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-May-2020 at 0:35:24a, UTC+7, Novosibirsk, Wednesday;
	This is FakeGPS driver configuration dialog interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver client project on 22-Jun-2020 at 4:46:35p, UTC+7, Monday;
*/
#include "StdAfx.h"
#include "ebo.vac.drv.dlg.h"
#include "ebo.vac.drv.dlg.res.h"

using namespace ebo::vac::gui;

#include "shared.gui.page.layout.h"

using namespace shared::gui::layout;

#include "shared.gen.app.res.h"
#include "shared.uix.gdi.provider.h"

using namespace shared::user32;
using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace vac { namespace gui { namespace _impl {

	class CDrvDlg_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_ban   = IDC_EBO_VAC_DRV_BAN  ,
			e_ctl_sep   = IDC_EBO_VAC_DRV_SEP  ,
			e_ctl_set   = IDOK                 ,
			e_ctl_apply = IDYES                , 
			e_ctl_close = IDCANCEL  
		};
	};
	typedef CDrvDlg_Ctl This_Ctl;

	class CDrvDlg_Res {
	public:
		enum _res : WORD {
			e_res_ban   = IDR_EBO_VAC_DRV_BAN  ,
		};
	};
	typedef CDrvDlg_Res This_Res;

	class CDrvDlg_Initer {
	private:
		CWindow&     m_dlg_ref;

	public:
		 CDrvDlg_Initer (CWindow&  _dlg_ref) : m_dlg_ref(_dlg_ref) {}
		~CDrvDlg_Initer (void) {}

	public:
		HRESULT      OnCreate(void) {
			HRESULT hr_ = S_OK;
			return  hr_;
		}
	};

	class CDrvDlg_Layout {
	private:
		enum _e {
			e_gap = 0x04,
			e_ban = 0x4b,  // 75x850px is taken from PNG banner file; this is a hight of the banner;
		};

	private:
		CWindow&     m_dlg_ref;
		RECT         m_dlg_rec;

	public:
		CDrvDlg_Layout(CWindow&  _dlg_ref) : m_dlg_ref(_dlg_ref) {

			if (m_dlg_ref.GetClientRect(&m_dlg_rec) == FALSE)
				::SetRectEmpty(&m_dlg_rec);
		}

	public:
		RECT     BanArea(VOID) {
			__no_args;
			RECT rc_ban  = {0};
			CWindow ban_ = (Lay_(m_dlg_ref) << This_Ctl::e_ctl_ban);
			if (NULL == ban_)
				return rc_ban;

			rc_ban = (Lay_(m_dlg_ref) = This_Ctl::e_ctl_ban);
			return rc_ban;
		}

		RECT     TabArea(VOID) {
			const
			RECT rc_ban = this->BanArea();
			RECT rc_sep = (Lay_(m_dlg_ref) = This_Ctl::e_ctl_sep);

			RECT rc_tab = {0};
			::SetRect(
				&rc_tab, m_dlg_rec.left + _e::e_gap, rc_ban.bottom - _e::e_gap/2, m_dlg_rec.right - _e::e_gap, rc_sep.top - _e::e_gap
			);

			return rc_tab;
		}
	};

	class CDrvDlg_Handler {
	private:
		CWindow&     m_dlg_ref;

	public:
		CDrvDlg_Handler(CWindow&  _dlg_ref) : m_dlg_ref(_dlg_ref) {}

	public:
		VOID   OnCommand(WORD wNotify, WORD ctrlId) {
			wNotify; ctrlId;
#if (0)
			switch (ctrlId) {
			case This_Ctl::e_ctl_apply: {} break;
			case This_Ctl::e_ctl_set  : {} break;
			case This_Ctl::e_ctl_close:
				::EndDialog(m_dlg_ref, ctrlId); break;
			}
#endif
		}
	};

}}}}

using namespace ebo::vac::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CDrvDlg::CDrvDlgImpl:: CDrvDlgImpl(void) : 
	IDD(IDD_EBO_VAC_DRV_DLG), m_banner(This_Res::e_res_ban), m_tabset(*this) {
	m_banner.Renderer(this);	
}
CDrvDlg::CDrvDlgImpl::~CDrvDlgImpl(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CDrvDlg::CDrvDlgImpl::OnBtnCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const WORD wNotify = HIWORD(wParam); wNotify;
	const WORD ctrlId  = LOWORD(wParam); ctrlId ;

	CDrvDlg_Handler hand_(*this);
	hand_.OnCommand(wNotify, ctrlId);

	switch (ctrlId) {
	case This_Ctl::e_ctl_apply: { m_tabset.UpdateData(); } break;
	case This_Ctl::e_ctl_set  : { m_tabset.UpdateData(); } 
	case This_Ctl::e_ctl_close:
	     TDialog::EndDialog(ctrlId); break;
	}

	return 0;
}

LRESULT   CDrvDlg::CDrvDlgImpl::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled = FALSE;
	m_tabset.Destroy();
	m_banner.Destroy();
	return 0;
}

LRESULT   CDrvDlg::CDrvDlgImpl::OnInitDlg(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	TDialog::CenterWindow();
	{
		CIconLoader   loader_(IDR_EBO_VAC_DRV_DLG_ICO);
		TDialog::SetIcon(loader_.DetachLarge(), TRUE );
		TDialog::SetIcon(loader_.DetachSmall(), FALSE);
	}
	HRESULT hr_ = m_banner.Create(TDialog::m_hWnd, This_Ctl::e_ctl_ban);
	if (SUCCEEDED(hr_)){}
	CDrvDlg_Initer   initer_(*this); initer_.OnCreate();

	CDrvDlg_Layout   layout_(*this);
	const RECT rc_tabs = layout_.TabArea();

	hr_ = m_tabset.Create(*this, rc_tabs);

	return 0;
}

LRESULT   CDrvDlg::CDrvDlgImpl::OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	switch (wParam)  {
	case VK_RETURN : {
			CWindow sav_but = (Lay_(*this)) << This_Ctl::e_ctl_apply;
			if (sav_but.IsWindowEnabled()) {
				CDrvDlg_Handler hand_(*this); hand_.OnCommand(0, This_Ctl::e_ctl_apply);
			}
		} break;
	}
	return 0;
}

LRESULT   CDrvDlg::CDrvDlgImpl::OnSysCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			TDialog::EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CDrvDlg::CDrvDlgImpl::OnNotify (INT idCtrl, LPNMHDR pnmh, BOOL& bHandled) {
	idCtrl; pnmh; bHandled = TRUE;
	m_tabset.UpdateLayout();
	return 0;
}

/////////////////////////////////////////////////////////////////////////////
#if (1)
HRESULT   CDrvDlg::CDrvDlgImpl::TabSet_OnDataChanged (const UINT pageId, const bool bChanged ) {
	pageId; bChanged;
	HRESULT hr_ = S_OK;

	CWindow app_but = (Lay_(*this)) << This_Ctl::e_ctl_apply;
	if (NULL != app_but) {
		app_but.EnableWindow(bChanged);
	}
	CWindow set_but = (Lay_(*this)) << This_Ctl::e_ctl_set;
	if (NULL != set_but) {
		set_but.EnableWindow(bChanged);
	}

	return  hr_;
}

HRESULT   CDrvDlg::CDrvDlgImpl::TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) {
	ctrlId; _rc_ctrl;
	HRESULT hr_ = S_OK;
	switch (ctrlId) {
	case IDYES   : case IDOK:
	case IDCANCEL:
	{
		CWindow ctl_btn = TDialog::GetDlgItem(ctrlId);
		if (NULL != ctl_btn) {
			ctl_btn.GetWindowRect(&_rc_ctrl);
		}
	} break;
	default:
	hr_ = DISP_E_PARAMNOTFOUND;
	}
	return  hr_;
}
#endif
/////////////////////////////////////////////////////////////////////////////

HRESULT   CDrvDlg::CDrvDlgImpl::DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& _rc_area) const {
	hChild;
	if (NULL == hSurface)
		return __DwordToHresult(ERROR_INVALID_HANDLE);
	if (::IsRectEmpty(&_rc_area))
		return OLE_E_INVALIDRECT;
	// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getsyscolor
	CZBuffer dc_(hSurface, _rc_area);
	dc_.FillSolidRect(
		&_rc_area, ::GetSysColor(COLOR_3DFACE)
	);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CDrvDlg:: CDrvDlg(void) {}
CDrvDlg::~CDrvDlg(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CDrvDlg::DoModal(void) {

	const INT_PTR result = m_dlg.DoModal();
	switch (result) {
	case IDNO : return S_OK    ;
	case IDOK : return S_OK    ;
	}
	return S_FALSE;
}