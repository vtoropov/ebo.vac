#ifndef _EBOVACDRVPAG3RD_H_35CF3A7E_FDB5_4748_87CC_BC036665B283_INCLUDED
#define _EBOVACDRVPAG3RD_H_35CF3A7E_FDB5_4748_87CC_BC036665B283_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Jun-2020 at 7:51:13p, UTC+7, Novosibirsk, Thursday;
	This is Ebo VAC driver setup tab page interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.gui.page.base.h"

#include "ebo.vac.ins.hand.h"

namespace ebo { namespace vac { namespace gui {

	using shared::sys_core::CError;
	using shared::gui::ITabPageEvents ;
	using shared::gui::ITabSetCallback;
	using shared::gui::CTabPageBase   ;

	using ebo::vac::handler::CSetupHandler;

	class CPageDrv_3rd : public CTabPageBase, public  ITabPageEvents {
	                    typedef CTabPageBase  TPage;
	private:
		CError          m_error;
		CSetupHandler   m_setup;

	public :
		 CPageDrv_3rd(ITabSetCallback&);
		~CPageDrv_3rd(void);

	private:
#pragma warning(disable:4481)
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageEvents
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public : // TPage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CAtlString  GetPageTitle(void) const override sealed;
		virtual HRESULT     UpdateData  (const DWORD _opt = 0) override sealed;
#pragma warning(default:4481)
	public:
		TErrorRef           Error (void) const { return m_error; }
	};

}}}

#endif/*_EBOVACDRVPAG3RD_H_35CF3A7E_FDB5_4748_87CC_BC036665B283_INCLUDED*/