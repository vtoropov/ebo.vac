#ifndef _EBOFAKEGPSDRVPAG1ST_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
#define _EBOFAKEGPSDRVPAG1ST_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-May-2020 at 3:27:56a, UTC+7, Novosibirsk, Wednesday;
	This is FakeGPS driver configuration dialog 1st page interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver client project on 22-Jun-2020 at 7:56:05a, UTC+7, Novosibirsk, Monday;
*/
#include "shared.gen.sys.err.h"
#include "shared.gui.page.base.h"

#include "ebo.vac.cnn.data.h"
#include "ebo.vac.smp.data.h"

namespace ebo { namespace vac { namespace gui {

	using shared::sys_core::CError;
	using shared::gui::ITabPageEvents ;
	using shared::gui::ITabSetCallback;
	using shared::gui::CTabPageBase   ;

	using ebo::vac::data::CConnection ;
	using ebo::vac::data::CSample;

	class CPageDrv_1st : public CTabPageBase, public  ITabPageEvents {
	                    typedef CTabPageBase  TPage;
	private:
		CError          m_error  ;
		CConnection     m_connect;
		CSample         m_sample ;

	public :
		 CPageDrv_1st(ITabSetCallback&);
		~CPageDrv_1st(void);

	private:
#pragma warning(disable:4481)
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageEvents
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public : // TPage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CAtlString  GetPageTitle(void) const override sealed;
		virtual HRESULT     UpdateData  (const DWORD _opt = 0) override sealed;
#pragma warning(default:4481)
	public:
		TErrorRef           Error (void) const { return m_error; }
	};

}}}

#endif/*_EBOFAKEGPSDRVPAG1ST_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED*/