#ifndef _EBOFAKEGPSDRVTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
#define _EBOFAKEGPSDRVTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-May-2020 at 0:52:22a, UTC+7, Novosibirsk, Wednesday;
	This is FakeGPS driver configuration dialog tab set interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver client project on 23-Jun-2020 at 2:55:19a, UTC+7, Tuesday;
*/
#include "shared.gui.page.base.h"

#include "ebo.vac.drv.pag.1st.h"
#include "ebo.vac.drv.pag.2nd.h"
#include "ebo.vac.drv.pag.3rd.h"

namespace ebo { namespace vac { namespace gui {

	using shared::gui::ITabSetCallback;

	class CDrvTabSet
	{
	private:
		WTL::CTabCtrl   m_cTabCtrl;

	private: //tab page(s)
		CPageDrv_1st    m_connect ;
		CPageDrv_2nd    m_exchange;
		CPageDrv_3rd    m_setup   ;

	public:
		 CDrvTabSet (ITabSetCallback&);
		~CDrvTabSet (void);

	public:
		HRESULT       Create  (const HWND hParent, const RECT& rcArea);
		HRESULT       Destroy (void)        ;

	public:
		HRESULT       UpdateData  (void);
		VOID          UpdateLayout(void);
	};
}}}

#endif/*_EBOFAKEGPSDRVTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED*/