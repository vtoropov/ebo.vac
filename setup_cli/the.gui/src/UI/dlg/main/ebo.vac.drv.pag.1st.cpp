/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-May-2020 at 3:36:50a, UTC+7, Novosibirsk, Wednesday;
	This is FakeGPS driver configuration dialog 1st page interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver client project on 22-Jun-2020 at 7:56:05a, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "ebo.vac.drv.pag.1st.h"
#include "ebo.vac.drv.dlg.res.h"

using namespace ebo::vac::gui;
using namespace ebo::vac::data;

#include "shared.gui.page.layout.h"

using namespace shared::gui::layout;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace vac { namespace gui { namespace _impl
{
	class CPageDrv_1st_Ctl {
	public:
		enum _ctl : WORD {
			e_dev_ava = IDC_EBO_VAC_DRV_DEV_AVA  ,    // connect section avatar  ;
			e_dev_cap = IDC_EBO_VAC_DRV_DEV_CAP  ,    // connect page caption    ;
			e_inp_lst = IDC_EBO_VAC_DRV_INP_LST  ,    // input  device dropdown  ;
			e_inp_lab = IDC_EBO_VAC_DRV_INP_LAB  ,    // input  device list label;
			e_out_lst = IDC_EBO_VAC_DRV_OUT_LST  ,    // output device dropdown  ;
			e_out_lab = IDC_EBO_VAC_DRV_OUT_LAB  ,    // output device list label;
			e_smp_ava = IDC_EBO_VAC_DRV_SMP_AVA  ,    // sample section avatar   ;
			e_smp_cap = IDC_EBO_VAC_DRV_SMP_CAP  ,    // samp;le page caption    ;
			e_frq_lst = IDC_EBO_VAC_DRV_SMP_FRQ  ,    // frequency rate dropdown ;
			e_frq_lab = IDC_EBO_VAC_DRV_SMP_LAB  ,    // frequency rate label    ;
			e_bit_lst = IDC_EBO_VAC_DRV_BIT_LST  ,    // sound bits dropdown list;
			e_bit_lab = IDC_EBO_VAC_DRV_BIT_LAB  ,    // sound bits text label   ;
			e_cnl_lst = IDC_EBO_VAC_DRV_CHL_LST  ,    // channel dropdown list   ;
			e_cnl_lab = IDC_EBO_VAC_DRV_CHL_LAB  ,    // channel text label      ;
			e_edt_buf = IDC_EBO_VAC_DRV_SMP_BUF  ,    // buffer size edit box    ;
		};
	};
	typedef CPageDrv_1st_Ctl This_Ctl;

	class CPageDrv_1st_Res {
	public:
		enum _res : WORD {
			e_dev_ava = IDR_EBO_VAC_DRV_DEV_AVA  ,
			e_smp_ava = IDR_EBO_VAC_DRV_SMP_AVA  ,
		};
	};
	typedef CPageDrv_1st_Res This_Res;

	class CPageDrv_1st_Layout
	{
	private:
		const CWindow&   m_page_ref;
		RECT             m_page_rec;

	public:
		CPageDrv_1st_Layout(const CWindow& page_ref) : m_page_ref(page_ref) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_dev_ava, This_Res::e_dev_ava) );
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_smp_ava, This_Res::e_smp_ava) );

			CWindow  dev_cap = Lay_(m_page_ref) << This_Ctl::e_dev_cap;
			if (0 != dev_cap) {
				dev_cap.SetFont(CTabPageBase::SectionCapFont());
			}

			const RECT rc_inp = (Lay_(m_page_ref) = This_Ctl::e_inp_lab);
			const RECT rc_out = (Lay_(m_page_ref) = This_Ctl::e_out_lab);
			const RECT rc_frq = (Lay_(m_page_ref) = This_Ctl::e_frq_lab);
			const RECT rc_bit = (Lay_(m_page_ref) = This_Ctl::e_bit_lab);
			const RECT rc_chl = (Lay_(m_page_ref) = This_Ctl::e_cnl_lab);

		//	CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_inf_img, This_Res::e_inf_goo) );

			CPage_Layout(m_page_ref).AlignComboHeightTo(This_Ctl::e_inp_lst, __H(rc_inp)) ;
			CPage_Layout(m_page_ref).AlignComboHeightTo(This_Ctl::e_out_lst, __H(rc_out)) ;
			CPage_Layout(m_page_ref).AlignComboHeightTo(This_Ctl::e_frq_lst, __H(rc_frq)) ;
			CPage_Layout(m_page_ref).AlignComboHeightTo(This_Ctl::e_bit_lst, __H(rc_bit)) ;
			CPage_Layout(m_page_ref).AlignComboHeightTo(This_Ctl::e_cnl_lst, __H(rc_chl)) ;

			CWindow  smp_cap = Lay_(m_page_ref) << This_Ctl::e_smp_cap;
			if (0 != smp_cap) {
				smp_cap.SetFont(CTabPageBase::SectionCapFont());
			}

			/*CComboBox cbo_frq = Lay_(m_page_ref) << This_Ctl::e_frq_lst;
			if (NULL!=cbo_frq) {
				cbo_frq.SetFont(CTabPageBase::FixedMonoFont());
			}*/
		}

		VOID OnSize (VOID) {}
	};

	class CPageDrv_1st_Init
	{
	private:
		CWindow&       m_page_ref;
		CError         m_error   ;
		
	public:
		CPageDrv_1st_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (const CConnection& _connect, const CSample& _sample) {
			m_error << __MODULE__ << S_OK;
			// https://docs.microsoft.com/en-us/windows/win32/api/mmeapi/nf-mmeapi-waveingetdevcaps
			INT n_selected = CSelection().Default();

			CComboBox cbo_inp = Lay_(m_page_ref) << This_Ctl::e_inp_lst;
			if (NULL!=cbo_inp) {
				CInputDevices devs_in;
				HRESULT hr_ = devs_in.Load();
				if (SUCCEEDED(hr_)) {
					
					const TDevices& devs_ = devs_in.Devices();
					for (size_t i_ = 0; i_ < devs_.size(); i_++) {
						cbo_inp.AddString(devs_[i_].Name());
					}
					if (devs_.empty() == true) {
						cbo_inp.AddString(_T("[No input device]"));
						cbo_inp.EnableWindow(0);
					}
					else if (CSelection::not_selected != devs_in.Has(_connect.Input()))
						n_selected = devs_in.Has(_connect.Input());
				}
				else
					m_error = devs_in.Error();
				if (m_error) {
					static LPCTSTR lp_sz_err = _T("[#err: code=0x%x; desc=%s]");
					CAtlString cs_error; cs_error.Format(
						lp_sz_err, m_error.Result(), m_error.Desc()
					);
					cbo_inp.AddString((LPCTSTR)cs_error);
					cbo_inp.EnableWindow(0);
				}
				cbo_inp.SetCurSel(n_selected);
			}

			n_selected = CSelection().Default();

			CComboBox cbo_out = Lay_(m_page_ref) << This_Ctl::e_out_lst;
			if (NULL!=cbo_out) {
				COutputDevices devs_out;
				HRESULT hr_ =  devs_out.Load();
				if (SUCCEEDED(hr_)) {

					const TDevices& devs_ = devs_out.Devices();
					for (size_t i_ = 0; i_ < devs_.size(); i_++) {
						cbo_out.AddString(devs_[i_].Name());
					}
					if (devs_.empty() == true) {
						cbo_out.AddString(_T("[No output device]"));
						cbo_out.EnableWindow(0);
					}
					else if (CSelection::not_selected != devs_out.Has(_connect.Output()))
						n_selected = devs_out.Has(_connect.Output());
				}
				else
					m_error = devs_out.Error();
				if (m_error) {
					static LPCTSTR lp_sz_err = _T("[#err: code=0x%x; desc=%s]");
					CAtlString cs_error; cs_error.Format(
						lp_sz_err, m_error.Result(), m_error.Desc()
					);
					cbo_out.AddString((LPCTSTR)cs_error);
					cbo_out.EnableWindow(0);
				}
				cbo_out.SetCurSel(n_selected);
			}

			CComboBox cbo_frq = Lay_(m_page_ref) << This_Ctl::e_frq_lst;
			if (NULL!=cbo_frq) {
				CFreqRates rates;
				CAtlString cs_rate;
				for (size_t i_ = 0; i_ < rates.Raw().size(); i_++) {
					cs_rate.Format(_T("%u"), rates.Raw()[i_]);
					cbo_frq.AddString((LPCTSTR)cs_rate);
				}
				cbo_frq.SetCurSel(cbo_frq.GetCount() ? rates.Has(_sample.Rate()) : CB_ERR);
			}

			CComboBox cbo_bit = Lay_(m_page_ref) << This_Ctl::e_bit_lst;
			if (NULL!=cbo_bit) {
				CBits bits;
				CAtlString cs_bits;
				for (size_t i_ = 0; i_ < bits.Raw().size(); i_++) {
					cs_bits.Format(_T("%02u"), bits.Raw()[i_]);
					cbo_bit.AddString((LPCTSTR)cs_bits);
				}
				cbo_bit.SetCurSel(cbo_bit.GetCount() ? bits.Has(_sample.Bits()) : CB_ERR);
			}

			CComboBox cbo_cnl = Lay_(m_page_ref) << This_Ctl::e_cnl_lst;
			if (NULL!=cbo_cnl) {
				CChannels channels;
				for (size_t i_ = 0; i_ < channels.Raw().size(); i_++) {
					cbo_cnl.AddString( channels.Raw()[i_].Caption() );
				}
				cbo_cnl.SetCurSel(cbo_cnl.GetCount() ? channels.Has(_sample.Channel()) : CB_ERR);
			}

			CEdit edt_buf = Lay_(m_page_ref) << This_Ctl::e_edt_buf;
			if (NULL!=edt_buf) {
				CAtlString cs_buf; cs_buf.Format(_T("%u"), _sample.Buffer());
				edt_buf.SetWindowTextW((LPCTSTR)cs_buf);
			}
			return m_error;
		}
	};

	class CPageDrv_1st_Handler {
	private:
		CWindow&         m_page_ref;

	public:
		CPageDrv_1st_Handler(CWindow& dlg_ref) : m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD u_ctl_id, const WORD u_ntf_cd) {
			u_ntf_cd; u_ctl_id;

			BOOL   bHandled = FALSE;
			return bHandled;
		}
		BOOL   OnNotify  (WPARAM _w_p, LPARAM _l_p) {
			_w_p; _l_p;
			BOOL    bHandled = FALSE ;
			return (bHandled = FALSE);
		}
	};
}}}}
using namespace ebo::vac::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageDrv_1st:: CPageDrv_1st(ITabSetCallback& _set_snk) :
       TPage(IDD_EBO_VAC_DRV_1ST_PAG, *this, _set_snk) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CPageDrv_1st::~CPageDrv_1st(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageDrv_1st::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	const LRESULT l_res = TPage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageDrv_1st::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TPage::OnPageInit(uMsg, wParam, lParam, bHandled);

	m_connect = CConnection_Default();
	m_sample  = CSample_Default();

	CPageDrv_1st_Layout layout_(*this);
	layout_.OnCreate();

	CPageDrv_1st_Init init_(*this);
	init_.OnCreate(m_connect, m_sample);

	TPage::m_set_snk.TabSet_OnDataChanged(TPage::Index(), FALSE);
	TPage::m_bInited = true;
	return l_res;
}

LRESULT    CPageDrv_1st::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TPage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageDrv_1st_Layout layout_(*this);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageDrv_1st::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TPage::m_bInited)
				break;
			const WORD u_ntf_cd = HIWORD(wParam);
			const WORD u_ctl_id = LOWORD(wParam);

			CPageDrv_1st_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(u_ctl_id, u_ntf_cd);
			if (bHandled == TRUE ) {
				TPage::m_set_snk.TabSet_OnDataChanged(TPage::Index(), FALSE);
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageDrv_1st::IsChanged   (void) const {
	return false;
}

CAtlString CPageDrv_1st::GetPageTitle(void) const {  static CAtlString cs_title(_T(" Connection ")); return cs_title; }

HRESULT    CPageDrv_1st::UpdateData  (const DWORD _opt) {
	m_error << __MODULE__ << S_OK; _opt;
	return m_error;
}