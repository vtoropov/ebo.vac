#ifndef _EBOVACDRVPAG2ND_H_04FA9F2B_A1C9_4661_99B5_640FB428385A_INCLUDED
#define _EBOVACDRVPAG2ND_H_04FA9F2B_A1C9_4661_99B5_640FB428385A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-Jun-2020 at 3:33:29a, UTC+7, Novosibirsk, Wednesday;
	This is Ebo VAC driver command execution tab page interface declaration file. 
*/
#include "shared.gen.sys.err.h"
#include "shared.gui.page.base.h"

#include "ebo.vac.cmd.defs.h"
#include "ebo.vac.cmd.hand.h"

namespace ebo { namespace vac { namespace gui {

	using shared::sys_core::CError;
	using shared::gui::ITabPageEvents ;
	using shared::gui::ITabSetCallback;
	using shared::gui::CTabPageBase   ;

	using ebo::vac::handler::CCmdHandler;

	class CPageDrv_2nd : public CTabPageBase, public  ITabPageEvents {
	                    typedef CTabPageBase  TPage;
	private:
		CError        m_error  ;
		CCmdHandler   m_handler;

	public :
		 CPageDrv_2nd(ITabSetCallback&);
		~CPageDrv_2nd(void);

	private:
#pragma warning(disable:4481)
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageEvents
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public : // TPage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CAtlString  GetPageTitle(void) const override sealed;
		virtual HRESULT     UpdateData  (const DWORD _opt = 0) override sealed;
#pragma warning(default:4481)
	public:
		TErrorRef           Error (void) const { return m_error; }
	};

}}}

#endif/*_EBOVACDRVPAG2ND_H_04FA9F2B_A1C9_4661_99B5_640FB428385A_INCLUDED*/