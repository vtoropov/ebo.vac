/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Jun-2020 at 7:56:05p, UTC+7, Novosibirsk, Thursday;
	This is Ebo VAC driver setup tab page interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.vac.drv.pag.3rd.h"
#include "ebo.vac.drv.dlg.res.h"

using namespace ebo::vac::gui;
using namespace ebo::vac::handler;

#include "shared.gui.page.layout.h"

using namespace shared::gui::layout;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace vac { namespace gui { namespace _impl
{
	class CPageDrv_3rd_Ctl {
	public:
		enum _ctl : WORD {
			e_sta_ava = IDC_EBO_VAC_DRV_STA_AVA  ,    // driver status section avatar   ;
			e_sta_cap = IDC_EBO_VAC_DRV_STA_CAP  ,    // driver status section caption  ;

			e_cmd_lab = IDC_EBO_VAC_DRV_MOD_LAB  ,    // driver status command label    ;
			e_cmd_lst = IDC_EBO_VAC_DRV_MOD_CMD  ,    // driver status command dropdown ;

			e_ins_edt = IDC_EBO_VAC_DRV_UTL_PAT  ,    // installer path edit control    ;
			e_ins_btn = IDC_EBO_VAC_DRV_UTL_BRW  ,    // installer path browse button   ;

			e_inf_edt = IDC_EBO_VAC_DRV_INF_PAT  ,    // INF file path edit control     ;
			e_inf_btn = IDC_EBO_VAC_DRV_INF_BRW  ,    // INF file path browse button    ;
			e_inf_img = IDC_EBO_VAC_DRV_INF_IMG  ,    // driver install info image ctrl ;
			e_inf_txt = IDC_EBO_VAC_DRV_INF_TXT  ,    // driver install info text label ;
		};
	};
	typedef CPageDrv_3rd_Ctl This_Ctl;

	class CPageDrv_3rd_Res {
	public:
		enum _res : WORD {
			e_sta_ava = IDR_EBO_VAC_DRV_STA_AVA  ,
			e_inf_img = IDR_EBO_VAC_DRV_INF_IMG  ,
			e_inf_txt = IDS_EBO_VAC_DRV_INF_TXT  ,
		};
	};
	typedef CPageDrv_3rd_Res This_Res;

	class CPageDrv_3rd_Layout {
	private:
		const CWindow&   m_page_ref;
		RECT             m_page_rec;

	public:
		CPageDrv_3rd_Layout(const CWindow& page_ref) : m_page_ref(page_ref) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}
	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_sta_ava, This_Res::e_sta_ava) );

			CWindow  sta_cap = Lay_(m_page_ref) << This_Ctl::e_sta_cap;
			if (0 != sta_cap) {
				sta_cap.SetFont(CTabPageBase::SectionCapFont());
			}

			const RECT rc_cmd = (Lay_(m_page_ref) = This_Ctl::e_cmd_lab);

			CPage_Layout(m_page_ref).AlignComboHeightTo(This_Ctl::e_cmd_lst, __H(rc_cmd)) ;
		}

		VOID   OnSize  (void) {
			CPage_Ban pan_(This_Ctl::e_inf_img, This_Ctl::e_inf_txt);
			pan_.Image().Resource() = This_Res::e_inf_img;
			pan_.Label().Resource() = This_Res::e_inf_txt;
			CPage_Layout(m_page_ref).AdjustBan(
				pan_
			);
		}
	};

	class CPageDrv_3rd_Init
	{
	private:
		CWindow&       m_page_ref;
		CError         m_error   ;

	public:
		CPageDrv_3rd_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}
	public:
		HRESULT   OnCreate(const CSetupHandler& _handler) {
			m_error << __MODULE__ << S_OK;

			CComboBox cmd_lst = Lay_(m_page_ref) << This_Ctl::e_cmd_lst;
			if (NULL!=cmd_lst) {

				CSetupCmd_Enum cmd_enum;
				for (size_t i_ = 0; i_ < cmd_enum.Raw().size(); i_++) {

					cmd_lst.AddString(cmd_enum.Raw()[i_].Caption());
				}
				cmd_lst.SetCurSel(cmd_lst.GetCount() ? cmd_enum.Has(_handler.Command()) : CB_ERR);
			}

			CEdit ins_edt = Lay_(m_page_ref) << This_Ctl::e_ins_edt;
			if (NULL!=ins_edt) {
				ins_edt.SetWindowText(_handler.Install().Path());
				ins_edt.EnableWindow (_handler.Install().Enabled());
			}
			CButton ins_btn = Lay_(m_page_ref) << This_Ctl::e_ins_btn;
			if (NULL!=ins_btn) {
				ins_btn.EnableWindow (_handler.Install().Enabled());
			}

			CEdit inf_edt = Lay_(m_page_ref) << This_Ctl::e_inf_edt;
			if (NULL!=inf_edt) {
				inf_edt.SetWindowText(_handler.INF().Path());
				inf_edt.EnableWindow (_handler.INF().Enabled());
			}
			CButton inf_btn = Lay_(m_page_ref) << This_Ctl::e_inf_btn;
			if (NULL!=inf_btn) {
				inf_btn.EnableWindow (_handler.INF().Enabled());
			}

			return OnUpdate();
		}

		HRESULT   OnUpdate(void) {
			m_error << __MODULE__ << S_OK;

			CWindow inf_txt = Lay_(m_page_ref) << This_Ctl::e_inf_txt;
			if (0!= inf_txt) {

				CAtlString cs_cap; cs_cap.LoadString(This_Res::e_inf_txt);
				inf_txt.SetWindowText(
					(LPCTSTR) cs_cap
				);
			}
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_inf_img, This_Res::e_inf_img) );
			return m_error;
		}
	};

	class CPageDrv_3rd_Handler {
	private:
		CWindow&         m_page_ref;

	public:
		CPageDrv_3rd_Handler(CWindow& dlg_ref) : m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD u_ctl_id, const WORD u_ntf_cd) {
			u_ntf_cd; u_ctl_id;

			BOOL   bHandled = FALSE;
			return bHandled;
		}
		BOOL   OnNotify  (WPARAM _w_p, LPARAM _l_p) {
			_w_p; _l_p;
			BOOL    bHandled = FALSE ;
			return (bHandled = FALSE);
		}
	};

}}}}
using namespace ebo::vac::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageDrv_3rd:: CPageDrv_3rd(ITabSetCallback& _set_snk) :
	TPage(IDD_EBO_VAC_DRV_3RD_PAG, *this, _set_snk) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CPageDrv_3rd::~CPageDrv_3rd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageDrv_3rd::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	const LRESULT l_res = TPage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageDrv_3rd::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TPage::OnPageInit(uMsg, wParam, lParam, bHandled);

	m_setup = CSetupHandler_Default();

	CPageDrv_3rd_Layout layout_(*this);
	layout_.OnCreate();

	CPageDrv_3rd_Init init_(*this);
	init_.OnCreate(m_setup);

	TPage::m_set_snk.TabSet_OnDataChanged(TPage::Index(), FALSE);
	TPage::m_bInited = true;
	return l_res;
}

LRESULT    CPageDrv_3rd::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TPage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageDrv_3rd_Layout layout_(*this);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageDrv_3rd::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
		if (!TPage::m_bInited)
			break;
		const WORD u_ntf_cd = HIWORD(wParam);
		const WORD u_ctl_id = LOWORD(wParam);

		CPageDrv_3rd_Handler handler_(*this);

		if (bHandled == FALSE)
			bHandled = handler_.OnCommand(u_ctl_id, u_ntf_cd);
		if (bHandled == TRUE ) {
			TPage::m_set_snk.TabSet_OnDataChanged(TPage::Index(), FALSE);
		}
	} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageDrv_3rd::IsChanged   (void) const {
	return false;
}

CAtlString CPageDrv_3rd::GetPageTitle(void) const {  static CAtlString cs_title(_T(" Driver Setup ")); return cs_title; }

HRESULT    CPageDrv_3rd::UpdateData  (const DWORD _opt) {
	m_error << __MODULE__ << S_OK; _opt;
	return m_error;
}