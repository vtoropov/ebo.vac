/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-May-2020 at 0:57:01a, UTC+7, Novosibirsk, Wednesday;
	This is FakeGPS driver configuration dialog tab set interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver client project on 23-Jun-2020 at 2:57:28a, UTC+7, Tuesday;
*/
#include "StdAfx.h"
#include "ebo.vac.drv.tab.set.h"
#include "ebo.vac.drv.dlg.res.h"

using namespace ebo::vac::gui;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace vac { namespace gui { namespace _impl
{
	class CDrvTabSet_Layout
	{
	private:
		RECT    m_area;
	public:
		CDrvTabSet_Layout(const RECT& rcArea) : m_area(rcArea) {}

	public:
		RECT    TabsArea (void) const {
			RECT rcTabs = m_area;
			::InflateRect(&rcTabs, -5, -3);
			return rcTabs;
		}
	};
}}}}

using namespace ebo::vac::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CDrvTabSet:: CDrvTabSet(ITabSetCallback& _snk) : m_connect(_snk), m_exchange(_snk), m_setup(_snk) {
}
CDrvTabSet::~CDrvTabSet(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDrvTabSet::Create(const HWND hParent, const RECT& rcArea) {
	hParent; rcArea;
	HRESULT hr_ = S_OK;

	if(FALSE == ::IsWindow(hParent) || ::IsRectEmpty(&rcArea))
		return (hr_ = E_INVALIDARG);

	CDrvTabSet_Layout layout(rcArea);
	RECT rcTabs  = layout.TabsArea();

	m_cTabCtrl.Create(
			hParent,
			rcTabs ,
			0,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			WS_EX_CONTROLPARENT,
			IDC_EBO_VAC_DRV_TAB
		);
	const SIZE szPadding = {
		5, 2
	};
	m_cTabCtrl.SetFont(::ATL::CWindow(hParent).GetFont());
	m_cTabCtrl.SetPadding(szPadding);
	m_cTabCtrl.SetItemSize(128, 24);

	INT nIndex = 0; nIndex;
	const DWORD dwFlags = SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW; // auto-placement is made by default implementation of create/initialize page handler;
	
	m_cTabCtrl.AddItem(m_connect.GetPageTitle());
	{
		m_connect.Create(m_cTabCtrl.m_hWnd);
		m_connect.SetWindowPos(
				HWND_TOP,
				0, 0, 0 , 0,
				dwFlags
			);
		if (m_connect.IsWindow()) m_connect.Index(nIndex++);
	}
	m_cTabCtrl.AddItem(m_exchange.GetPageTitle());
	{
		m_exchange.Create(m_cTabCtrl.m_hWnd);
		m_exchange.SetWindowPos(
			HWND_TOP,
			0, 0, 0 , 0,
			dwFlags
		);
		if (m_exchange.IsWindow()) m_exchange.Index(nIndex++);
	}
	m_cTabCtrl.AddItem(m_setup.GetPageTitle());
	{
		m_setup.Create(m_cTabCtrl.m_hWnd);
		m_setup.SetWindowPos(
			HWND_TOP,
			0, 0, 0 , 0,
			dwFlags
		);
		if (m_setup.IsWindow()) m_setup.Index(nIndex++);
	}
	m_cTabCtrl.SetCurSel(0);
	
	this->UpdateLayout();

	return S_OK;
}

HRESULT       CDrvTabSet::Destroy(void) {
	if (m_connect.IsWindow()){
		m_connect.DestroyWindow();  m_connect.m_hWnd = NULL;
	}
	if (m_exchange.IsWindow()){
		m_exchange.DestroyWindow();  m_exchange.m_hWnd = NULL;
	}
	if (m_setup.IsWindow()){
		m_setup.DestroyWindow();  m_setup.m_hWnd = NULL;
	}
	return S_OK;
}

HRESULT       CDrvTabSet::UpdateData  (void) {
	m_connect.UpdateData();
	m_exchange.UpdateData();
	m_setup.UpdateData(); return S_OK;
}

void          CDrvTabSet::UpdateLayout(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();

	if (m_connect.IsWindow()) m_connect.ShowWindow(nTabIndex == m_connect.Index() ? SW_SHOW : SW_HIDE);
	if (m_exchange.IsWindow()) m_exchange.ShowWindow(nTabIndex == m_exchange.Index() ? SW_SHOW : SW_HIDE);
	if (m_setup.IsWindow()) m_setup.ShowWindow(nTabIndex == m_setup.Index() ? SW_SHOW : SW_HIDE);
}