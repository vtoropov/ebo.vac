/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-Jun-2020 at 3:47:21a, UTC+7, Novosibirsk, Wednesday;
	This is Ebo VAC driver command execution tab page interface implementation file. 
*/
#include "StdAfx.h"
#include "ebo.vac.drv.pag.2nd.h"
#include "ebo.vac.drv.dlg.res.h"

using namespace ebo::vac::gui;
using namespace ebo::vac::handler;

#include "shared.gui.page.layout.h"

using namespace shared::gui::layout;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace vac { namespace gui { namespace _impl
{
	class CPageDrv_2nd_Ctl {
	public:
		enum _ctl : WORD {
			e_cmd_ava = IDC_EBO_VAC_DRV_CMD_AVA  ,    // command section avatar   ;
			e_cmd_cap = IDC_EBO_VAC_DRV_CMD_CAP  ,    // command section caption  ;
			e_cmd_lst = IDC_EBO_VAC_DRV_CMD_LST  ,    // command list dropdown    ;
			e_cmd_lab = IDC_EBO_VAC_DRV_CMD_LAB  ,    // command list label       ;
			e_prf_lst = IDC_EBO_VAC_DRV_PRF_LST  ,    // exe method list dropdown ;
			e_prf_lab = IDC_EBO_VAC_DRV_PRF_LAB  ,    // exe method list label    ;
			e_log_ava = IDC_EBO_VAC_DRV_LOG_AVA  ,    // log file section avatar  ;
			e_log_cap = IDC_EBO_VAC_DRV_LOG_CAP  ,    // log file section caption ;
			e_log_edt = IDC_EBO_VAC_DRV_LOG_PTH  ,    // log path edit control    ;
			e_log_lab = IDC_EBO_VAC_DRV_LOG_LAB  ,    // log path label           ;
			e_log_btn = IDC_EBO_VAC_DRV_LOG_BTN  ,    // log folder browse button ;
		};
	};
	typedef CPageDrv_2nd_Ctl This_Ctl;

	class CPageDrv_2nd_Res {
	public:
		enum _res : WORD {
			e_cmd_ava = IDR_EBO_VAC_DRV_CMD_AVA  ,
			e_log_ava = IDR_EBO_VAC_DRV_LOG_AVA  ,
		};
	};
	typedef CPageDrv_2nd_Res This_Res;

	class CPageDrv_2nd_Layout
	{
	private:
		const CWindow&   m_page_ref;
		RECT             m_page_rec;

	public:
		CPageDrv_2nd_Layout(const CWindow& page_ref) : m_page_ref(page_ref) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}
	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_cmd_ava, This_Res::e_cmd_ava) );
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_log_ava, This_Res::e_log_ava) );

			CWindow  cmd_cap = Lay_(m_page_ref) << This_Ctl::e_cmd_cap;
			if (0 != cmd_cap) {
				cmd_cap.SetFont(CTabPageBase::SectionCapFont());
			}

			CWindow  log_cap = Lay_(m_page_ref) << This_Ctl::e_log_cap;
			if (0 != log_cap) {
				log_cap.SetFont(CTabPageBase::SectionCapFont());
			}

			const RECT rc_cmd = (Lay_(m_page_ref) = This_Ctl::e_cmd_lab);
			const RECT rc_prf = (Lay_(m_page_ref) = This_Ctl::e_prf_lab);
			const RECT rc_log = (Lay_(m_page_ref) = This_Ctl::e_log_lab); rc_log;

			CPage_Layout(m_page_ref).AlignComboHeightTo(This_Ctl::e_cmd_lst, __H(rc_cmd)) ;
			CPage_Layout(m_page_ref).AlignComboHeightTo(This_Ctl::e_prf_lst, __H(rc_prf)) ;
		}
		VOID   OnSize  (void) {}
	};

	class CPageDrv_2nd_Init
	{
	private:
		CWindow&       m_page_ref;
		CError         m_error   ;

	public:
		CPageDrv_2nd_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (const CCmdHandler& _handler) {
			m_error << __MODULE__ << S_OK;

			CComboBox cbo_cmd = Lay_(m_page_ref) << This_Ctl::e_cmd_lst;
			if (NULL!=cbo_cmd) {

				CDrvCommand_Enum cmd_enum;
				for (size_t i_ = 0; i_ < cmd_enum.Raw().size(); i_++) {
					cbo_cmd.AddString(cmd_enum.Raw()[i_].Caption());
				}
				cbo_cmd.SetCurSel(cbo_cmd.GetCount() ? cmd_enum.Has(_handler.Command()) : CB_ERR);

			}

			CComboBox cbo_prf = Lay_(m_page_ref) << This_Ctl::e_prf_lst;
			if (NULL!=cbo_prf) {

				CExeMethod_Enum mtd_enum;
				for (size_t i_ = 0; i_ < mtd_enum.Raw().size(); i_++) {
					cbo_prf.AddString(mtd_enum.Raw()[i_].Caption());
				}
				cbo_prf.SetCurSel(cbo_prf.GetCount() ? mtd_enum.Has(_handler.Method()) : CB_ERR);

			}

			CEdit  log_edt = Lay_(m_page_ref) << This_Ctl::e_log_edt;
			if (0!=log_edt) {
				log_edt.SetWindowText(_T("[Log option is not available]"));
				log_edt.EnableWindow(FALSE);
			}

			CButton log_btn = Lay_(m_page_ref) << This_Ctl::e_log_btn;
			if (0!= log_btn) {
				log_btn.EnableWindow(FALSE);
			}

			return m_error;
		}
	};

	class CPageDrv_2nd_Handler {
	private:
		CWindow&         m_page_ref;

	public:
		CPageDrv_2nd_Handler(CWindow& dlg_ref) : m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD u_ctl_id, const WORD u_ntf_cd) {
			u_ntf_cd; u_ctl_id;

			BOOL   bHandled = FALSE;
			return bHandled;
		}
		BOOL   OnNotify  (WPARAM _w_p, LPARAM _l_p) {
			_w_p; _l_p;
			BOOL    bHandled = FALSE ;
			return (bHandled = FALSE);
		}
	};

}}}}
using namespace ebo::vac::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageDrv_2nd:: CPageDrv_2nd(ITabSetCallback& _set_snk) :
	TPage(IDD_EBO_VAC_DRV_2ND_PAG, *this, _set_snk) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CPageDrv_2nd::~CPageDrv_2nd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageDrv_2nd::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	const LRESULT l_res = TPage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageDrv_2nd::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TPage::OnPageInit(uMsg, wParam, lParam, bHandled);

	m_handler = CCmdHandler_Default();

	CPageDrv_2nd_Layout layout_(*this);
	layout_.OnCreate();

	CPageDrv_2nd_Init init_(*this);
	init_.OnCreate(m_handler);

	TPage::m_set_snk.TabSet_OnDataChanged(TPage::Index(), FALSE);
	TPage::m_bInited = true;
	return l_res;
}

LRESULT    CPageDrv_2nd::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TPage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageDrv_2nd_Layout layout_(*this);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageDrv_2nd::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
		if (!TPage::m_bInited)
			break;
		const WORD u_ntf_cd = HIWORD(wParam);
		const WORD u_ctl_id = LOWORD(wParam);

		CPageDrv_2nd_Handler handler_(*this);

		if (bHandled == FALSE)
			bHandled = handler_.OnCommand(u_ctl_id, u_ntf_cd);
		if (bHandled == TRUE ) {
			TPage::m_set_snk.TabSet_OnDataChanged(TPage::Index(), FALSE);
		}
	} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageDrv_2nd::IsChanged   (void) const {
	return false;
}

CAtlString CPageDrv_2nd::GetPageTitle(void) const {  static CAtlString cs_title(_T(" Commands ")); return cs_title; }

HRESULT    CPageDrv_2nd::UpdateData  (const DWORD _opt) {
	m_error << __MODULE__ << S_OK; _opt;
	return m_error;
}