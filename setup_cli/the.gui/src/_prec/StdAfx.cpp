/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2019 at 6:19:45p, UTC+7, Phuket, Rawai, Tuesday;
	This is communication data exchange receiver desktop console app precompiled header creation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 3:45:34a, UTC+7, Novosibirsk, Tuesday;
	Adopted to FakeGPS driver project on 16-Apr-2020 at 2:57:55p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Xor Java Eclipse build tool project on 21-May-2020 at 5:31:55p, UTC+7, Thursday;
	Adopted to Ebo VAC driver client project on 22-Jun-2020 at 6:58:36a, UTC+7, Monday;
*/

#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)

#include "shared.gen.syn.obj.h"

::WTL::CMessageLoop* get_MessageLoop(){ return _Module.GetMessageLoop(); }
HINSTANCE get_HINSTANCE() { return _Module.GetResourceInstance(); }