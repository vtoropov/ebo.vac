#ifndef _STDAFX_H_A2AD1503_5555_ABCD_9B38_341E6E697B7E_INCLUDED
#define _STDAFX_H_A2AD1503_5555_ABCD_9B38_341E6E697B7E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2019 at 6:17:57p, UTC+7, Phuket, Rawai, Tuesday;
	This is communication data exchange receiver desktop console app precompiled header declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 3:24:20a, UTC+7, Novosibirsk, Tuesday;
	Adopted to FakeGPS driver project on 16-Apr-2020 at 2:56:24a, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Xor Java Eclipse build tool project on 21-May-2020 at 5:31:55p, UTC+7, Thursday;
	Adopted to Ebo VAC driver client project on 22-Jun-2020 at 6:58:36a, UTC+7, Monday;
*/
#include "ebo.vac.cli.ver.h"

#ifndef STRICT
#define STRICT
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe
#pragma warning(disable: 4458)  // declaration of '{func_name}' hides class member (GDI+)
//
// ATL header(s)
//
#include <atlbase.h>
#include <atlstr.h>             // important order, this file must be included before any includes of the WTL headers
#include <atlapp.h>

extern CAppModule _Module;

#include <atlwin.h>
#include <atlctrls.h>
#include <atldlgs.h>
//
// COM & Win API
//
#include <comdef.h>
#include <comutil.h>
#include <mmeapi.h>
#include <DSound.h>

#include <ks.h>
#include <ksmedia.h>

using namespace ATL;

#ifdef _DEBUG
	#define _ATL_DEBUG_INTERFACES
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif
//
// STL header(s)
//
#include <vector>
#include <map>
#include <time.h>
#include <typeinfo>

#if (0)
#if defined WIN64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined WIN32
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

#if (0)
#else
#define __nothing
#define __empty_ln
#define __no_args
#endif

#pragma comment(lib, "Winmm.lib")
#pragma comment(lib, "Dsound.lib")

#pragma comment(lib, "__shared.lite_v15.lib")

#pragma comment(lib, "_ntfs_v15.lib" )
#pragma comment(lib, "_registry_v15.lib" )

#pragma comment(lib, "_uix.ctrl_v15.lib" )
#pragma comment(lib, "_uix.draw_v15.lib" )
#pragma comment(lib, "_uix.frms_v15.lib" )

#pragma comment(lib, "_user.32_v15.lib"  )



#endif/*_STDAFX_H_A2AD1503_5555_ABCD_9B38_341 E6E697B7E_INCLUDED*/