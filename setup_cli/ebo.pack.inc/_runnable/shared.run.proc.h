#ifndef _SHAREDRUNPROC_H_94AB6D14_3CB4_4970_A262_09DA20F7061C_INCLUDED
#define _SHAREDRUNPROC_H_94AB6D14_3CB4_4970_A262_09DA20F7061C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 09-May-2007 at 11:19:30am (GMT+3), Rostov-on-Don, Wednesday;
	For Prospect OutCRuns project (COM.Async);
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 17-May-2020 at 9:19:04a, UTC+7, Novosibirsk, Sunday;
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.han.h"
namespace shared { namespace runnable {
	using shared::sys_core::CError;
	using shared::common::CAutoHandle;
	using shared::common::CStdHandle ;

	interface IRunProcEvents {
		virtual VOID    IRunProc_OnError (const CError) PURE;
		virtual VOID    IRunProc_OnFinish(CAtlString _result) PURE;
	};
	// https://docs.microsoft.com/en-us/windows/win32/ProcThread/creating-a-child-process-with-redirected-input-and-output
	class COutProc {
	public:
		enum _handles {
			e_out_read  = 0,   // a handle to read from std output pipe;
			e_out_write = 1,   // a handle to write to  std  input pipe;
			e_err_read  = 2,   // a handle to read from error output pipe;
			e_err_write = 3,   // a handle to write to  error  input pipe;
		};
	private:
		IRunProcEvents&
		             m_evt_sink;
		CAutoHandle  m_handles[_handles::e_out_write + 1];
		CError       m_error   ;

	public:
		 COutProc (IRunProcEvents&);
		~COutProc (void);

	public:
		TErrorRef   Error (void) const;
		HRESULT     Run   (LPCTSTR _lp_sz_cmd_ln, LPCTSTR _lp_sz_work_dir);
	};

}}

#endif/*_SHAREDRUNPROC_H_94AB6D14_3CB4_4970_A262_09DA20F7061C_INCLUDED*/