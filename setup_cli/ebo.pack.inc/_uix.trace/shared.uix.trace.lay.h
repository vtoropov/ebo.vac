#ifndef	_SHAREDUIXTRACELAY_H_EB5039C7_978D_4832_9FC2_239BE2A895DF_INCLUDED
#define _SHAREDUIXTRACELAY_H_EB5039C7_978D_4832_9FC2_239BE2A895DF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Apr-2020 at 11:11:43p, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Pack UIX trace view layout interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"

namespace ex_ui { namespace trace {

	using shared::sys_core::CError ;
	using ex_ui::controls::CMargins;
	using ex_ui::controls::TSide   ;

	class CImg_Layout {
	protected:
		CMargins   m_margins ; // image rectangle magins; is used for creating a gap between an image and item text;
		SIZE       m_size    ; // image size; by default it's calculated by means of image list control, but can be set manually;

	public:
		 CImg_Layout (void);
		 CImg_Layout (const CImg_Layout&);
		~CImg_Layout (void);

	public:
		const
		CMargins&  Margins(void) const ;
		CMargins&  Margins(void)       ;
		const
		SIZE    &  Size   (void) const ;       // sets a size of an image;
		SIZE    &  Size   (void)       ;

	public:
		CImg_Layout& operator << (const SIZE&);  // sets a size of an image;
		CImg_Layout& operator  = (const CImg_Layout&);
	};

	typedef CImg_Layout TImgLay;

	class CTraceEx;
	class CTrace_Layout {
	protected:
		CTraceEx&    m_view    ;
		CMargins     m_margins ;
		TImgLay      m_img_lay ;
		mutable
		CError       m_error   ;
		LONG         m_ln_gap  ; // a vertical gap between entries;
		SIZE         m_scroll  ; // size of the scroll button; by default is 32x32;

	public:
		 CTrace_Layout(CTraceEx&);
		~CTrace_Layout(void);

	public:
		TErrorRef    Error  (void) const ;
		const
		TImgLay&     Images (void) const ;          // if defined, it is used for all items regardless having an image;
		TImgLay&     Images (void)       ;
		const
		LONG&        LineGap(void) const ;
		LONG&        LineGap(void)       ;
		const
		CMargins&    Margins(void) const ;          // trace view margins between frame border(s) and a content area;
		CMargins&    Margins(void)       ;
		const
		SIZE&        Scrolls(void) const ;          // gets a size of scroll button (ro);
		SIZE&        Scrolls(void)       ;          // gets a size of scroll button (rw);
		HRESULT      Update (const RECT&);          // sets a trace window/content to specified area;
		
	public:
		CTrace_Layout& operator << (const RECT& _rc_area);
	};

}}

typedef ex_ui::trace::CTrace_Layout TLayout;

#endif/*_SHAREDUIXTRACELAY_H_EB5039C7_978D_4832_9FC2_239BE2A895DF_INCLUDED*/