#ifndef _UIXFRAMEIMAGEHEADER_H_ADF4AA40_1A25_43f7_9ABF_B02BC5319F25_INCLUDED
#define _UIXFRAMEIMAGEHEADER_H_ADF4AA40_1A25_43f7_9ABF_B02BC5319F25_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 3:17:13am, GMT+4, Saint-Petersburg, Monday;
	This is Ebo Pack shared UIX Frame Library Image Header class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 8-Aug-2018 at 11:50:26a, UTC+7, Novosibirsk, Tulenina, Wednesday; 
*/
#include "shared.uix.gdi.provider.h"

namespace ex_ui { namespace frames {
	
	using ex_ui::draw::defs::IRenderer;
	
	class CImageBanner {
		
	private:
		HANDLE m_ban_wnd;
	public: 
		 CImageBanner(void);
		 CImageBanner(const ATL::_U_STRINGorID RID);
		~CImageBanner(void);
	public:
		HRESULT         Create  (const HWND hParent, const UINT ctrlId = 0);
		HRESULT         Destroy (void);
		HRESULT         Image   (const UINT u_res_id);
		bool            IsValid (void) const;
		HRESULT         Renderer(IRenderer* _p_parent_rnd);
		const SIZE      ReqSize (void) const;
		bool            Visible (void) const;
		HRESULT         Visible (const bool);
		const CWindow   Window  (void) const; // gets banner window reference (read);
		      CWindow   Window  (void)      ; // gets banner window reference (read/write);
	private:
		CImageBanner(const CImageBanner&);
		CImageBanner& operator= (const CImageBanner&);
	};
}}

#endif/*_UIXFRAMEIMAGEHEADER_H_ADF4AA40_1A25_43f7_9ABF_B02BC5319F25_INCLUDED*/