#ifndef _SHAREDGENERICCONOBJECT_H_4D74D09A_4FA1_4d03_9401_4D064A3AE294_INCLUDED
#define _SHAREDGENERICCONOBJECT_H_4D74D09A_4FA1_4d03_9401_4D064A3AE294_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Dec-2013 at 12:29:53pm, GMT+4, Saint-Petersburg, Sunday;
	This is Pulsepay WWS Server Light Console wrapper interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to FG project (thefileguardian.com) on 6-Jan-2016 at 9:26:48am, GMT+7, Phuket, Rawai, Wednesday;
	Adopted to FakeGPS driver project on 13-Dec-2019 at 9:47:05a, UTC+7, Novosibirsk, Friday;
*/
#include <atlwin.h>
#include "shared.gen.sys.err.h"
namespace shared { namespace user32 {
	using shared::sys_core::CError;
	class CEventType {
	public:
		enum _type {
			eInfo    = 0x0,
			eWarning = 0x1,
			eError   = 0x2
		};
	};

	typedef CEventType::_type TEventType;

	interface IConsoleNotify
	{
		virtual VOID   ConsoleNotify_OnEvent(const TEventType , const UINT  nResId) PURE;
		virtual VOID   ConsoleNotify_OnEvent(const TEventType , LPCTSTR pszDetails) PURE;
		virtual VOID   ConsoleNotify_OnError(const UINT nResId) PURE;
		virtual VOID   ConsoleNotify_OnInfo (const UINT nResId) PURE;
	};

	class CConsoleNotifyDefImpl : public IConsoleNotify
	{
	private: // IConsoleNotify
#pragma warning(disable: 4481)
		virtual VOID   ConsoleNotify_OnEvent(const TEventType , const UINT  nResId) override sealed;
		virtual VOID   ConsoleNotify_OnEvent(const TEventType , LPCTSTR pszDetails) override sealed;
		virtual VOID   ConsoleNotify_OnError(const UINT nResId) override sealed;
		virtual VOID   ConsoleNotify_OnInfo (const UINT nResId) override sealed;
#pragma warning(default: 4481)
	};

	class CConsoleFont {
	protected:
		mutable
		CError    m_error;
	public:
		 CConsoleFont(void);
		~CConsoleFont(void);
	public:
		TErrorRef Error (void) const;
		UINT      Size  (void) const;   // gets font size that is set in console output;
	};

	class CConsolePage {
	private:
		UINT      m_in;
		UINT      m_out;
	public:
		 CConsolePage(const bool bAutoHandle = true);
		~CConsolePage(void);

	public:
		HRESULT   Dos (void);
		HRESULT   Set (const UINT _in, const UINT _out);
	};

	class CConsoleWindow
	{
	private:
		CWindow   m_wnd;
	public:
		CConsoleWindow(void);
	public:
		HRESULT   ClearContent(void);
		HWND      GetHwndSafe (void)const;
		bool      IsValid(void)const;
		HRESULT   SetIcon(const UINT nResId);
	public:
		static
		HRESULT   Create(LPCTSTR lpszTitle=NULL);
	};

	static LPCTSTR lp_sz_sep = _T("\n\t\t");

	class CConsole {
	public:
		 CConsole(void);
		~CConsole(void);

	public:
		HRESULT   OnClose  (void);
		HRESULT   OnCreate (LPCTSTR    _lp_sz_cap);
		HRESULT   OnWait   (LPCTSTR    _lp_sz_msg);
		HRESULT   SetIcon  (const WORD _w_res_id );
		HRESULT   Write    (const TEventType, LPCTSTR _lp_sz_msg, LPCTSTR _lp_sz_sep = lp_sz_sep);
		HRESULT   WriteErr (TErrorRef  _err);
		HRESULT   WriteErr (LPCTSTR _lp_sz_msg, LPCTSTR  _lp_sz_sep = lp_sz_sep);
		HRESULT   WriteInfo(LPCTSTR _lp_sz_msg, LPCTSTR  _lp_sz_sep = lp_sz_sep);
		HRESULT   WriteWarn(LPCTSTR _lp_sz_msg, LPCTSTR  _lp_sz_sep = lp_sz_sep);

	public:
		CConsole& operator << (const WORD);        // info message;
		CConsole& operator << (LPCTSTR   );        // info message;
		CConsole& operator << (TErrorRef );        // error message;
		CConsole& operator << (const CAtlString&); // info message;

	};
}}

#endif/*_SHAREDGENERICCONOBJECT_H_4D74D09A_4FA1_4d03_9401_4D064A3AE294_INCLUDED*/