#ifndef _SHAREDGENERICAPPRESOURCE_H_F3703DAD_884A_467b_BB97_A47EB3A50C8A_INCLUDED
#define _SHAREDGENERICAPPRESOURCE_H_F3703DAD_884A_467b_BB97_A47EB3A50C8A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Jul-2012 at 8:19:45pm, GMT+3, Rostov-on-Don, Friday;
	This is Pulsepay Shared Application Icon Holder class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 28-May-2018 at 5:24:53p, UTC+7, Phuket, Rawai, Monday;
*/
#include <Shellapi.h>
#include "shared.gen.sys.err.h"
#include "shared.gen.raw.buf.h"

namespace shared { namespace user32
{
	class CIconLoader
	{
	private:
		HICON   m_small;
		HICON   m_large;
	public:
		 CIconLoader (void);
		 CIconLoader (LPCTSTR pszIconFile, const INT nIconIndex);
		 CIconLoader (const UINT nIconId); // it is assumed that resource identifier points to multiframe icon resource
		 CIconLoader (const UINT nSmallIconId, const UINT nLargeIconId);
		~CIconLoader (void);
	public:
		VOID    Destroy    (void);
		HICON   DetachLarge(void);
		HICON   DetachSmall(void);
		bool    Has    (const bool _b_large) const;
		HRESULT Load   (const UINT nIconId );
	private:
		CIconLoader(const CIconLoader&);
		CIconLoader& operator= (const CIconLoader&);
	};

	class CApplicationCursor
	{
	private:
		bool    m_resource_owner;
	public:
		 CApplicationCursor(LPCTSTR lpstrCursor = IDC_WAIT);
		~CApplicationCursor(void);
	};

	using shared::sys_core::CError;
	using shared::common::CRawData;

	class CGenericResourceLoader
	{
	private:
		CError    m_error;
	public:
		CGenericResourceLoader(void);
	public:
		TErrorRef Error(void)const;
		HRESULT   LoadRcDataAsUtf8(const UINT uResId, CAtlString& _buffer); // loads data as UTF8 character buffer and copies data to string object
		HRESULT   LoadRcDataAsUtf8(const UINT uResId, CRawData& _buffer);   // loads data as UTF8 character buffer
	};
}}

#endif/*_SHAREDGENERICAPPRESOURCE_H_F3703DAD_884A_467b_BB97_A47EB3A50C8A_INCLUDED*/