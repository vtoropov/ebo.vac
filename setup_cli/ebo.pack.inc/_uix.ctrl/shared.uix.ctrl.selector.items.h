#ifndef _SHAREDUIXCTRLSELECTORITEMS_H_BC95061E_E924_4B33_9111_129F400D28A3_INCLUDED
#define _SHAREDUIXCTRLSELECTORITEMS_H_BC95061E_E924_4B33_9111_129F400D28A3_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Apr-2020 at 9:20:08p, UTC+7, Novosibirs, Friday;
	This is shared UIX library task/view selector control group/item interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"

namespace ex_ui { namespace controls {

	using shared::sys_core::CError;

	class CSel_Type {
	public:
		enum _type  {
			e_cmd = 0x0,    // command item type, default;
			e_url = 0x1,    // web link;
			e_cap = 0x2,    // caption/group ; can act as collapsing/expanding element; not produce event outside;
		};
	};

	typedef CSel_Type::_type TSelItmType;

	class CSel_Item {
	protected:
		RECT         m_place;
		UINT         m_id   ;
		CAtlString   m_text ;
		INT          m_img  ; // image index in image list control, by default is -1, i.e. no image;
		TSelItmType  m_type ;

	public:
		 CSel_Item (void);
		~CSel_Item (void);

	public:
		const UINT&  Id  (void) const;
		      UINT&  Id  (void)      ;  // item collection does not control changing identifier this way;
		const  INT&  Img (void) const;
		       INT&  Img (void)      ;
		const RECT&  Rect(void) const;
		      RECT&  Rect(void)      ;
		const
		CAtlString&  Text(void) const;
		CAtlString&  Text(void)      ;
		const
		TSelItmType& Type(void) const;
		TSelItmType& Type(void)      ;
	};

	typedef ::std::vector <CSel_Item>  TSelItems; // selector control supposes unsorted item sequence;

	class CSelector   ;
	class CSel_Item_Enum {
	protected:
		CSelector&      m_selector;
		TSelItems       m_items   ;
		CError          m_error   ;
		INT         m_ndx_selected;    // if less than zero that means no item is selected;

	public:
		 CSel_Item_Enum (CSelector&);
		~CSel_Item_Enum (void);

	public:
		HRESULT         Append  (const CSel_Item&);
		TErrorRef       Error   (void) const;
		const
		TSelItems&      Items   (void) const;
		TSelItems&      Items   (void)      ;
		const INT       Selected(void) const;
		HRESULT         Selected(const INT _ndx)  ;

	public:
		operator const TSelItems& (void) const;
	public:
		CSel_Item_Enum& operator += (const CSel_Item&);
		CSel_Item_Enum& operator << (const UINT u_cmd);  // selects item by command identifier provided;
	};

}}

#endif/*_SHAREDUIXCTRLSELECTORITEMS_H_BC95061E_E924_4B33_9111_129F400D28A3_INCLUDED*/