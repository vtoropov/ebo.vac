#ifndef _SHAREDUIXCTRLSELECTOR_H_97B01DFB_8AFE_4B7E_9CC9_B8FC391DEE77_INCLUDED
#define _SHAREDUIXCTRLSELECTOR_H_97B01DFB_8AFE_4B7E_9CC9_B8FC391DEE77_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Nov-2019 at 9:16:09p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is shared UIX library task/view selector control interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.selector.ext.h"
#include "shared.uix.ctrl.selector.items.h"

namespace ex_ui { namespace controls {

	using shared::sys_core::CError;

	class CImg_Layout {
	protected:
		CMargins        m_margins ; // image rectangle magins; is used for creating a gap between an image and item text;
		SIZE            m_size    ; // image size; by default it's calculated by means of image list control, but can be set manually;

	public:
		 CImg_Layout (void);
		~CImg_Layout (void);

	public:
		const
		CMargins&       Margins(void) const ;
		CMargins&       Margins(void)       ;
		const
		SIZE    &       Size   (void) const ;
		SIZE    &       Size   (void)       ;

	public:
		CImg_Layout&    operator << (const SIZE&);  // sets a size of an image;
	};

	class CSelector   ;
	class CSel_Layout {
	protected:
		CSelector&      m_selector;
		THorzAlign      m_hz_align; // horizontal alignment in parent window;
		CMargins        m_margins ;
		UINT            m_width   ; // control width; it has a default value on startup, can be re-written;
		mutable CError  m_error   ;
		CImg_Layout     m_img_lay ;

	public:
		 CSel_Layout(CSelector&);
		~CSel_Layout(void);

	public:
		TErrorRef       Error  (void) const ;
		const
		THorzAlign&     HAlign (void) const ;
		THorzAlign&     HAlign (void)       ;
		const
		CImg_Layout&    Images (void) const ;          // if defined, it is used for all items regardless having an image;
		CImg_Layout&    Images (void)       ;
		const
		CMargins&       Margins(void) const ;          // selector control margins between frame border(s) and control's content;
		CMargins&       Margins(void)       ;
		UINT            Width  (void) const ;
		HRESULT         Width  (const UINT );          // TODO: input value is checked on 0-value only; minimal value must be defined;
		HRESULT         Update (const RECT& _rc_area); // updates selector window position into an area provided;

	public:
		CSel_Layout&    operator<<(const RECT& _rc_area);        // updates selector window position in accordance with area  ;
		const RECT      operator =(const RECT& _rc_area) const;  // returns calculated rectangle of selector for area provided;
	};

	class CSelector : public ISelector_Events {
	protected:
		ISelector_Events&
		               m_evt_snk;   // control owner event sink reference;
		HANDLE         m_wnd_ptr;
		CError         m_error  ;
		CSel_Item_Enum m_itm_enm;
		CSel_Format    m_format ;
		CSel_Layout    m_layout ;
		UINT           m_ctrl_id;

	public:
		 CSelector(ISelector_Events&);
		~CSelector(void);

	public:
		HRESULT        Create  (const HWND hParent, const RECT& _rc_area, LPCTSTR _lp_sz_cap, const UINT _ctrl_id);
		HRESULT        Destroy (void)      ;
		TErrorRef      Error   (void) const;
		const
		CSel_Format&   Format  (void) const;
		CSel_Format&   Format  (void)      ;
		const UINT     Id      (void) const;
		const bool     Is      (void) const; // checks a validity state;
		const
		CSel_Item_Enum& Items  (void) const;
		CSel_Item_Enum& Items  (void)      ;
		const
		CSel_Layout&   Layout  (void) const;
		CSel_Layout&   Layout  (void)      ;
		HRESULT        Refresh (void)      ;
		const bool     Visible (void) const; // returns control visibility state;
		HRESULT        Visible (const bool); // sets control visibility;
		CWindow        Window  (void) const;

	private: // ISelector_Events
		virtual HRESULT   ISelector_OnBkgChanged (void) override;
		virtual HRESULT   ISelector_OnFontChanged(void) override;
		virtual HRESULT   ISelector_OnItemClick  (const UINT _u_itm_id) override;
		virtual HRESULT   ISelector_OnItemImages (const UINT _u_res_id) override;

	private:
		CSelector(const CSelector&);
		CSelector& operator= (const CSelector&);
	};
}}

#endif/*_SHAREDUIXCTRLSELECTOR_H_97B01DFB_8AFE_4B7E_9CC9_B8FC391DEE77_INCLUDED*/