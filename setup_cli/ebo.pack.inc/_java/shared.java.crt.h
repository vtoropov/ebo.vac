#ifndef _SHAREDJAVACRT_H_A148DDB7_EDA4_4361_AF0B_C4A01B0B56A9_INCLUDED
#define _SHAREDJAVACRT_H_A148DDB7_EDA4_4361_AF0B_C4A01B0B56A9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 26-May-2020 at 1:10:20p, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Pack shared java integration library interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.ver.info.h"
namespace shared { namespace java {
	using shared::sys_core::CError;
	using shared::user32::CVersion;

	class CJavaRT {
	protected:
		CError         m_error  ;
		CAtlString     m_path   ;
		CVersion       m_version;
		CAtlString     m_runtime;

	public:
		 CJavaRT (void);
		 CJavaRT (const CJavaRT&);
		~CJavaRT (void);

	public:
		TErrorRef      Error  (void) const;
		HRESULT        Init   (void)      ;
		const bool     Is     (void) const;
		LPCTSTR        Path   (void) const;
		LPCTSTR        Runtime(void) const;
		TVersionRef    Version(void) const;

	public:
		CJavaRT& operator = (const CJavaRT&);
	};

	class CJavaSetup {
	protected:
		mutable
		CError      m_error;
		CAtlString  m_path ;

	public:
		 CJavaSetup (void);
		~CJavaSetup (void);

	public:
		TErrorRef   Error    (void) const;
		HRESULT     Init     (void)      ;
		const bool  Installed(void) const;
		LPCTSTR     Path     (void) const;
	};

}}

#endif/*_SHAREDJAVACRT_H_A148DDB7_EDA4_4361_AF0B_C4A01B0B56A9_INCLUDED*/