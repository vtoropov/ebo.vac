#ifndef _SHAREDUIXGENCLRS_H_A1B4CB5B_C15C_4E5D_929D_D284CB99FB48_INCLUDED
#define _SHAREDUIXGENCLRS_H_A1B4CB5B_C15C_4E5D_929D_D284CB99FB48_INCLUDED
/*
	I'm Vladimir Toropov (ebontrop@gmail.com) as a code writer transfer all ownership rights on this file
	with all exclusive intellectual rights for this product "FakeGPS" to Oleksii Gupa (aleksey.gupa@gmail.com).
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw {

	typedef Gdiplus::Color TColor;

	class CColour
	{
	protected:
		TColor m_clr;

	public:
		 CColour(void);
		 CColour(const CColour&);
		 CColour(const COLORREF clr, const BYTE alpha = eAlphaValue::eOpaque);
		 CColour(const TColor& );
		~CColour(void);

	public:
		BYTE     Alpha  (void) const;
		VOID     Alpha  (const eAlphaValue::_e = eAlphaValue::eSemitransparent);
		VOID     Create (const COLORREF clr, const BYTE _alpha = eAlphaValue::eOpaque);
		const
		TColor&  Data   (void) const;
		VOID     Empty  (void)      ;
		bool     Is     (void) const; // returns true if m_clr is not CLR_NONE;

	public:
		operator const TColor& (void) const;
		operator const TColor* (void) const;
		operator const COLORREF(void) const;

	public:
		CColour& operator = (const CColour &);
		CColour& operator = (const COLORREF&);
		CColour& operator = (const TColor  &);
	};

	class CColour_Ex : public CColour {
	                  typedef CColour TBase;
	public:
		 CColour_Ex(void);
		 CColour_Ex(const CColour_Ex&);
		 CColour_Ex(const COLORREF clr, const BYTE alpha = eAlphaValue::eOpaque);
		~CColour_Ex(void);

	public:
		HRESULT  Darken (const __int8 btValue);
		HRESULT  Lighten(const __int8 btValue);

	public:
		CColour_Ex& operator+= (const __int8 btValue);  // makes color light;
		CColour_Ex& operator-= (const __int8 btValue);  // makes color dark ;
		CColour_Ex& operator = (const CColour&);        // regular color object is assigned;
		CColour_Ex& operator = (const CColour_Ex&);
	};

}}

#endif/*_SHAREDUIXGENCLRS_H_A1B4CB5B_C15C_4E5D_929D_D284CB99FB48_INCLUDED*/