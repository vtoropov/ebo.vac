#ifndef _UIXIMGWRAP_H_79F33EAF_DDFB_43f1_9FC1_3E41CD7C9916_INCLUDED
#define _UIXIMGWRAP_H_79F33EAF_DDFB_43f1_9FC1_3E41CD7C9916_INCLUDED
/*
	I'm Vladimir Toropov (ebontrop@gmail.com) as a code writer transfer all ownership rights on this file
	with all exclusive intellectual rights for this product "FakeGPS" to Oleksii Gupa (aleksey.gupa@gmail.com).
*/
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw { namespace common
{
	class CImageEncoderMime
	{
	public:
		enum _e{
			ePng   = 0,
			eBmp   = 1,
			eJpg   = 2,
		};
	};

	class CImageEncoderEnum
	{
	public:
		CImageEncoderEnum(void);
		~CImageEncoderEnum(void);
	public:
		static HRESULT    GetEncoderClsid(const CImageEncoderMime::_e eMimeType, CLSID& __in_out_ref);
	private:
		CImageEncoderEnum(const CImageEncoderEnum&);
		CImageEncoderEnum& operator= (const CImageEncoderEnum&);
	};
}}}

#endif/*_UIXIMGWRAP_H_79F33EAF_DDFB_43f1_9FC1_3E41CD7C9916_INCLUDED*/