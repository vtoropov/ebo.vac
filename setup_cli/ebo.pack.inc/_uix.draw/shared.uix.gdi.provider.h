#ifndef __PLATINUMSHAREDGDIPROVIDER_H_C659CA0D_5756_44c3_A5DC_2BFD16EFA8D7_INCLUDED
#define __PLATINUMSHAREDGDIPROVIDER_H_C659CA0D_5756_44c3_A5DC_2BFD16EFA8D7_INCLUDED
/*
	I'm Vladimir Toropov (ebontrop@gmail.com) as a code writer transfer all ownership rights on this file
	with all exclusive intellectual rights for this product "FakeGPS" to Oleksii Gupa (aleksey.gupa@gmail.com).
*/
#pragma warning(disable: 4458)  // declaration of 'abcd' hides class member (GDI+)

#include <gdiplus.h>
#include <atlapp.h>
#include <atlgdi.h>

#include "shared.uix.gdi.draw.defs.h"
#include "shared.uix.gdi.draw.shape.h"
#include "shared.uix.gen.clrs.h"

#pragma comment(lib, "gdiplus.lib")

namespace ex_ui { namespace draw
{
	HRESULT   GdiplusStatusToHresult(const Gdiplus::Status);

	using ex_ui::draw::eAlphaValue;
	using ex_ui::draw::CColour;

	using ex_ui::draw::shape::CRectEdge;

	class CZBuffer : public ::WTL::CDC
	{
		typedef ::WTL::CDC   TBaseDC;
	private:
		HDC            m_hOrigin;
		RECT           m_rcPaint;
		CBitmap        m_surface;
		HBITMAP        m_hBmpOld;
	public:
		CZBuffer(void);
		CZBuffer(const HDC hDC, const RECT& rcPaint);
		~CZBuffer(VOID);
	public:
		HRESULT        CopyTo(HBITMAP& hBitmap);
		HRESULT        CopyTo(
		                    CONST HDC hCompatibleDC,
		                    const INT _x,
		                    const INT _y,
		                    const BYTE _alpha = eAlphaValue::eOpaque
		               );
		HRESULT        Create(const HDC hDC, const RECT& rcPaint);
		VOID           DrawGragRect(
		                    const RECT& rcDraw,
		                    const COLORREF clrFrom,
		                    const COLORREF clrUpto,
		                    const bool bVertical,
		                    const BYTE ba
		               ) CONST;
		VOID           DrawLine(
		                    const INT _x0,
		                    const INT _y0,
		                    const INT _x1,
		                    const INT _y1,
		                    const COLORREF _clr,
		                    const INT nThickness = 1
		               );
		VOID           DrawRectangle(
		                    const RECT& rcDraw,
		                    const COLORREF clrBorder,
		                    const INT nThickness = 1,
							const DWORD dEdges = CRectEdge::eAll
		               );
		VOID           DrawSolidRect(
		                    const RECT& rcDraw,
		                    const CColour&
		               ) CONST;
		VOID           DrawSolidRect(
		                    const RECT& rcDraw,
		                    const COLORREF clrFill,
		                    const BYTE _alpha = eAlphaValue::eOpaque
		               ) CONST;
		VOID           DrawTextExt(
		                    LPCTSTR pszText,
		                    const HFONT fnt_,
		                    const RECT& rcDraw,
		                    const COLORREF clrFore,
		                    const DWORD fmt_
		               );
		VOID           DrawTextExt(
		                    LPCTSTR pszText,
		                    LPCTSTR pszFontFamily,
		                    const DWORD dwFontSize,
		                    const RECT& rcDraw,
		                    const COLORREF clrFore,
		                    const DWORD dwFormat
		               );
		const RECT&    GetDrawRect(void) const;
		bool           IsValid(void)const;
		VOID           Reset(void);                // copies the buffer content to original device and resets the buffer to uninitialized state
	public:
		operator HDC(void) const { return TBaseDC::m_hDC; }
	private:
		CZBuffer(const CZBuffer&);
		CZBuffer& operator= (const CZBuffer&);
	};

	class CGdiPlusLib_Guard {
	protected:
		volatile bool m_auto_mode;
	public:
		 CGdiPlusLib_Guard(const bool _b_auto_mode = true); // capturing and releasing are automatic;
		~CGdiPlusLib_Guard(void);
	public:
		HRESULT   Capture(void);
		HRESULT   Release(void);
		bool      Secured(void) const;
	private:
		CGdiPlusLib_Guard(const CGdiPlusLib_Guard&);
		CGdiPlusLib_Guard& operator= (const CGdiPlusLib_Guard&);
	};

	class CGdiPlusPngLoader
	{
	public:
		static HRESULT     CreateImages(const LPVOID pRawData, const DWORD _data_sz, Gdiplus::Bitmap*&);
		static HRESULT     CreateImages(const LPVOID pRawData, const DWORD _data_sz, HIMAGELIST& hImageList);
	public:
		static HRESULT     CreateImages(const ATL::_U_STRINGorID RID, const HMODULE hModule, HIMAGELIST& hImageList);
		static HRESULT     LoadResource(const ATL::_U_STRINGorID RID, const HMODULE hModule, Gdiplus::Bitmap*&);
		static HRESULT     LoadResource(const ATL::_U_STRINGorID RID, const HMODULE hModule, HBITMAP&);
	public:
		static HRESULT     CreateImages(const HBITMAP&, HIMAGELIST&); // bitmap handle provided must be destroyed by a caller;
		static HRESULT     CreateImages(const HBITMAP&, HIMAGELIST& , const SIZE& _sz_frame);
		static HMODULE     CurrentModule(void);
	};
}}

#endif/*__PLATINUMSHAREDGDIPROVIDER_H_C659CA0D_5756_44c3_A5DC_2BFD16EFA8D7_INCLUDED*/