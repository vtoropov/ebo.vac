#ifndef _UIXGDIDRAWSHAPE_H_895270FC_2B30_4EC7_96A2_490C2782EEE6_INCLUDED
#define _UIXGDIDRAWSHAPE_H_895270FC_2B30_4EC7_96A2_490C2782EEE6_INCLUDED
/*
	I'm Vladimir Toropov (ebontrop@gmail.com) as a code writer transfer all ownership rights on this file
	with all exclusive intellectual rights for this product "FakeGPS" to Oleksii Gupa (aleksey.gupa@gmail.com).
*/
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw { namespace shape
{
	class CRectEdge
	{
	public:
		enum _e{
			eNone   = 0x0,
			eLeft   = 0x1,
			eTop    = 0x2,
			eRight  = 0x4,
			eBottom = 0x8,
			eAll    = eLeft | eTop | eRight | eBottom
		};

	};

	class CRectEx
	{
	private:
		Gdiplus::Rect  m_rc;
	public:
		 CRectEx (void);
		 CRectEx (const CRectEx& );
		 CRectEx (const RECT& rc_);
		 CRectEx (const INT left_, const INT top_, const INT width_, const INT height_);
		~CRectEx (void);
	public:
		const
		Gdiplus::Rect&  Get (void) const;
		Gdiplus::Rect&  Get (void)      ;
		Gdiplus::Rect   Set (const INT left_, const INT top_, const INT width_, const INT height_);
		Gdiplus::Rect   Set (const RECT&);

	public:
		operator Gdiplus::Rect*(void);
		operator Gdiplus::Rect&(void);
		operator const RECT (void) const;

	public:
		CRectEx& operator = (const CRectEx&);
		CRectEx& operator = (const RECT&);
	};

	class CRectFEx
	{
	private:
		Gdiplus::RectF m_rc;
	public:
		 CRectFEx(void);
		 CRectFEx(const CRectFEx&);
		 CRectFEx(const RECT& rc_);
		 CRectFEx(const INT left_, const INT top_, const INT width_, const INT height_);
		~CRectFEx(void);
	public:
		const
		Gdiplus::RectF& Get (void) const;
		Gdiplus::RectF& Get (void)      ;
		Gdiplus::RectF  Set (const CRectEx&);
		Gdiplus::RectF  Set (const INT left_, const INT top_, const INT width_, const INT height_);
		Gdiplus::RectF  Set (const RECT&);

	public:
		operator Gdiplus::RectF*(void);
		operator Gdiplus::RectF&(void);
		operator const RECT (void) const;

	public:
		CRectFEx& operator = (const CRectFEx&);
		CRectFEx& operator = (const RECT&);
	};
}}}

#endif/*_UIXGDIDRAWSHAPE_H_895270FC_2B30_4EC7_96A2_490C2782EEE6_INCLUDED*/